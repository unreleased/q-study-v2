<base href="<?php echo base_url();?>" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $headerlink;?>
    </head>
    <body>
        <div class="wrapper">
            <!--===================== Header ========================-->

            <section class="main_content ss_sign_up_content bg-gray animatedParent">
                <div class="container-fluid container-fluid_padding">
                    <div class="row"> 
                        
                        <?php echo $header;?>

                    </div> 
                    <div class="">
                        <div class="row">
                            
                            <?php echo $maincontent;?>
                            
                        </div>
                    </div>
                </div>
            </section>
            
            <!--===================== End of Footer ========================-->
        </div>
        
        <!--wrapper-->
        
        <?php echo $footerlink;?>
        
        
    </body>
</html>