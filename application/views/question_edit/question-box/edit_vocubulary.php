<?php //echo '<pre>';print_r($question_info_ind->vocubulary_image);die;?>
<style>
  .form-group{
    display: block !important;
    margin-bottom: 10px !important;
  }
</style>

<div class="col-sm-4">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" aria-expanded="true" aria-controls="collapseOne">
            <span onclick="setSolution()">
              <img src="assets/images/icon_solution.png"> Solution
            </span> Question
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body form-horizontal ss_image_add_form">
          <!--  <form class="form-horizontal ss_image_add_form"> -->
            <div class="form-group">
              <label for="inputwordl3" class="col-sm-4 control-label">Word</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputword3" name="answer" value="<?php echo $question_info[0]['answer']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputDefinitionl3" class="col-sm-4 control-label">Definition</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputDefinitionl3" name="definition" value="<?php echo $question_info_ind->definition; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPartsofspeech3" class="col-sm-4 control-label">Parts of speech</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputPartsofspeech3" name="parts_of_speech" value="<?php echo $question_info_ind->parts_of_speech; ?>" >
              </div>
            </div>
            <div class="form-group">
              <label for="inputSynonym3" class="col-sm-4 control-label">Synonym</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputSynonym3" name="synonym" value="<?php echo $question_info_ind->synonym; ?>" >
              </div>
            </div>
            <div class="form-group">
              <label for="inputAntonym3" class="col-sm-4 control-label">Antonym</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputAntonym3" name="antonym" value="<?php echo $question_info_ind->antonym; ?>" >
              </div>
            </div>

            <div class="form-group">
              <label for="inputYourSentence3" class="col-sm-4 control-label">Hint</label>
              <div class="col-sm-8">
                <a data-toggle="modal" data-target="#questionHint" class="text-center" style="display: inline-block;">
                  <img src="assets/images/icon_details.png">
                </a>
                <input type="hidden" class="form-control" id="sentence" name="sentence" placeholder="Your Sentence">
                <!-- <input type="text" class="form-control" id="inputYourSentence3" name="sentence" value="<?php echo $question_info_ind->sentence; ?>" > -->
              </div>
            </div>
            <div class="form-group">
              <label for="inputNearAntonym3" class="col-sm-4 control-label">Category</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="inputNearAntonym3" name="near_antonym" value="<?php echo $question_info_ind->near_antonym; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputNearAntonym3" class="col-sm-4 control-label">Speech To Text</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="spchToTxt" name="speech_to_text" value="<?php echo isset($question_info_ind->speech_to_text)?$question_info_ind->speech_to_text:''; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-4 control-label">Audio File</label>
              <div class="col-sm-8">
                <input type="file" id="exampleInputFile" name="audioFile">
                <input type="hidden" name="existed_audio_File" value="<?php echo isset($question_info_ind->audioFile)?$question_info_ind->audioFile:''; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-4 control-label">Video file</label>
              <div class="col-sm-8">
                <?php if ($this->session->userdata('userType')==7) : ?>
                  <input type="file" id="exampleInputFilevideo" name="videoFile">
                  <label id="upload-photo-label" for="exampleInputFilevideo"><i class="fa fa-youtube-play"></i></label>
                <?php endif; ?>
                <label id="upload-photo-label" class="ytLink"><i class="fa fa-youtube"></i></label>
              </div>
            </div>

            <!--  </form> -->
          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="col-sm-4">
    <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">  Image</a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body ss_imag_add_right">

            <div class="text-center">
              <div class="form-group ss_h_mi">
                <label for="exampleInputiamges1">How many images</label>
                <div class="select">
                  <input class="form-control" type="number" value="<?php echo sizeof($question_info_ind->vocubulary_image); ?>" id="box_qty" onclick="getImageBox(this)">
                </div>
              </div>
            </div>

            <div class="image_box_list" id="image_box_list">
                <?php
                $i = 1;
                $lettry_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'k', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
                foreach ($question_info_ind->vocubulary_image as $row) {
                    ?>
                <div class="row editor_hide" id="list_box_<?php echo $i; ?>" style="display:none; margin-bottom:5px">
                  <div class="col-xs-2">
                    <p class="ss_lette" style="min-height: 136px; line-height: 137px; ">
                      <?php echo $lettry_array[$i - 1]; ?>
                    </p>
                  </div>
                  <div class="col-xs-10">
                    <div class="box">
                      <textarea class="form-control vocubulary_image" name="vocubulary_image_<?php echo $i; ?>[]"><?php echo $row[0]; ?></textarea>
                    </div>
                  </div>
                </div>
                    <?php $i++;
                } ?>
                <?php for ($desired_i = $i; $desired_i <= 20; $desired_i++) { ?>                    
                <div class="row editor_hide" id="list_box_<?php echo $desired_i; ?>" style="display:none; margin-bottom:5px">
                  <div class="col-xs-2">
                    <p class="ss_lette" style="min-height: 136px; line-height: 137px; ">
                      <?php echo $lettry_array[$desired_i - 1]; ?>
                    </p>
                  </div>
                  <div class="col-xs-10">
                    <div class="box">
                      <textarea class="form-control vocubulary_image" name="vocubulary_image_<?php echo $desired_i; ?>[]"></textarea>
                    </div>
                  </div>
                </div>                      
                <?php } ?>
            </div>
            <input type="hidden" name="image_quantity" id="image_quantity">         

          </div>
        </div>
      </div>


    </div>
  </div>

  <!-- vocabulary hint modal -->
  <div class="modal fade ss_modal ew_ss_modal" id="questionHint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel"> Question Hint </h4>
        </div>
        <div class="modal-body">
          <textarea class="form-control" id="questionHintText" name="questionHint"><?php echo $question_info_ind->sentence; ?></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
          <button type="button" id="hintSaveBtn" class="btn btn_blue" data-dismiss="modal">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div id="ytLinkDialog" title="Set youtube link" style="display: none">
    <input class="form-control" type="text" name="" id="dialogLink" value="<?php echo isset($question_info_ind->ytLinkInput) ? $question_info_ind->ytLinkInput:''; ?>">
    <hr>
    <div class="well" style="padding:10px;"><strong>Put Reference</strong></div>
    <input class="form-control" type="text" name="" id="dialogTitle" value="<?php echo isset($question_info_ind->ytLinkTitle) ? $question_info_ind->ytLinkTitle:''; ?>">
  </div>

  <input type="hidden" name="ytLinkInput" id="ytLinkInput" value="<?php echo isset($question_info_ind->ytLinkInput) ? $question_info_ind->ytLinkInput:''; ?>">
  <input type="hidden" name="ytLinkTitle" id="ytLinkTitle" value="<?php echo isset($question_info_ind->ytLinkTitle) ? $question_info_ind->ytLinkTitle:''; ?>">

  <script>
    var qtye = $("#box_qty").val();

    document.getElementById("image_quantity").value = qtye;
    common(qtye);
    function getImageBox() {
      var qty = $("#box_qty").val();
      if (qty < 1) {
        $("#box_qty").val(1);
      } else if (qty > 20) {
        $("#box_qty").val(20);
      } else {
        $('.editor_hide').hide();
        document.getElementById("image_quantity").value = qty;
        common(qty);
      }

    }
    function common(quantity)
    {
      for (var i = 1; i <= quantity; i++)
      {
        $('#list_box_' + i).show();
      }
    }


  //save yt link on hidden field
  $('.ytLink').on('click', function(){
    $( "#ytLinkDialog" ).dialog({
      minWidth: 600,
      title: 'Set youtube link',
      buttons: [
      {
        text: "Cancel",
        icon: "ui-icon ui-icon-heart",
        click: function() {
          $( this ).dialog( "close" );
        }
      },
      {
        text: "Save",
        icon: "ui-icon-heart",
        click: function() {
          var link = $('#dialogLink').val();
          var title = $('#dialogTitle').val();
          $('#ytLinkInput').val(link);
          $('#ytLinkTitle').val(title);
          $( this ).dialog( "close" );
        }
      }
      ]
    });
  });

  //hint save on hidden input
  $('#hintSaveBtn').on('click', function(){
    var hint = $('#questionHintText').val();
    $('#sentence').val(hint);
  })
</script>

