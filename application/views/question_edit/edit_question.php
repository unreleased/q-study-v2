<style>
  .ss_q_btn {
    margin-top: 21px;
  }

  .checkbox,.form-group{
    display: block !important;
    margin-bottom: 10px !important;
  }

  .form-control {
    width: 100% !important;
  }

  .createQuesLabel{
    margin-top: 5px;
  }

  .select2-container .select2-selection--single {
    height: 33px;
    margin-top: 6px;
  }

  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 30px;
  }

  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 40px;
  }

</style>
<div id="add_ch_success" style="text-align:center;"></div>
<!-- <form class="form-inline" id="question_form"> -->
  <form class="form-inline" id="question_form" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="questionType" value="<?php echo $question_item?>">

    <div class="row" >
      <div class="col-sm-1"></div>
      <div class="col-sm-11 ">
        <div class="ss_question_add_top">
          <div class="form-group" style="float: left;margin-right: 10px;margin-top: 10px;">
            <ul class="ss_question_menu" id="quesType_1">
              <li style="background-color: ">
                <a href="question_edit/<?php echo $question_item.'/'.$question_id ?>"><?php echo "Q".$qIndex; ?></a>
              </li>
            </ul>
          </div>

          <div class="form-group" style="float: left;margin-right: 10px;">
            <label for="exampleInputName2">Grade/Year/Level</label>
            <select class="form-control createQuesLabel" name="studentgrade">
              <option value="">Select Grade/Year/Level</option>
              <?php $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; ?>
              <?php foreach ($grades as $grade) { ?>
                <option value="<?php echo $grade ?>" <?php if ($question_info[0]['studentgrade'] == $grade) { echo 'selected';} ?>>
                  <?php echo $grade; ?>
                </option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group" style="float: left;margin-right: 10px;">
            <label>Subject <span data-toggle="modal" data-target="#add_subject"><img src="assets/images/icon_new.png"> New</span> </label>
            <select class="form-control createQuesLabel" name="subject" id="subject" onchange="getChapter(this)">
              <option value="">Select ...</option>
              <?php foreach ($all_subject as $subject) { ?>
                <option value="<?php echo $subject['subject_id'] ?>" <?php if ($question_info[0]['subject'] == $subject['subject_id']) { echo 'selected'; } ?>>
                  <?php echo $subject['subject_name']; ?>
                </option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group" style="float: left;margin-right: 10px;">
            <label>Chapter <span id="get_subject"><img src="assets/images/icon_new.png"> New</span></label>
            <select class="form-control createQuesLabel" name="chapter" id="subject_chapter">
              <option value="">Select Chapter</option>
              <?php foreach ($subject_base_chapter as $chapter) { ?>
                <option value="<?php echo $chapter['id']; ?>" <?php if ($question_info[0]['chapter'] == $chapter['id']) {echo 'selected';} ?>>
                  <?php echo $chapter['chapterName']; ?>
                </option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group" style="float: left;margin-right: 10px;">
            <label>Country</label>
            <select class="form-control createQuesLabel select2" name="country" id="quesCountry">
              <option value="">Select Country</option>
              <?php foreach ($allCountry as $country) : ?>
                <?php $sel = strlen($selCountry)&&($country['id']==$selCountry) ? 'selected' : ''; ?>
                <option value="<?php echo $country['id'] ?>" <?php echo $sel; ?>><?php echo $country['countryName'] ?></option>
              <?php endforeach ?>  
            </select>
          </div>
          
          <a class="ss_q_btn btn btn_red pull-left" onclick="open_question_setting()">
            Question setting
          </a>

                <!-- <a class="ss_q_btn btn pull-left" onclick="save_question_data()">
                    <i class="fa fa-save" aria-hidden="true"></i> Save
                  </a> -->
                  <button class="ss_q_btn btn pull-left"><i class="fa fa-save" aria-hidden="true" type="submit"></i> Save</button>
                  <!--<a class="ss_q_btn btn pull-left" href="#" data-toggle="modal" data-target="#ss_sucess_mess"><i class="fa fa-save" aria-hidden="true"></i> Save</a>-->
                  <a class="ss_q_btn btn pull-left" href="#"><i class="fa fa-remove" aria-hidden="true"></i> Cancel</a>

                  <a class="ss_q_btn btn pull-left" id="preview_btn" href="question_preview/<?php echo $question_info[0]['questionType']; ?>/<?php echo $question_info[0]['id']; ?>" style="">
                    <i class="fa fa-file-o" aria-hidden="true"></i> Preview
                  </a>
                </div>

              </div>

            </div>
            <div class="row">
              <div class="ss_question_add">
                <div class="ss_s_b_main" style="">

                  <?php echo $question_box; ?>

                  <div class="col-sm-4">
                    <div class="panel-group ss_edit_q" id="raccordion" role="tablist" aria-multiselectable="true" style="display: none;">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a> 
                              <label class="form-check-label" for="">Question Time</label> 
                              <input type="checkbox" id="question_time" name="ansTimeRequired" value="1" <?php if ($question_info[0]['questionTime']) {echo 'checked';} ?>>  
                              Calculator Required 
                              <input type="checkbox" name="isCalculator" value="1" <?php if ($question_info[0]['isCalculator'] == 1) {echo 'checked';} ?>> 
                              <!-- Score <input type="checkbox" name=""> -->
                              <?php if($this->session->userdata('userType') == 7) : ?>
                                <strong style="text-decoration: underline; cursor:pointer;" data-toggle="modal" data-target="#ss_instruction_model">Upload video</strong>
                              <?php endif; ?>
                            </a>
                          </h4>
                        </div>

                        <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class=" ss_module_result">
                              <p>Module Name:</p>
                              <div class="table-responsive">
                                <table class="table table-bordered">
                                  <thead>    
                                    <tr>
                                      <th>SL</th>
                                      <th>Mark</th>
                                      <!-- <th>Obtained</th> -->
                                      <th>Description </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>1</td>
                                      <input type="hidden" class="form-control" id="marks_value" name="questionMarks" value="<?php echo $question_info[0]['questionMarks']; ?>">
                                      <td onclick="setMark()" id="marks">
                                        <?php echo $question_info[0]['questionMarks']; ?>
                                      </td>
                                      <!-- <td><?php //echo $question_info[0]['questionMarks']; ?></td> -->
                                      <td style="text-align: center;">
                                        <a href="" data-toggle="modal" data-target="#ss_description_model" class="text-center" style="display: inline-block;">
                                          <img src="assets/images/icon_details.png">
                                        </a>
                                        <?php if($this->session->userdata('userType') == 7){ ?>
                                          <a data-toggle="modal" data-target="#ss_instruction_model" class="text-center" style="display: inline-block;">
                                            <img src="assets/images/icon_draw.png">
                                          </a>
                                        <?php }?>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                              </div>
                              <!-- Modal -->
                              <div class="modal fade ss_modal ew_ss_modal" id="ss_description_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">

                                      <h4 class="modal-title" id="myModalLabel"> Question Description </h4>
                                    </div>
                                    <div class="modal-body">
                                      <textarea class="form-control" name="questionDescription"><?php echo $question_info[0]['questionDescription']; ?></textarea>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn_blue" data-dismiss="modal">Save</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <!-- Start Instruction Modal -->
                              <?php $question_instruct =isset($question_info[0]['question_instruction']) ? json_decode($question_info[0]['question_instruction']):'';?>
                              <div class="modal fade ss_modal ew_ss_modal" id="ss_instruction_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel"> Question Instruction </h4>
                                    </div>
                                    <div class="modal-body">
                                      <textarea class="form-control instruction" name="question_instruction"><?php echo trim($question_instruct[0])?></textarea>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn_blue" data-dismiss="modal">Save</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- End Instruction Modal -->

                              <?php
                              $questionTime = $question_info[0]['questionTime'];
                              $array = explode(":", $questionTime);
                              ?>
                              <p><strong> Time to answer:</strong></p>
                              <!--<form class="form-inline ss_common_form" id="set_time" style="display: none">-->
                                <div class="form-group" style="display: inline-block !important;">
                                  <select class="form-control" name="hour">
                                    <option>HH</option>
                                    <?php for ($i = 0; $i < 24; $i++) { ?>
                                      <?php if ($i < 24) { ?>
                                        <?php $hour = str_pad($i, 2, "0", STR_PAD_LEFT);
                                      } ?>
                                      <option <?php if ($array[0] == $hour) { echo 'selected'; } ?> value="<?php echo $hour; ?>">
                                        <?php echo $hour; ?>
                                      </option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <div class="form-group" style="display: inline-block !important;">
                                  <select class="form-control" name="minute">
                                    <option>MM</option>
                                    <?php for ($i = 0; $i < 60; $i++) { ?>
                                      <?php if ($i < 60) { ?>
                                        <?php $minute = str_pad($i, 2, "0", STR_PAD_LEFT);
                                      } ?>
                                      <option <?php if ($array[1] == $minute) { echo 'selected'; } ?> value="<?php echo $minute; ?>">
                                        <?php echo $minute; ?>
                                      </option>
                                    <?php } ?>

                                  </select>
                                </div>
                                <div class="form-group" style="display: inline-block !important;">
                                  <select class="form-control" name="second">
                                    <option>SS</option>
                                    <?php for ($i = 0; $i < 60; $i++) { ?>
                                      <?php 
                                      if ($i < 60) {
                                        $second = str_pad($i, 2, "0", STR_PAD_LEFT);
                                      } 
                                      ?>
                                      <option <?php if ($array[2] == $second) { echo 'selected'; } ?> value="<?php echo $second; ?>">
                                        <?php echo $second; ?>
                                      </option>
                                    <?php } ?>

                                  </select>
                                </div>
                                <br/>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php if($question_item == 11) {?>
                      <div class="col-sm-12">
                        <div class="row htm_r" style="margin: 10px 0px;<?php if($question_info_ind['operator'] != '/') {?>display: none;<?php }?>">

                          <div class="col-sm-10"></div>
                          <div class="col-sm-1 col-xs-3" style="text-align: right;font-size: 30px;line-height: 40px;padding-right: 0px;">
                            <span>R</span>
                          </div>
                          <div class="col-sm-1 col-xs-4">
                            <div class="pull-right" style="padding: 8px;min-width: 18%;border: 1px solid #ddd">
                              <input type="text" readonly="" name="remainder" class="form-control rsskpin" value="<?php echo $question_info_ind['remainder'];?>" style="background-color: #baffba">
                            </div>
                          </div>
                        </div>

                        <div class="col-sm-4"></div>
                        <div class="skip_box col-sm-4">
                          <div class="table-responsive">
                            <table class="dynamic_table_skpi table table-bordered">
                              <tbody class="dynamic_table_skpi_tbody">
                                <?php if($question_info_ind['operator'] != '/') {
                                  for($i = 1; $i <= $question_info_ind['numOfRows']; $i++) { ?>
                                    <tr class="rw<?=$i?>">
                                      <?php for($j = 0; $j < $question_info_ind['numOfCols']; $j++) {?>
                                        <td>
                                          <input type="text" data_q_type="0" data_num_colofrow="<?=$i?>_<?=$j?>" value="<?php echo $question_info_ind['item'][$i][$j]?>" name="item[<?=$i?>][]" class="form-control input-box rsskpin rsskpinpt<?=$i?>_<?=$j?>" readonly style="min-width:50px;background-color: #ffb7c5">
                                          <input type="hidden" value="" name="ques_ans[]" id="obj">
                                          <input type="hidden" value="" name="ans[]" id="ans_obj">
                                        </td>
                                      <?php }?>
                                    </tr>
                                  <?php }?>
                                  <tr>
                                    <td colspan="<?= $question_info_ind['numOfCols'] ?>">
                                      <input type="text" data_q_type="0" value="<?php echo $question_answer;?>" name="result" class="form-control input-box rsskpin" readonly style="min-width: 50px;background-color: #baffba;">
                                    </td>
                                  </tr>
                                <?php } if($question_info_ind['operator'] == '/') { 
                        //Divisor
                                  for($i = 1; $i <= 1; $i++) { ?>
                                    <tr class="rw<?=$i?>">
                                      <?php for($j = 0; $j < $question_info_ind['numOfCols']; $j++) {?>
                                        <td>
                                          <input type="text" data_q_type="0" data_num_colofrow="<?=$i?>_<?=$j?>" value="<?php echo $question_info_ind['divisor'][$j]?>" name="divisor[]" class="form-control input-box rsskpin rsskpinpt<?=$i?>_<?=$j?>" readonly style="min-width:50px;background-color: #ffb7c5">
                                          <input type="hidden" value="" name="ques_ans[]" id="obj">
                                          <input type="hidden" value="" name="ans[]" id="ans_obj">
                                        </td>
                                      <?php }?>
                                    </tr>
                                  <?php }}?>

                                </tbody>
                              </table>

                              <!-- may be its a draggable modal -->
                              <div id="skiping_question_answer" style="display:none">
                                <input type="text" name="set_skip_value" class="input-box form-control rs_set_skipValue">
                              </div>
                            </div>

                          </div>
                          <?php if($question_info_ind['operator'] == '/') {//Dividend  ?>
                            <div class="col-sm-4">
                              <div class="table-responsive">
                                <table class="dynamic_table_dividend table table-bordered">
                                  <tbody class="dynamic_table_dividend_tbody">
                                    <?php for($i = 1; $i <= 1; $i++) { ?>
                                      <tr class="rw<?=$i?>">
                                        <?php for($j = 0; $j < $question_info_ind['numOfRows']; $j++) {?>
                                          <td>
                                            <input type="text" data_q_type="0" data_num_colofrow="<?=$i?>_<?=$j?>" value="<?php echo $question_info_ind['dividend'][$j]?>" name="dividend[]" class="form-control input-box rsskpin rsskpinpt<?=$i?>_<?=$j?>" readonly style="min-width:50px;background-color: #ffb7c5">
                                            <input type="hidden" value="" name="ques_ans[]" id="obj">
                                            <input type="hidden" value="" name="ans[]" id="ans_obj">
                                          </td>
                                        <?php }?>
                                      </tr>
                                    <?php }?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          <?php }?>
                        </div>
                      <?php }?>

                      <input type="hidden" id="question_item" name="question_item" value="<?php echo $question_item; ?>">
                      <input type="hidden" id="question_id" name="question_id" value="<?php echo $question_id; ?>">
                    </div>
                  </div>
                </div>

                <!--Set Question Solution on jquery ui-->
                <div id="dialog">
                  <textarea  id="setSolution" style="display:none;">
                    <?php echo $question_info[0]['question_solution']?>
                  </textarea>
                </div>
                <input type="hidden" name="question_solution" id="setSolutionHidden" value='<?php echo $question_info[0]['question_solution']?>'>
              </form>


              <!--Set Question Marks-->
              <div class="modal fade ss_modal" id="set_marks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body row">
                      <form id="marksValue">
                        <div class="row">
                          <div class="col-xs-4 sh_input">
                            <!-- <input type="number" class="form-control" name="first_digit"> -->
                            <input type="hidden" class="form-control" name="first_digit" value="0">
                          </div>
                          <div class="col-xs-4 sh_input">
                            <input type="number" class="form-control" name="second_digit">
                          </div>
                          <div class="col-xs-4">
                            <input type="number" class="form-control" name="first_fraction_digit">
                            <input type="number" class="form-control" name="second_fraction_digit">
                          </div>
                        </div>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn_blue" onclick="markData()">Save</button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">

                      <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body row">
                      <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Update Sucessfully</span> 
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>
                    </div>
                  </div>
                </div>
              </div>

              <!--Add Chapter Modal-->
              <div class="modal fade ss_modal" id="add_chapter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Add Chapter</h4>
                    </div>
                    <div id="chapter_error"></div>
                    <div class="modal-body">
                      <form class="" id="add_subject_wise_chapter">

                        <div class="form-group">
                          <label for="attached_subject"></label>
                          <input type="hidden" class="form-control" name="attached_subject" id="attached_subject">
                        </div>
                        <div class="form-group">
                          <label for="chapter">Chapter Name</label>
                          <input class="form-control" name="chapter" id="chapter">
                        </div>

                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" onclick="add_chapter()" class="btn btn_blue">Save</button>
                      <button type="button" class="btn btn_blue" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>

              <!--Add Subject Modal-->
              <div class="modal fade ss_modal" id="add_subject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Add New Subject</h4>
                    </div>
                    <form class="" id="add_subject_name">
                      <div class="modal-body">

                        <div class="form-group">
                          <label>Add Subject</label>
                          <input type="text" class="form-control wordSearch" name="subject_name">
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" onclick="add_subject()" class="btn btn_blue">Save</button>
                        <button type="button" class="btn btn_blue" data-dismiss="modal">Cancel</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>


              <script>
                function setSolution() {

                  $( "#dialog" ).dialog({
                    height: 400,
                    width: 600,
                    buttons: [
                    {
                      text:"Close",
                      icon: "ui-icon-heart",
                      click: function() {
                        $( this ).dialog( "close" );
                      }
                    },
                    {
                      text:"Save",
                      click: function() {
                        var solution = ($('#setSolution').val());
                        ($('#setSolutionHidden').val(solution));
                        $( this ).dialog( "close" );
                      }
                    }
                    ]
                  });

                  $('#setSolution').ckeditor({
                    height: 200,
                    extraPlugins : 'simage,ckeditor_wiris',
                    filebrowserBrowseUrl: '/assets/uploads?type=Images',
                    filebrowserUploadUrl: 'imageUpload',
                    toolbar: [
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                    { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
                    '/',
                    { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] }, 
                    '/',
                    { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
                    { name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
                    ],
                    allowedContent: true
                  });
                }


    /*function save_question_data() {
        var form = $("#question_form");
        var question_item = document.getElementById('question_item').value;
        var pathname = '<?php echo base_url(); ?>';
        CKupdate();
        $.ajax({
            url: "update_question_data",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                $("#ss_sucess_mess").modal('show');
            }
        });
      }*/

      $("#question_form").on('submit', function(e){
       e.preventDefault();
       var form = $("#question_form");
       var question_item = document.getElementById('question_item').value;
       var pathname = '<?php echo base_url(); ?>';
       CKupdate();
       $.ajax({
         url: "update_question_data",
         type: "POST",
           //data: form.serialize(),
           data: new FormData(this),
           processData:false,
           contentType:false,
           cache:false,
           success: function (response) {
             $("#ss_sucess_mess").modal('show');
           }
         });
     });

      function CKupdate() {
        for (instance in CKEDITOR.instances)
          CKEDITOR.instances[instance].updateElement();
      }

      function add_subject() {
        $.ajax({
          url: "add_subject_name",
          method: "POST",
          data: $("#add_subject_name").serialize(),
          success: function (response) {
            $('#add_subject').modal('hide');

            $('#subject').html(response);
          }
        });
      }

      function getChapter(e) {
        var subject_id = e.value;
        $.ajax({
          url: "get_chapter_name",
          method: "POST",
          data: {
            subject_id: subject_id
          },
          success: function (response) {

            $('#subject_chapter').html(response);
          }
        });
      }

      function open_question_setting() {
        $("#raccordion").show();
      }
      
      $('#get_subject').click(function () {
        var subject_id = document.getElementById('subject').value;
        document.getElementById("attached_subject").value = subject_id;
        $('#add_chapter').modal('show');
      });
      
      function add_chapter() {
        var data = $("#add_subject_wise_chapter").serialize();
        $.ajax({
          url: "add_chapter",
          method: "POST",
          dataType: 'html',
          data: data,
          success: function (response) {
        // if (response == 1) {
          $('#add_ch_success').html('Chapter added successfully');
          $('#add_chapter').modal('hide');
        // } else {
          $('#subject_chapter').html(response);
        // }
      }
    });
      }
      
      function setMark() {
        $("#set_marks").modal('show');
      }
      

      function markData(){
        var marks = $("#marksValue").serializeArray();
        
        var first_digit = marks[0].value;
        var second_digit = marks[1].value;
        first_digit = first_digit.length ? first_digit : 0;
        second_digit = second_digit.length ? second_digit : 0;
        var number = "" + first_digit + second_digit;

        var first_fraction_digit = marks[2].value;
        var second_fraction_digit = marks[3].value;
        first_fraction_digit = first_fraction_digit.length ? first_fraction_digit : 1;
        second_fraction_digit = second_fraction_digit.length ? second_fraction_digit : 1;
        
        var decimal_digit = parseInt(number) + parseFloat(first_fraction_digit / second_fraction_digit);
        if(first_fraction_digit == second_fraction_digit){
          decimal_digit = parseInt(number) * parseFloat(first_fraction_digit / second_fraction_digit);
        }
        decimal_digit = decimal_digit.toFixed(2);
        $("#marks").html(decimal_digit) ;
        $("#marks_value").val(decimal_digit) ;
        $("#mark_icon").hide() ;
        $("#marks").show() ;

        $('#set_marks').modal('hide');
      }


      /*autocomplete*/
      $(document).ready(function(){ 
        $('.wordSearch').devbridgeAutocomplete({
          serviceUrl: 'Subject/suggestSubject',
          onSelect: function (suggestions) {
            console.log(suggestions.answer);
          }
        });
      })

    </script>