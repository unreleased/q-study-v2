    
<div class="container top100">
    <div class="row">

        <div class="col-md-10 text-center">
            <?php if ($this->session->userdata('success_msg')) : ?>
                <div class="alert alert-success alert-dismissible show" role="alert">
                    <strong><?php echo $this->session->userdata('success_msg'); ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php elseif ($this->session->userdata('error_msg')) : ?>
                <div class="alert alert-danger alert-dismissible show" role="alert">
                    <strong><?php echo $this->session->userdata('error_msg'); ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    </div> <!-- end row -->
    <form action="" method="post" id="reorderModuleForm">
        <div class="row text-center">
            <button class="btn btn-primary" type="submit">Save</button>
            <button class="btn btn-primary" type="button" disabled="true">Preview</button>
        </div> <!-- end save preview btn row -->

        <div class="row">
            <br>
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="exampleInputName2">Country</label>
                    <div class="select country">
                        <select class="form-control" name="" id="">
                            <?php echo $all_country; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-2">

                <div class="form-group">
                    <label for="exampleInputEmail2">Module Type</label>
                    <div class="select">
                        <select class="form-control select-hidden moduleType">
                            <option value="">Select....</option>
                            <?php foreach ($all_module_type as $module_type) { ?>
                                <option value="<?php echo $module_type['id'] ?>">
                                    <?php echo $module_type['module_type']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <br>
                    <button type="button" class="btn btn_green" id="moduleSearch"><i class="fa fa-search"></i>Choose Module</button>
                </div>
            </div>

            <div class="col-md-4">

            </div>


        </div> <!-- row -->

        <div class="sign_up_menu">
            <div class="table-responsive">
                <table class="table table-bordered" id="module_setting">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Module Name</th>
                            <th>Module Type</th>
                            <th>Subject</th>
                            <th>Reorder</th>
                        </tr>
                    </thead>
                    <tbody id="moduleList">
                        <?php echo $row; ?>

                    </tbody>
                </table>
            </div>
        </div><!-- end .sign_up_menu -->
    </form>
</div>


<script>
  /*prevent duplicate ordering for module*/
  $('.moduleOrder').on('change', function(e){
        //e.stopPropagation();
        var temp=0;
        var inp = this.value;

        $('.moduleOrder').not(this).each(function(){
          if(inp==this.value){
            temp=1;
          }
        });
        
        if(temp){
          alert('Order Overlaping, Please fix');
          $(this).val('');
        }else{
          var modId = $(this).siblings('#modId').val();
          $(this).siblings('input#modId_ordr').val(modId+'_'+inp);
        }

      })

    //checkbox check functionality on module ordering
    var maxOrder = $('#maxOrder').val();
    $(document).on('change', '#moduleChecked', function(){
        var x = $(this).siblings('#modOrdr');  
        var modId = $(this).siblings('#modId').val();
        if(this.checked){
            $(this).siblings('#modOrdr').prop('disabled', false);
            $(this).siblings('#modOrdr').prop('required', true);

            x.val(++maxOrder);
            
            $(this).siblings('input#modId_ordr').val(modId+'_'+maxOrder);
        } else {
            $(this).siblings('#modOrdr').prop('disabled', true);
            x.val('');
            --maxOrder;
            $(this).siblings('input#modId_ordr').val(modId+'_'+'0');//0 -> order removed
        }
    })

    //module ordering form submit & preview button enable
    $(document).on('submit', '#reorderModuleForm', function(e){
        e.preventDefault();

        var all_module_order = $('.moduleOrder');
        var tempOrderArray = [];
        var max_module_order = 0;
        $.each(all_module_order,function(index, object) {
            max_module_order = Math.max(max_module_order, object.value);
            if(!isNaN(parseInt(object.value))){
                tempOrderArray.push(parseInt(object.value));
            }
       });

        tempOrderArray.sort();
        var flag=0;
        for(var a=1; a<=max_module_order; a++){
            if($.inArray(a,tempOrderArray)==-1){
                flag = 1;
            }
        }
        if(flag){
            alert('You Need to Maintain the Sequence of Module Order And Order Can not be Overlapped');
        } else {
            $.ajax({
                url : 'Module/saveModuleOrdering',
                method : 'POST',
                data : $(this).serialize(),
                success : function(data){
                    if(data=='true') {
                        alert('Module Order Updated Successfully.');
                    }else {
                        alert('Something is wrong.');
                    } //end else
                } //end success
            });
        }
      
    })

//    function get_order_value(e,serial){
////        alert(serial);
//        $(this).siblings('input#modId').val('')
//        $("#modId_ordr"+serial).val($("#modId").val()+'_'+e.value);
//    }

    //module search
    $(document).on('click keyup','.moduleOrder', function(e){ 
        var module_id = $(this).siblings('input#modId').val();
        var order_id = $(this).val();
        
        $(this).siblings("input#modId_ordr").val(module_id + '_' + order_id);
    });
    
    $(document).on('click','#moduleSearch', function(e){
        e.preventDefault();
        var country    = $('.country :selected').val();
        var moduleType = $('.moduleType :selected').val();
        $.ajax({
            url: 'Module/moduleSearchFromReorderPage',
            method: 'POST',
            data: {
                'country': country,
                'moduleType':moduleType
            },
        })
        .done(function(data) {
            $('#moduleList').html(data)
        })
      
    })
  </script>
