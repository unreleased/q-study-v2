<style>
  .sign_up_menu ul {
    display: none !important;
  }
  
  .select2-container{
    display: initial;
  }
  .select, .select input{
    width: 100px !important;
  }

</style>

<!-- <div class="container top100"> -->
  <div>
    <div class="row">
      <div class="col-md-10 text-center">
        <?php if ($this->session->userdata('success_msg')) : ?>
          <div class="alert alert-success alert-dismissible show" role="alert">
            <strong><?php echo $this->session->userdata('success_msg'); ?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php elseif ($this->session->userdata('error_msg')) : ?>
            <div class="alert alert-danger alert-dismissible show" role="alert">
              <strong><?php echo $this->session->userdata('error_msg'); ?></strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php endif; ?>
        </div>
      </div>

      <div class="ss_q_list_top">

        <div class="ss_student_progress">
          <div class="search_filter">
            <form class="form-inline" method="post" action="javascript:;">
              <div class="form-group">
                <label for="exampleInputName2">Module Name</label>
                <div class="select">
                    <?php $modName = isset($_SESSION['modInfo']['moduleName']) ? $_SESSION['modInfo']['moduleName']:'';?>
                  <input type="text" value="<?php echo $modName; ?>" class="form-control" name="moduleName" style="width:120px;" id="moduleName">
                </div>
              </div>
              
                <?php if ($_SESSION['userType']==7) : ?>
                <div class="form-group">
                  <label for="exampleInputName2">Country</label>
                  <div class="select">
                    <?php $disabled = isset($_GET['country']) ? 'disabled' : ''; ?>
                    <select class="form-control" name="country" id="moduleCountry" onChange="moduleSearch()" <?php echo $disabled; ?>>
                      <option value="">Select Country</option>
                      <?php foreach ($allCountry as $country) : ?>
                            <?php $sel = isset($_SESSION['modInfo']['country'])&&($country['id']==$_SESSION['modInfo']['country']) ? 'selected' : '';?>
                        <option value="<?php echo $country['id'] ?>" <?php echo $sel; ?>><?php echo $country['countryName'] ?></option>
                        <?php endforeach ?>
                    </select>
                  </div>
                </div>
                <?php endif; ?>

              <div class="form-group">

                <label for="exampleInputName2">Grade/Year/Level</label>
                <div class="select">
                  <select class="form-control select-hidden" name="grade" id="moduleGrade" onChange="moduleSearch()">
                    <option value="" name="studentGrade">Select Grade/Year/Level</option>
                    <?php $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; ?>
                    <?php foreach ($grades as $stGrade) { ?>
                        <?php $sel = isset($_SESSION['modInfo']['studentGrade'])&&($stGrade==$_SESSION['modInfo']['studentGrade']) ? 'selected' : '';?>
                      <option value="<?php echo $stGrade; ?>" <?php echo $sel; ?>>
                        <?php echo $stGrade; ?>
                      </option>
                    <?php } ?>
                    <option value="13">Upper Level</option>
                  </select>

                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail2">Module Type</label>
                <div class="select">
                  <select class="form-control select-hidden" name="moduleType" name="studentGrade" id="moduleType" onChange="moduleSearch()">
                    <option value="" >Select....</option>
                    <?php foreach ($all_module_type as $module_type) {?>
                        <?php $sel = isset($_SESSION['modInfo']['moduleType'])&&($module_type['id']==$_SESSION['modInfo']['moduleType']) ? 'selected' : '';?>
                      <option value="<?php echo $module_type['id']?>" <?php echo $sel; ?>>
                        <?php echo $module_type['module_type'];?>
                      </option>
                    <?php }?>
                  </select>

                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail2">Subject</label>
                <div class="select">
                  <select class="form-control select-hidden" id="moduleSubject" name="subject" onChange="moduleSearch()">
                    <option value="">Select....</option>
                    <?php foreach ($all_subject as $subject) {?>
                        <?php $sel = isset($_SESSION['modInfo']['subject'])&&($subject['subject_id']==$_SESSION['modInfo']['subject']) ? 'selected' : '';?>
                      <option value="<?php echo $subject['subject_id']?>" <?php echo $sel; ?>>
                        <?php echo $subject['subject_name'];?>
                      </option>
                    <?php }?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail2">Chapter</label>
                <div class="select">
                  <select class="form-control select-hidden" name="chapter" id="moduleChapter" onChange="moduleSearch()">
                    <option value="">Select....</option>
                    <?php if (isset($_SESSION['modInfo']['chapter'])) : ?>
                        <?php echo $_SESSION['modInfo']['chapter']; ?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
              
                <?php if ($_SESSION['userType']==7) : ?>
                <div class="form-group">
                  <label for="exampleInputEmail2">Course</label>
                  <div class="select">
                    <select class="form-control select-hidden" id="moduleCourse" name="course" onChange="moduleSearch()">
                      <option value="">Select....</option>
                      <?php foreach ($all_course as $course) {?>
                            <?php $sel = isset($_SESSION['modInfo']['course_id'])&&($course['id']==$_SESSION['modInfo']['course_id']) ? 'selected' : '';?>
                        <option value="<?php echo $course['id']?>" <?php echo $sel; ?>>
                            <?php echo $course['courseName'];?>
                        </option>
                        <?php }?>
                    </select>
                  </div>
                </div>
                <?php endif; ?>

              <div class="form-group" style="width:80px !important;">
                <button type="button" id="modSearchBtn" onClick="moduleSearch()" class="btn btn-primary" style="margin-top:20px !important">search</button>
              </div>

              <div class="form-group">
                <a href="add-module">
                  <button type="button" class="btn btn_orange">
                    <i class="fa fa-file"></i>  Add New
                  </button>
                </a>
              </div>

              <div class="form-group">
                <a href="reorder-module" class="btn btn_green">Re-Order</a>
              </div>

            </form>

          </div>
        </div>

      </div>


      <div class="row">
        <div class="sign_up_menu">
          <div class="table-responsive">
            <table class="table table-bordered" id="module_setting">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Module Name</th>
                  <th>Module Type</th>
                  <th>Duplicate</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody id="allModuleTable">
                <?php $moduleTypes = ['','Tutorial', 'Everyday Study', 'Special Exam', 'Assignment'] ?>
                
                <?php foreach ($all_module as $module) : ?>
                  <tr id="<?php echo $module['id']; ?>" studentGrade="<?php echo $module['studentGrade']; ?>">
                    <td><?php echo date('d-M-Y', $module['exam_date']) ?></td>
                    <td id="modName">
                      <a href="edit-module/<?php echo $module['id']; ?>"><?php echo $module['moduleName']; ?></a> 
                    </td>
                    <td>
                      <?php echo $moduleTypes[$module['moduleType']]; ?>
                    </td> 
                    <td>
                      <i class="fa fa-clipboard" id="modDuplicateIcon" data-toggle="modal" data-target="#moduleDuplicateModal" style="color:#4c8e0c;"></i>
                    </td>
                    <td>
                      <a href="edit-module/<?php echo $module['id']; ?>">
                        <i class="fa fa-pencil" style="color:#4c8e0c;"></i>
                      </a>
                    </td>
                    <td>
                      <i data-toggle="modal" data-target="#moduleDelModal" class="fa fa-trash" id="dltModOpnIcon" style="color:red;"></i>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- delete module -->
    <div class="modal fade" tabindex="-1" role="dialog" id="moduleDelModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title">Really want to delete module?</h5>
          </div>
          <div class="modal-body text-center">
            <input type="hidden" value="" id="moduleToDel">
            <button type="button" class="btn btn-danger" id="moduleDltBtn">YES</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
          </div>
          <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- duplicate module -->
  <div class="modal fade" tabindex="-1" role="dialog" id="moduleDuplicateModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h5 class="modal-title">Really want to duplicate module?</h5>
        </div>
        
        <div class="modal-body text-center">
          <input type="hidden" value="" id="moduleToDel">
          
          <div class="row">

            <form class="form-horizontal" id="moduleDuplicateForm">
              <div class="col-md-10">

                <input type="hidden" name="origModId" id="origModId" val="">
                
                <div class="form-group row">
                  <label for="exampleInputEmail1" class="col-sm-4">Module Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="moduleName" id="duplicateModName" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-4" >Grade/Year/Level</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="studentGrade" name="studentGrade" required readonly>

                  </div>
                </div>

                <div class="form-group row">
                  <label  class="col-sm-4" >Date</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="enterDate" name="examDate" autocomplete="off" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-sm-4" >Module Type</label>
                  <div class="col-sm-8">
                    <select id="" class="form-control" name="moduleType" required>
                      <option value="">SELECT MODULE TYPE</option>
                        <?php echo $allRenderedModType; ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-sm-4" ></label>
                  <div class="col-sm-4">
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox1" value="1" name="respToSMS"> Respond to SMS
                    </label>
                  </div>
                  <div class="col-sm-4">                            
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox2" value="1" name="forAllStd"> For all student
                    </label>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-sm-4">For individual student</label>
                  <div class="col-sm-8">
                    <select id="indivStIds" class="form-control select2" name="indivStIds[]" multiple="">
                      <!-- <?php echo $allStudents; ?> -->
                      <option value="">--Student--</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-sm-4"></label>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary btn-sm">YES</button>
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary btn-sm">NO</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>

      </div><!-- /.modal-content -->

      <div class="modal-footer">
      </div>
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


  <script>
    //set module id to modal for deletion
    $(document).on('click', '#dltModOpnIcon' ,function(){
      var moduleTodel = $(this).closest('tr').attr('id');
      $('#moduleToDel').val(moduleTodel);
      
    })

    //module delete functionality
    $('#moduleDltBtn').on('click', function(){
      var moduleId = $(this).siblings('#moduleToDel').val();
      $.ajax({
        url:'Module/deleteModule',
        method:'post',
        data:{moduleId:moduleId},
        success: function(data){
          if(data=='true'){
            alert('Module deleted successfully');
          }else{
            alert('Something is wrong');
          }
          $('#moduleDelModal').modal('toggle');
          location.reload();
        }
      })
    });

    //set original module id on module duplicate modal
    $(document).on('click', '#modDuplicateIcon', function(){
      var origModId = $(this).closest('tr').attr('id');
      var origModStGrade = $(this).closest('tr').attr('studentGrade');
      var origModName = $(this).closest('tr').find('#modName a').html();
      console.log(origModName);
      $('#origModId').val(origModId);
      $('#studentGrade').val(origModStGrade);
      $('#duplicateModName').val(origModName);
    })

    //module duplicate form submit
    $(document).on('submit', '#moduleDuplicateForm', function(event){
      event.preventDefault();
      var data = $(this).serialize();

      $.ajax({
        url : 'Module/moduleDuplicate',
        method : 'POST',
        data : data,
        success : function(data) {
          if(data == 'false') {
            alert('To keep same module name please change student grade.');
          } else if(data == 'true') {
            alert('Module duplication complete.');
          } else {
            alert(data);
          }
          
          $('#moduleDuplicateModal').modal('toggle');
          location.reload();
        }
      })
      //console.log(data);
    });


    //get student on grade change
    $('#studentGrade').change(function () {
      var grade = $('#studentGrade :selected').val();
      $.ajax({
        url: 'Module/getStudentByGrade',
        type: 'POST',
        data: {grade: grade},
        success : function(data){
          $('#indivStIds').html(data);
        }
      })
      
    })
    //module search 
    function moduleSearch(){
      var moduleName = $('#moduleName').val();
      var country = $('#moduleCountry :selected').val();
      var grade = $('#moduleGrade :selected').val();
      var type = $('#moduleType :selected').val();
      var subject = $('#moduleSubject :selected').val();
      var chapter = $('#moduleChapter :selected').val();
      var course = $('#moduleCourse :selected').val();
      $.ajax({
        'url': 'module/search',
        'method' : 'POST',
        'data' : {
          'moduleName':moduleName,
          'country':country,
          'studentGrade':grade,
          'moduleType':type,
          'subject': subject,
          'chapter': chapter,
          'course_id': course,
        },
        beforeSend: function() {
          $.LoadingOverlay("show");
        },
        success : function(data){
          $(".table").dataTable().fnDestroy();
          $('#allModuleTable').html(data);
          $.LoadingOverlay("hide");

          $('.table').dataTable({
            searching: false,
            lengthChange:false,
            select: false,
            "aaSorting": []
          }); 
        } 
      })
    }


    //date picker on duplicate module modal
    $('#enterDate').datepicker({});
    //datatable
    $('.table').dataTable({
      searching: false,
      lengthChange:false,
      select: true,
      "aaSorting": []
    });  
    //get chapter on subject change
    $(document).on('change', '#moduleSubject', function(){
      var subject = $(this).val();
      $.ajax({
        url:'Tutor/get_chapter_name',
        method:'post',
        data:{'subject_id':subject},
        success: function(response){
          $('#moduleChapter').html(response);
        }
      })
    });

  </script>

    <?php unset($_SESSION['modInfo']); ?>