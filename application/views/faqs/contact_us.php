<br>
<div class="row" style="margin-top:30px; margin-bottom:30px;">
    <div class="col-md-4 text-center">
        <img src="assets/images/banner/contact_us_1.png" alt="" width="215px" height="285px">
    </div>

    <div class="col-md-4">
        <img style="display: block;margin:auto; margin-bottom:80px;" src="assets/images/banner/contact_us_2.jpg" alt="" width="240" height="200" >
        <form class="form-horizontal" method="POST" name="contact" action="contact_us">
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Name"  name="userName">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="userEmail">
                </div>
            </div>
            <div class="form-group">
                <label for="messageBody" class="col-sm-2 control-label">Message</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="messageBody" name="userMessage"></textarea>
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3 text-left" style="padding-left:0px !important;">
        <div class="panel panel-default">
            <?php if (isset($qStudyContactInfo)) : ?>
                <div class="panel-body">
                    <?php echo $qStudyContactInfo; ?>
                </div>
            <?php else : ?>
                    <div class="panel-body">
                        <strong style="color:#0078AE;">SHOP 2/60 A THE BOULEVARDE LAKEMBA NSW 2195 AUSTRALIA</strong>
                    </div>
                    <div class="panel-body">
                        <p>Phone : 02 80045632</p>
                        <p>Mobile : 0414339854</p>
                        <p>Email : info@q-study.com</p>
                    </div>
            <?php endif; ?>
            </div>


        </div>
    </div>
