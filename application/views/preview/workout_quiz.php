<br>
<div class="ss_student_board">
  <div class="ss_s_b_top">
    <div class="ss_index_menu">
      <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
    </div>
    <div class="col-sm-6 ss_next_pre_top_menu">
        <?php if ($question_info_s[0]['isCalculator']) : ?>
        <input type="hidden" name="" id="scientificCalc">
        <?php endif; ?>

		<a class="btn btn_next" href="question_edit/<?php echo $question_item; ?>/<?php echo $question_id; ?>">
			<i class="fa fa-caret-left" aria-hidden="true"></i> Back
		</a>
		<a class="btn btn_next" href="#">
			<i class="fa fa-caret-right" aria-hidden="true"></i> Next
		</a>
      <!-- <a class="btn btn_next" href="#">Draw <img src="assets/images/icon_draw.png"></a> -->
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="ss_s_b_main" style="min-height: 100vh">
        <div class="col-sm-4">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><img src="assets/images/icon_draw.png"> Instruction</span> Question</a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body" id="quesBody">
                  <textarea disabled class="mytextarea" ><?php echo $question_info[0]['questionName']?></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" value="<?php echo $question_id; ?>" name="question_id" id="question_id">
        
        <!-- <div class="col-sm-4">
          <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">Answer</a>
                </h4>
              </div>
              <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <textarea name="answer" class="mytextarea"></textarea>
              </div>
            </div>
          </div>
          <div class="col-sm-4"></div>
          <div class="col-sm-4">
            <button class="btn btn_next" id="answer_matching">Submit</button>
          </div>
          <div class="col-sm-4"></div>
        
        </div> -->
			<div class="col-sm-4">
				<strong class="text-center">
					<a href="javascript:void(0)" style="text-decoration: underline;padding:10px;" onclick="showDrawBoard()">Workout</a>
				</strong>
				<div class="text-center" style="margin-top:10px;">
					<a class="btn btn_next" id="answer_matching">Submit</a>
				</div>
			</div>
        
			<div class="col-sm-4">
				<div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
					  <div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						  <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
							<span>Module Name: Every Sector</span></a>
						  </h4>
						</div>
						<div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class=" ss_module_result">
								  <div class="table-responsive">
									<table class="table table-bordered">
									  <thead>    
										<tr>

										  <th>SL</th>
										  <th>Mark</th>
										  <!-- <th>Obtained</th> -->
										  <th>Description</th>

										</tr>
									  </thead>
									  <tbody>
										<tr>
										  <td>1</td>
										  <td><?php echo $question_info[0]['questionMarks']; ?></td>
										  <!-- <td><?php // echo $question_info[0]['questionMarks']; ?></td> -->
										  <td><a onclick="showDescription()" class="text-center"><img src="assets/images/icon_details.png"></a></td>
										</tr>

									  </tbody>
									</table>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		  
			<div class="col-sm-4 pull-right" id="draggable" style="display: none;">
				<div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
							</h4>
						</div>
						<div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<textarea name="workout" class="mytextarea"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

          
        </div>

      </div>

      <div class="row">
        
      </div>
    </div>
  </div>

  <!--Description Modal-->
  <div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">Question Description</h4>
        </div>
        <div class="modal-body row">
          <span class="ss_extar_top20"><?php echo $question_info[0]['questionDescription']?></span> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

        </div>
      </div>
    </div>
  </div>


  <!--Success Modal-->
  <div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
        </div>

        <div class="modal-body row">
          <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

        </div>
      </div>
    </div>
  </div>

  <!--Solution Modal-->
  <div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Solution</h4>
        </div>
        <div class="modal-body row">
          <i class="fa fa-pencil" style="font-size:20px;color:#e0ca28; margin-left: 5px;"></i><br>
          <span class="ss_extar_top20">
            Examiner will scrutinize your answer and get back to you.
          </span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
        </div>
      </div>
    </div>
  </div>


  <script>
    $('#answer_matching').click(function () {
      $('#ss_info_worng').modal('show');
    });

    function showDescription(){
      $('#ss_info_description').modal('show');
    }
  </script>

    <?php $this->load->view('module/preview/drawingBoard'); ?>