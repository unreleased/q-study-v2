<?php
if (isset($question_info['item'])) {
    end($question_info['item']);
    $last_item_index = key($question_info['item']);
}
    
//    echo '<pre>';print_r($question_info);die;
?>

<style>
    .operator_div{
        display: inline-block;
        position: relative;
        width: 0;
    }
    
    .operator_div span{
        position: absolute;
        bottom: -2px;
        left: -9px;
    }
</style>

<div class="ss_student_board">
    <div class="ss_s_b_top">
        <div class="ss_index_menu">
            <a href="<?php echo base_url().$userType.'/view_course'; ?>">Question/Module</a>
        </div>
        <div class="col-sm-6 ss_next_pre_top_menu">
            <a class="btn btn_next" href="question_edit/<?php echo $question_item; ?>/<?php echo $question_id; ?>">
                <i class="fa fa-caret-left" aria-hidden="true"></i> Back
            </a>
            <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
            <a class="btn btn_next" href="javascript:void(0)" onclick="showDrawBoard()">Draw <img src="assets/images/icon_draw.png"></a>
        </div>
    </div>
    <div class="container-fluid">
        <form id="preview_form">
            <div class="row">
                <div class="ss_s_b_main" style="min-height: 100vh">
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span><img src="assets/images/icon_draw.png"> Instruction</span> Question
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                                    <div class="panel-body">
                                        <!-- <div class=" math_plus">
                                            <?php echo $question_info['questionName']; ?>
                                        </div> -->
                                        <textarea disabled class="mytextarea"><?php echo $question_info['questionName'];?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <input type="hidden" value="<?php echo $question_id; ?>" name="question_id" id="question_id">
                    
                    <div class="col-sm-4">
                        <div class="row">
                            <strong class="text-center">
                                <a href="javascript:void(0)" style="text-decoration: underline;padding:10px;" onclick="showDrawBoard()">Workout</a>
                            </strong>
                            <div class="col-sm-12 times_table_div" style="<?php if($question_info['operator'] != '/'){?>text-align: center<?php }?>">
                                <div style="font-size: 30px;">
                                    <?php if ($question_info['operator'] != '/') { ?>
                                        <div id="quesBody" style="padding: 0px 10px;border-bottom: 2px solid black;text-align: right;margin-bottom: 10px;">
                                            <?php $i = 1;
                                            foreach ($question_info['item'] as $row) {
                                                if ($i == $last_item_index) {
                                                    ?>
                                                    <div class="operator_div"><span><?php echo $question_info['operator'] ?></span></div>
                                                <?php }
                                                foreach ($row as $key_data) {
                                                    ?>
                                                    <span><?php echo $key_data; ?></span>
                                            <?php } ?><br>
                                            <?php $i++;
                                        } ?>
                                        </div>
                                        <input type="text" class="form-control" id="" name="answer" autocomplete="off" autofocus style="font-size: 30px;">
                                    <?php } if ($question_info['operator'] == '/') { ?>
                                        <div style="display: block;margin-top: 55px;">
                                            <div class="form-group" style="float: left;">
                                                <input type="text" class="form-control" id="" name="answer[]" autocomplete="off" autofocus style="font-size: 30px;max-width: 160px !important">
                                            </div>
                                            <div class="form-group" style="float: left;margin-left: 30px;">
                                                <label>R</label>
                                                <input type="text" class="form-control" id="" name="answer[]" autocomplete="off" autofocus style="font-size: 30px;">
                                            </div>

                                        </div>

                                        <div>
                                            <div id="quesBody" style="float: left;padding: 5px;">
                                                    <?php
                                                    foreach ($question_info['divisor'] as $divisor) {
                                                        echo $divisor;
                                                    }
                                                    ?><span class="dividend">
                                                        <?php
                                                        foreach ($question_info['dividend'] as $dividend) {
                                                            echo $dividend;
                                                        }
                                                        ?>
                                                    </span>
                                            </div>
                                            <!--<div style="float: left;padding: 5px;background-image: url('assets/images/44.png');padding: 3px 5px 6px 17px;background-repeat: no-repeat;background-position: 0px 0px;min-height: 41px;">
                                            <?php
                                            foreach ($question_info['dividend'] as $dividend) {
                                                echo $dividend;
                                            }
                                            ?>
                                            </div>-->
                                        </div>

                                    <?php } ?>
                                </div>
                                
                            </div>
                            
                        </div>
						
						<div class="col-sm-12" style="text-align: center">
							<button class="btn btn_next" type="submit" id="answer_matching">Submit</button>
						</div>
						
                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
                                            <span>Module Name: Every Sector</span></a>
                                        </h4>
                                    </div>
                                    <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class=" ss_module_result">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>    
                                                            <tr>

                                                                <th>SL</th>
                                                                <th>Mark</th>
                                                                <!-- <th>Obtained</th> -->
                                                                <th>Description</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td><?php echo $question_info_s[0]['questionMarks']; ?></td>
                                                                <!-- <td><?php // echo $question_info[0]['questionMarks']; ?></td> -->
                                                                <td><a onclick="showDescription()" class="text-center"><img src="assets/images/icon_details.png"></a></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
					
					<div class="col-sm-4 pull-right" id="draggable" style="display: none;">
						<div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
									</h4>
								</div>
								<div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body" id="setWorkoutHere">
										<textarea name="workout" class="mytextarea"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

                    <!--<div class="col-sm-12" style="text-align: center">
                        <button class="btn btn_next" type="submit" id="answer_matching">Submit</button>
                    </div>-->

                </div>

            </div>
        </form>
    </div>
</div>

    <!--Description Modal-->
    <div class="modal fade ss_modal" id="ss_info_description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel">Question Description</h4>
                </div>
                <div class="modal-body row">
                    <span class="ss_extar_top20"><?php echo $question_info_s[0]['questionDescription']?></span> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

                </div>
            </div>
        </div>
    </div>


    <!--Success Modal-->
    <div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                </div>

                <div class="modal-body row">
					<img src="assets/images/icon_sucess.png" class="pull-left"> 
                    <span class="ss_extar_top20">Your answer is correct</span>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

                </div>
            </div>
        </div>
    </div>

    <!--Solution Modal-->
    <div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Solution</h4>
                </div>
                <div class="modal-body row">
                    <i class="fa fa-times" style="font-size:20px;color:red"></i><br>
                    <span class="ss_extar_top20">
                        <?php echo $question_info_s[0]['question_solution']?>
                        
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
                </div>
            </div>
        </div>
    </div>


<script>
    
    $("#preview_form").on('submit', function (e) {
        e.preventDefault();
		var workout_val = CKEDITOR.instances['workout'].getData();
		if(workout_val == ''){
			alert('You have to do workout first');
		}  else {
            var form = $("#preview_form");
        
            $.ajax({
                type: 'POST',
                url: 'IDontLikeIt/answer_algorithm',
                data: form.serialize(),
                dataType: 'html',
                success: function (results) {
                    if (results == 0) {
                        $('#ss_info_worng').modal('show');
                    } else if (results == 1) {
                        $('#ss_info_sucesss').modal('show');
                    }
                }
            });
        }

    });

    function showDescription(){
        $('#ss_info_description').modal('show');
    }
</script>

<?php $this->load->view('module/preview/drawingBoard'); ?>
