<?php
if ($this->session->userdata('user_id')) {
    $user = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
    $user = $user[0];
}

?>

<div class="sign_up_header">
    <div class="col-sm-4">
        <?php if(isset($pageType) and $pageType=='q-dictionary'): ?>
            <div class="col-md-12" style="margin-top: 5px; margin-bottom:10px;">
                <img src="assets/images/q-dictionary.png" alt="User Image" style="width: 265px; height:125px"/>   
            </div>  
        <?php elseif ($this->session->userdata('user_id')) : ?>
            <div class="col-md-12">
                <span style="float: left;">Welcome <?php echo $this->session->userdata('user_email'); ?></span>
                <a href="<?php echo base_url();?>logout" class="col-xs-2">
                    <button class="btn btn-default" style="color: #6e8de7;padding: 3px 12px;background-color: #ededed;border-color: #ccc;font-weight: bold;">Logout</button>
                </a>  
            </div>
            <div class="col-md-12" style="margin-top: 5px; margin-bottom:10px;">
            
                <?php if (isset($user['image']) && file_exists('assets/uploads/'.$user['image'])) : ?>
                    <img src="<?php echo base_url();?>assets/uploads/<?php echo $user['image'];?>" alt="User Image" style="width: 125px; height:115px"/>
                <?php else : ?>
                    <img src="assets/images/default_user.jpg" alt="User Image" style="width: 125px; height:115px"/>   
                <?php endif; ?>

            </div>
        <?php endif; ?>
    </div>
    <div class="col-sm-4 text-center">
        <!-- <?php if (isset($pageType) and $pageType=='q-dictionary') : ?>
            <a href="#"><img src="assets/images/q-dictionary.png" ></a>
        <?php else : ?>
            <a href="#"><img src="assets/images/logo_signup.png" ></a>
        <?php endif; ?> -->
            <a href="#"><img src="assets/images/logo_signup.png" ></a>
    </div>
    <div class="col-sm-4">
        <div class="top_signup">
            <ul>
                <li><a href="<?php echo isset($_SESSION['prevUrl'])?$_SESSION['prevUrl']:'#'; ?>">Back</a></li>
                <li><a href="#"><img src="assets/images/icon_video.png"/> Video Help </a><span>1</span></li>
            </ul>
        </div>
    </div>
</div>
