<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/css3-animate-it.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/counter.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/main.js"></script> -->

<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/config.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/styles.js" type="text/javascript"></script>



<script>
	$('.modal-dialog').draggable({
        "handle":".modal-header"
    });

	$('.faq_textarea').ckeditor({
		height: 200,
		extraPlugins : 'simage, ckeditor_wiris,svideo,youtube',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile','SVideo', 'Youtube'] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});

	$('.mytextarea').ckeditor({
		height: 200,
		extraPlugins : 'simage, ckeditor_wiris',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});
  
	$('.vocubulary_image').ckeditor({
		height: 60,
		extraPlugins : 'simage',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] }, 
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] }
		],
		allowedContent: true
	});
  
	$('.assignment_textarea').ckeditor({
		extraPlugins : 'spdf,simage,sdoc,ckeditor_wiris',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage','SPdf','SDoc' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});
  
	$('.multiple_choice_textarea').ckeditor({
		height: 80,
		extraPlugins : 'simage,ckeditor_wiris',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'addFile'] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});
  
	$('.my_preview_textarea').ckeditor({
		height: 115,
		extraPlugins : 'simage,ckeditor_wiris',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','Table','-', 'Templates','Link', 'addFile'] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});
  
	$('.course_textarea').ckeditor({
		height: 50,
		extraPlugins : 'simage,ckeditor_wiris',
		filebrowserBrowseUrl: '/assets/uploads?type=Images',
		filebrowserUploadUrl: 'imageUpload',
		toolbar: [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage' ] },
		'/',
		{ name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','Table','-', 'Templates','Link', 'addFile'] },  
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
		{ name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
		],
		allowedContent: true
	});
	
	$('.student_assignment_textarea').ckeditor({
        extraPlugins : 'simage,spdf,sdoc,ckeditor_wiris,sppt',
        filebrowserBrowseUrl: '/assets/uploads?type=Images',
        filebrowserUploadUrl: 'imageUpload',
        toolbar: [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'NewPage', 'Preview','Preview', 'Print','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript', 'Superscript', '-', 'SImage','SPdf','SDoc','SPpt' ] },
            '/',
            { name: 'document', items: [ 'RemoveFormat','Maximize', 'ShowBlocks','TextColor', 'BGColor','-', 'Templates','Link', 'Image', 'addFile'] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
            // Line break - next group will be placed in new line.
            '/',
            { name: 'styles', items: [ 'Styles', 'Format','Font','FontSize'] },
            { name: 'wiris', items: [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry'] }
        ],
        allowedContent: true
    });
  
    // $('.vocubulary_image').ckeditor({
        // height: 80,
        // filebrowserBrowseUrl: '/assets/uploads?type=Images',
        // filebrowserUploadUrl: 'imageUpload',
        // allowedContent: true,
    // });

    $(document).ready(function () {
		$('#question_time').click(function () {
			if ($(this).is(':checked'))
				$("#set_time").show();
			else
				$("#set_time").hide();
		});
		$('.ss_bottom_s_course .select').on('click', function () {
			$(this).parent().find('div.active').removeClass('active');
			$(this).addClass('active');
		});
      
		$('.select2').select2({

		});

    });
</script>
<script>
    function chkLoginAccess() {

		var pathname = '<?php echo base_url(); ?>';
		var user_name = $("#user_name").val();
		var password = $("#password").val();
		$.ajax({
			type: 'POST',
			url: 'loginChk',
			data: {
			user_name: user_name,
			password: password
			},
			dataType: 'html',
			success: function (results) {
			if (results == 0) {
				$("#error_msg").show();
			}
			if (results == 1) {
				window.location.href = pathname + "dashboard";
			}
			if (results == 2) {
				window.location.href = pathname + "dashboard";
			}
	
			}
		});

    }

    $('#flashmsg').fadeOut(5000);

    /*calculator*/
    $('#scientificCalc').calculator({
		layout: $.calculator.scientificLayout,
		showOn: 'button',
		buttonImageOnly: true, 
		buttonImage: 'img/calculator.jpg'
    });
  </script>