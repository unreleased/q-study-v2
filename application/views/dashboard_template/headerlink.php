<meta charset="UTF-8">
<title>.:: Q-Study :: Tutor yourself...</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
<!-- Framework Css -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- Font Awesome / Icon Fonts -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/font-awesome.min.css">
<!-- Owl Carousel / Carousel- Slider -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/owl.carousel.min.css">
<!-- Animations -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/animations.min.css">
<!-- Style Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<!-- Light Style Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/light.css">
<!-- Responsive Theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/countrySelect.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/intlTelInput.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css" rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/css/jquery-ui.multidatespicker.css" rel="stylesheet" type="text/css"/>

<!--  Video JS  -->
<link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
<!-- Stripe JavaScript library -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/dropzone.js"></script>

<!-- popover -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.css">
<script src="https://cdn.jsdelivr.net/jquery.webui-popover/1.2.1/jquery.webui-popover.min.js"></script>


<!-- cdn -->
<!-- datatable -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>
<!-- jquery ui cdn -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery-ui.multidatespicker.js" type="text/javascript"></script>

<!-- jquery timepicker -->
<!-- <link rel="stylesheet" href="http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

<!-- calculator -->
<link rel="stylesheet" type="text/css" href="assets/js/jquery.calculator/jquery.calculator.css"> 
<script type="text/javascript" src="assets/js/jquery.calculator/jquery.plugin.js"></script> 
<script type="text/javascript" src="assets/js/jquery.calculator/jquery.calculator.js"></script>

<!-- sweet alert -->
<script src="assets/js/sweet_alert.js"></script>

<!-- yt player -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mb.YTPlayer/3.2.8/jquery.mb.YTPlayer.min.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.mb.YTPlayer/3.2.8/css/jquery.mb.YTPlayer.min.css" />

<!-- loading -->
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<!-- slick -->
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

<style> 
.ui-widget.ui-widget-content {
    border: 1px solid #c5c5c5;
    z-index: 10000 !important;
}

/* calculator */
.calculator-trigger {
    width:40px;
    height:auto;
}
.is-calculator{
    width: 100px;
    height: 27px;
    background-color:white !important
}
</style>

<!-- font awesome-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
<!-- context menu cdn-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.0/jquery.contextMenu.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.0/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.0/jquery.ui.position.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<!-- autocomplete -->
<script src="assets/js/jquery.autocomplete.min.js"></script>

<!-- select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js?viewer=image&async=true&_wrs_conf_saveMode=xml"></script>

<!--  Video JS  -->
<!-- <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
-->

<!-- jquery validation -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

<!--  text to speech -->
<script src='https://code.responsivevoice.org/responsivevoice.js'></script> 