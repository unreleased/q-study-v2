<style>
  .panel-heading{
    background-color: #2F91BA !important;
  }

  .panel-title a {
    text-decoration: none;
    color: #fff !important;
  }
</style>

<!-- flash message -->
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-8">
    <?php $this->load->view('flash_message_section'); ?>
  </div>
</div>

<div class="" style="margin-left: 15px;">
  <div class="row">
    <div class="col-md-4">
      <?php echo $leftnav ?>
    </div>


    <div class="col-md-8 user_list">
      <div class="panel-group " id="task_accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title text-center">
              <a role="button" data-toggle="collapse" data-parent="#task_accordion" href="#collapseOnetask" aria-expanded="true" aria-controls="collapseOne"> 
                <strong><span style="font-size : 18px; color:white;">  Edit User </span></strong>
              </a>
            </h4>
          </div>

          <form autocomplete="off" action="edit_user/<?php echo $userId; ?>" method="POST">
            <div class="row panel-body">
              <div class="row">
                <div class="col-sm-12 text-right">
                  <button class="btn btn_next" id="cancelBtn"><i class="fa fa-times" style="padding-right: 5px;"></i>Cancel</button>
                  <button type="submit" class="btn btn_next" id="saveBtn">
                    <i class="fa fa-check" style="padding-right: 5px;"></i>Update
                  </button>
                </div>
              </div>

            </div>
            
            <div class="row panel-body">
              <div class="row" style="padding:0px 5px 0px 5px;">
                <div class="col-sm-3">Subscription Type:</div>
                <?php $uSubType=$user_info[0]['subscription_type']; ?>
                <label class="radio-inline">
                  <input type="radio" name="subscription_type" id="inlineRadio1" value="trial" required  <?php echo $uSubType=='trial'? ' checked':' '; ?>> Trial
                </label>
                <label class="radio-inline">
                  <input type="radio" name="subscription_type" id="inlineRadio2" value="signup" required  <?php echo $uSubType=='signup'? ' checked':' '; ?>> Signup
                </label>
                <label class="radio-inline">
                  <input type="radio" name="subscription_type" id="inlineRadio3" value="guest" required  <?php echo $uSubType=='guest'? ' checked':' '; ?>> Guest
                </label> 
              </div>
            </div>   

            <div class="row panel-body">
              <div class="row" style="padding:0px 5px 0px 5px;">

                <div class="col-sm-6">

                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $user_info[0]['name']?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Email</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="email" name="user_email" value="<?php echo $user_info[0]['user_email']?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Password</label>
                    <div class="col-sm-7">
                      <input type="password" class="form-control" id="password" name="user_pawd">
                    </div>
                    <div class="col-sm-1" style="padding:0 0 0 0px !important;"><i id="revPass" class="fa fa-eye"></i></div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Mobile No</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="mobile" name="user_mobile" value="<?php echo $user_info[0]['user_mobile']?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Ref Link</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="ref_link" name="SCT_link" value="<?php echo $user_info[0]['SCT_link']?>">
                    </div>
                  </div>
                  
                  
                  <?php if ($user_info[0]['user_type']==1) : ?>
                    <label href="">Student Info:</lavel>

                      <?php $cnt=0; ?>
                      <?php foreach ($allChild as $child) : ?>
                        <a href="edit_user/<?php echo $child['id']; ?>"><?php echo $child['name']; ?></a>
                      <?php endforeach; ?>

                    <?php endif ; ?>

                    <?php if ($user_info[0]['user_type']==6) : ?>
                      <a href="edit_user/<?php echo $parent[0]['parent_id']; ?>">Parent Info</a>
                    <?php endif ; ?>


                  </div>



                  <div class="col-sm-6">

                    <div class="form-group row">
                      <label for="" class="col-sm-4 col-form-label">User Type</label>
                      <div class="col-sm-8">
                        <select id="userType" class="form-control" name="user_type" required readonly>
                          <option selected>Choose...</option>
                          <?php foreach ($user_type as $userType) : ?>
                            <option value="<?php echo $userType['id'] ?>" 
                              <?php if($user_info[0]['user_type'] == $userType['id']){echo 'selected';}?>>
                              <?php echo $userType['userType'] ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="grade" style="display:none">
                      <label for="" class="col-sm-4 col-form-label">Grade/Year/Level</label>
                      <div class="col-sm-8">
                        <select  class="form-control" name="grade">
                          <option selected value="">Choose...</option>
                          <?php for ($a=1; $a<=12; $a++) : ?>
                            <option value="<?php echo $a;?>"><?php echo $a; ?></option>
                          <?php endfor; ?>
                          <option value="13">Upper Level</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="parent" style="display:none">
                      <label for="" class="col-sm-4 col-form-label">Parent</label>
                      <div class="col-sm-8">
                        <select  class="form-control" name="parent_id">
                          <option selected value="">Choose...</option>
                          <?php foreach ($parents as $parent) : ?>
                            <option value="<?php echo $parent['id'] ?>">
                              <?php echo $parent['name'] ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="numOfChild" style="display:none">
                      <label for="" class="col-sm-4 col-form-label">Number of children</label>
                      <div class="col-sm-8">
                        <select  class="form-control" name="numOfChild">

                          <option selected value="">Choose...</option>
                          <?php for ($a=1; $a<=10; $a++) : ?>
                            <option value="<?php echo $a;?>"><?php echo $a; ?></option>
                          <?php endfor; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-sm-4 col-form-label">Active</label>
                      <div class="col-sm-8">
                       <input class="form-check-input" type="checkbox" name="suspension_status" value="1" id="suspension_status" <?php echo !$user_info[0]['suspension_status'] ? 'checked':''; ?>>

                     </div>
                   </div>


                   <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Extend Trial Period</label>
                    <div class="col-sm-8">
                      <input class="form-check-input" type="checkbox" name="" value="1" 
                      <?php if($user_info[0]['trial_end_date']){echo 'checked';}?> id="isExtendTrialPeriod">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Country</label>
                    <div class="col-sm-8">
                      <select  id="country" class="form-control" name="country_id">
                        <option value="">Choose...</option>
                        <?php foreach ($all_country as $country) : ?>
                          <option value="<?php echo $country['id'] ?>"<?php if($user_info[0]['country_id'] == $country['id']){echo 'selected';}?>>
                            <?php echo $country['countryName'] ?>
                          </option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <?php if($user_info[0]['user_type']==2 || $user_info[0]['user_type']==6): ?>
                  <div class="form-group row">
                    <label for="" class="col-sm-4 col-form-label">Grade</label>
                    <div class="col-sm-8">
                      <select class="form-control select-hidden" name="grade">
                        <option value="">Select Grade/Year/Level</option>
                        <?php $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; ?>
                        <?php foreach ($grades as $stGrade) { ?>
                          <?php $sel = isset($user_info[0])&&($stGrade==$user_info[0]['student_grade']) ? 'selected' : '';?>
                          <option value="<?php echo $stGrade; ?>" <?php echo $sel; ?>>
                            <?php echo $stGrade; ?>
                          </option>
                        <?php } ?>
                        <option value="13">Upper Level</option>
                      </select>

                    </div>
                  </div>
                  <?php endif; ?>

                </div>

                <div class="form-group row">
                  <div class="col-sm-12" style="margin:20px 0px 0px 50px">
                    <div class="ss_top_s_course">
                      <ul id="course_ul">
                        <?php echo isset($courses)?$courses:''; ?>
                      </ul>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </form>

        </div>

      </div>

    </div>

  </div>

</div>

<script src="<?php echo base_url();?>assets/js/intlTelInput.js"></script>

<script>
	var iti='';
	$( document ).ready(function() {
		var input = document.querySelector("#mobile");

		iti = intlTelInput(input, {
			utilsScript: "<?php echo base_url();?>assets/js/utils.js?1537727621611",
			autoHideDialCode: true,
			autoPlaceholder: "off",
			hiddenInput: "full_phone",
			defaultCountry: "aus",
			numberType: "MOBILE",
			separateDialCode:true,
		});
	});
	

    //add/hide form fields based on user type selection
    $(document).on('change', '#userType', function(){
      var userType = this.value;
      if(userType == 1) { 
        $('#numOfChild').css('display', 'block');

            //hide other types
            $('#parent').css('display', 'none');
            $('#grade').css('display', 'none');

          }else if(userType == 2 || userType == 6){
            $('#parent').css('display', 'block');
            $('#grade').css('display', 'block');
            
            //hide other types
            $('#numOfChild').css('display', 'none');
            $('.childInfo').remove();

        } else { //else hide all custom
          $('#parent').css('display', 'none');
          $('#grade').css('display', 'none');
          $('#numOfChild').css('display', 'none');
          $('.childInfo').remove();
        }

      });

    $(document).on('change', "#numOfChild select", function(){
      var numOfChild = this.value;
      var html='';


      for(var a=0; a<numOfChild; a++){

        html +='<div class="form-group childInfo row">';
        html += '<label for="" class="col-sm-4 col-form-label">Student Name</label>';
        html += '<div class="col-sm-8">';
        html += '<input class="form-check-input form-control" type="text" name="childName[]"  required>';
        html += '</div></div>';


        html +='<div class="form-group childInfo row">';
        html += '<label for="" class="col-sm-4 col-form-label">Year/Grade</label>';
        html += '<div class="col-sm-8">';
        html += '<input class="form-check-input form-control" type="number" min="1" max="12" name="childGrade[]" required>';
        html += '</div></div>';


        html +='<div class="form-group childInfo row">';
        html += '<label for="" class="col-sm-4 col-form-label">School/Tutor Link</label>';
        html += '<div class="col-sm-8">';
        html += '<input class="form-check-input form-control" type="text" name="childSCTLink[]" required>';
        html += '<hr>';
        html += '</div></div>';
      }

      $('#numOfChild').after(html);


    })

    $('#revPass').on('click', function(){
      $('#password').attr('type', 'text')
    })

    //get courses on country change
    $(document).on('change', '#country', function(){
      var countryId = $(this).val();
      var studentId = <?php echo $user_info[0]['id']; ?> 
      $.ajax({
        url:'Admin/coursesByCountry/'+countryId+'/'+studentId,
        success: function(response){
          $('#course_ul').html(response);          
        }
      })
    })


  </script>





