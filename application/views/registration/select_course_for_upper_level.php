<?php echo $header; ?>
<?php echo $header_sign_up; ?>  
<div class="container">
    <div class="row">

        <div class="col-sm-10 col-sm-offset-1">
            
            <h6 style="color: #053167;font-weight: 600;text-decoration: underline;text-align: center;padding-top: 15px;">Select Your Course</h6>
            
            <form class="ss_form text-center form-inline" method="post" action="<?php echo base_url(); ?>upper_level_student_form">
                <div class="ss_top_s_course">
                    <ul>

                        <?php
                        if ($course_details) {
                            $i = 0;
                            foreach ($course_details as $course) {
                                $i++;
                                ?>
                                <li class="text-left">
                                    <p>
                                    <p><?php echo $course['courseName'] ?><br/> 

                                        <span>$</span><?php echo $course['courseCost'] ?> 

                                       
                                    </p>										
                                    </p>
                                    <p class="text-right filled-in">
                                        <input class="form-check-input"  id="course_<?php echo $i; ?>"type="checkbox" name="course[]" value="<?php echo $course['id'] ?>" data='<?php echo $course['courseCost'] ?>' onclick="courseClick('<?php echo $course['id'] ?>');"></p>
                                </li>
                            <?php }
                        } ?>
                    </ul>
                </div>
                <?php if ($this->session->userdata('registrationType') != 'trial') { ?>
                    <div class="ss_bottom_s_course">


                        <div class="select active r1" checked data="1" onclick="myR1Func();">Per month</div>
                        <div class="select r2" data="2" onclick="myR2Func();">6 Months</div>
                        <div class="select r3" data="3" onclick="myR3Func();">1Year</div>

                        <div class="total">Total<br/><b id="dolar">$0</b></div>
                        <input type="hidden" name="paymentType" value="" id="paymentType" />
                        <input type="hidden" name="totalCost" value="" id="totalCost" />
                    </div>
                    <p class="warnin_text">“Your membership will be renewed automatically. You may cencel anytime”</p>
                <?php } ?>
                <input type="hidden" value="1" name="token">
                <div class="text-center" > 
                    <button class="btn btn_next" id="must_select"> 
                        <img src="<?php echo base_url(); ?>assets/images/icon_save.png"/>Save & Proceed
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
</div>
</section>
<?php echo $footer; ?>
<script>
<?php if ($this->session->userdata('registrationType') != 'trial') { ?>
        function myR1Func() {
            var davalue = $('.r1').attr('data');
            document.getElementById("paymentType").value = davalue;
        }
        myR1Func();
        function myR2Func() {
            var davalue2 = $('.r2').attr('data');
            document.getElementById("paymentType").value = davalue2;
        }
        function myR3Func() {
            var davalue3 = $('.r3').attr('data');
            document.getElementById("paymentType").value = davalue3;
        }

<?php } ?>

    var courseNumber = document.getElementsByName('course[]');
    var amit = 0;
    for (i = 1; i <= courseNumber.length; i++) {
        if ($("#course_" + i).is(":checked")) {
            amit++;
        }
    }
    if (amit == 0) {
        $("#must_select").attr('disabled', true);
    } else {
        $("#must_select").attr('disabled', false);
    }



    function courseClick() {

        var courseNumber = document.getElementsByName('course[]');
        var j = 0;
        var total_cost = 0;
        for (i = 1; i <= courseNumber.length; i++) {
            if ($("#course_" + i).is(":checked")) {
                var course_cost = $("#course_" + i).attr('data');
                var total_cost = parseInt(total_cost) + parseInt(course_cost);
                j++;
            }
        }

        var total_amount = total_cost;
        if (j == 0) {
            $("#must_select").attr('disabled', true);
        } else {
            $("#must_select").attr('disabled', false);
        }
<?php if ($this->session->userdata('registrationType') != 'trial') { ?>
            $('#dolar').html('$' + total_amount);
            document.getElementById("totalCost").value = total_amount;
<?php } ?>

    }


</script>