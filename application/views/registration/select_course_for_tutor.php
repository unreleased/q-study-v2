<?php echo $header; ?>
<?php echo $header_sign_up; ?>  
<div class="container">
    <div class="row">

        <div class="col-sm-10 col-sm-offset-1">
            <form class="ss_form text-center form-inline" method="post" action="<?php echo base_url(); ?>tutor_form">
                <div class="ss_top_s_course">
                    <ul>	
						<?php
                        if ($course_details) {
                            $i = 0;
                            foreach ($course_details as $course) {
                                $i++;
                                ?>
                                <li class="text-left">
                                    <p style="line-height: 18px;">
                                        <?php echo $course['courseName'] ?><br/> 

                                        <?php //if($course['courseCost']){?>
                                            <span>$</span><?php echo $course['courseCost'] ?> 
                                        <?php //}?>

                                        
                                    </p>
                                    <p class="text-right filled-in">
                                        <input class="form-check-input"  id="course_<?php echo $i; ?>"type="checkbox" name="course[]" value="<?php echo $course['id'] ?>" 
                                               data='<?php echo $course['courseCost'] ?>' onclick="courseClick('<?php echo $course['id'] ?>');">
                                    </p>
                                </li>
                            <?php }
                        } ?>
                        <!-- <li class="text-left">
                            <p>Fee</p>
                            <p><span>$</span><?php echo $course_details[0]['courseCost']; ?></p>
                            <p class="text-right filled-in">
                                <input class="form-check-input" type="hidden" name="fee" value="<?php echo $course_details[0]['courseCost']; ?>" data='' ></p>
                        </li>	-->			 	 		 		
                    </ul>
                </div>
				<?php if($course_details[0]['subscription_type'] == 1){?>
					<div class="ss_bottom_s_course">
						<div class="select active r1" checked data="1" onclick="myR1Func();">Per month</div>
						<div class="select r2" data="2" onclick="myR2Func();">6 Months</div>
						<div class="select r3" data="3" onclick="myR3Func();">1Year</div>

						<div class="total">Total<br/>
							<b id="dolar">
								<?php echo '$' . $course_details[0]['courseCost']; ?>
							</b>
						</div>
						
						<input type="hidden" name="paymentType" value="" id="paymentType" />
						<input type="hidden" name="legal_cost" value="<?php echo $course_details[0]['courseCost']; ?>" id="legal_cost" />
						<input type="hidden" name="totalCost" value="<?php echo $course_details[0]['courseCost']; ?>" id="totalCost" />
					</div>
					<p class="warnin_text">“Your membership will be renewed automatically. You may cencel anytime”</p>
				<?php }?>
                <div class="text-center" > 
                    <button class="btn btn_next" id="must_select"> 
                        <img src="<?php echo base_url(); ?>assets/images/icon_save.png"/>Save & Proceed
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
</div>
</section>

<?php echo $footer; ?>

<script>
    
    function myR1Func() {
        var davalue = $('.r1').attr('data');
        var total = $('#legal_cost').val();

        document.getElementById("paymentType").value = davalue;
        $('#dolar').html('$' + total);
        document.getElementById("totalCost").value = total * 1;
    }
    
    myR1Func();
    
    function myR2Func() {
        var total = $('#legal_cost').val();

        var davalue2 = $('.r2').attr('data');

        document.getElementById("paymentType").value = davalue2;
        $('#dolar').html('$' + total * 6);
        document.getElementById("totalCost").value = total * 6;
    }
    
    function myR3Func() {
        var davalue3 = $('.r3').attr('data');
        var total = $('#legal_cost').val();

        document.getElementById("paymentType").value = davalue3;
        $('#dolar').html('$' + total * 12);
        document.getElementById("totalCost").value = total * 12;
    }

</script>