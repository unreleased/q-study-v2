<?php echo $header; ?>
<?php echo $header_sign_up; ?>   
	<div class="container ss_reg_form">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
			  <?php if ($this->session->userdata('token_error')) {
				echo $this->session->userdata('token_error');
				$this->session->unset_userdata('token_error');
			  }?>
				<form class="row" id="tutor_form" data-parsley-validate>
					<p id='form_error'></p>
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<h3 class="g_heading">All fields are mandatory</h3>
						<div class="form-group">
							<label for="tutor_name">Tutor’s Name </label>
							<input class="form-control" type="text" id="tutor_name" name="tutor_name" required>
							<p id="error_tutor_name" style="color:red"></p>
						</div>
						<div class="form-group">
							<label for="email">Email Adress </label>
							<input class="form-control" type="email" id="email" name="email"/>

						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input class="form-control" type="password" id="password" name="password" required>
							<p id="error_pass" style="color:red"></p>
						</div>
						<div class="form-group">
							<label for="cnfpassword">Confirm Password</label>
							<input class="form-control" type="password" id="cnfpassword" name="cnfpassword" required>
							<p id="error_cnfpass" style="color:red"></p>
						</div>

						<div class="form-group">
							<label>Country: </label>
							<input class="form-control" type="text" id="country" value="<?php echo $country_db[0]['countryName'];?>" name="" readonly />
							<input type="hidden" id="country_code" />                   
						</div>

						<div class="form-group">
							<button class="btn btn_next" id="btnSave">
								<img src="<?php echo base_url();?>assets/images/icon_save.png"/>Submit
							</button>
						</div>
					</div>

					<div class="col-sm-3"></div>

				</form>

			  <br/><br/>
			</div>
		</div>
	</div>
</div>
</section>

<div id="ss_confirm_mobile"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form  method="post" id="tutor_otp_modal" data-parsley-validate>

				<div class="modal-body">
					<p>
						<img src="<?php echo base_url();?>assets/images/icon_logo_small.png">
					</p>
					<p>
						Congratulations your registration almost done. Enter the venﬁtcation code that has been send to Your mobile Number and press submit. lf the mobile number is not valid or you have not received andy code, press cancel and rer-enter your mobile number and submit
					</p>
				  
				  <br/>
				  <p id="token_error" style="color:red"></p>
					<div class="form-group">
						<label>Enter code</label>
						<input class="form-control" type="text" id="" name="random"/>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn_blue" data-dismiss="modal">Cancel</button>             
					<button class="btn btn_blue" type="button" id="tutor_otp_check">
						Submit
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script>

  /*form validation*/
	$('#tutor_form').validate({
		rules: {
			email: {
				required: true,
				email: true,
				remote: "CommonAccess/emailNotExists",
			},
			password : {
				required: true,
				minlength: 6,
			},
			cnfpassword:{
				equalTo:'#password'
			}
		},
		messages : {
			email : {
				remote : 'email already exists'     
			}
		}   
	});


  <?php if ($country_db[0]['countryCode'] !='any') { ?>
    // $("#mobile").intlTelInput({
            // initialCountry:'<?php echo $country_db[0]['countryCode'];?>',//'au',
            // allowExtensions: true,
            // autoFormat: false,
            // autoHideDialCode: false,
            // autoPlaceholder: false,
            // defaultCountry: "auto",
            // ipinfoToken: "yolo",
            // nationalMode: false,
            // numberType: "MOBILE",
          // });
  <?php } else { ?>
    // $("#mobile").intlTelInput({
      // allowExtensions: true,
      // autoFormat: false,
      // autoHideDialCode: false,
      // autoPlaceholder: false,
      // defaultCountry: "auto",
      // ipinfoToken: "yolo",
      // nationalMode: false,
      // numberType: "MOBILE",
    // });
  <?php } ?>
	$('#btnSave').click(function(e){
		e.preventDefault();
		var data=$('#tutor_form').serialize();
		$.ajax({
			type: 'ajax',
			method: 'post',
			async: false,
			dataType:'json',
			url: 'save_tutor',
			data:data,
			success: function(msg){
				if(msg=='success'){  
				  console.log('hit');               
				  window.location.href = "sure_save_tutor";
				}else{
				  $('#form_error').html(msg);
				  e.preventDefault();                                 
				}   
			}
		});     
	});

</script>
