<style>
  .slick-prev:before, .slick-next:before {
    color: #337ab7 !important;   
  }

  .slick-slide {
    margin: 0 10px;
    height: auto !important;
  }
  /* the parent */
  .slick-list {
    margin: 0 -10px;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-11">
      <div class="ss_qstudy_list">
        <div class="ss_qstudy_list_top">
          <?php if ($moduleType == 1) {?>
            <form class="form-inline" action="/action_page.php">
              <div class="form-group">
                <label for="email">Country</label>
                <input type="text" class="form-control" readonly="" value="<?php echo $user_info[0]['countryName'];?>">
              </div>
              <div class="form-group">
                <label for="sbjct">Subject</label>
                <select class="form-control" id="subjects">
                  <option value="">Select Subject</option>
                  <?php foreach ($studentSubjects as $subject) {?>
                    <option value="<?php echo $subject['subject_id']?>"><?php echo $subject['subject_name']?></option>
                    <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="chpter">Chapter</label>
                <select class="form-control" name="chapter" id="subject_chapter">
                  <option value="">Select Chapter</option>
                </select>
              </div>
              <button type="button" class="btn btn-default" id="moduleSearchBtn">Search</button>
            </form>
            <?php } else { ?>
           <script>
            $(document).ready(function(){
              getTutorials();    
            })
          </script>
                <?php } ?> 
      </div>
      <div class="ss_qstudy_list_mid">
        <div class="row">
          <div class="col-sm-4">
            <h3 style="text-align: left;"><?php echo $tutorInfo[0]['name'];?></h3>
          </div>
          <div class="col-sm-4">
            <h3>Index</h3>
          </div>
          <div class="col-sm-4 ss_qstudy_list_mid_right">

            <div class="profise_techer">
                <?php if (isset($all_module[0]['image'])) : ?>
                <img src="<?php echo 'assets/uploads/'.$all_module[0]['image']; ?>">
                <?php else : ?>
                  <img src="assets/images/default_user.jpg">
                <?php endif; ?>
              </div>
            </div>
            <?php $qstudyEveryday = ($tutorInfo[0]['user_type']==7 && $moduleType==2) ? 1:0 ?>    
            <?php if ($moduleType==1 || ($qstudyEveryday)) : ?>
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 mySlick">
                  <span class="badge badge-pill badge-primary" id="subjectName" subjectId="<?php echo 'all'; ?>" style="margin:5px 5px 5px 5px; cursor: pointer;">
                    All
                  </span>
                  <?php foreach ($studentSubjects as $subject) : ?>
                    <span class="badge badge-pill badge-primary" id="subjectName" subjectId="<?php echo $subject['subject_id'] ?>" style="margin:5px 5px 5px 5px; cursor: pointer;"><?php echo $subject['subject_name'] ?></span>
                    <?php endforeach; ?>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="tab-content">
          <h5 class="text-center" style="color:red;"><?php echo  isset($_SESSION['message_name']) ? $_SESSION['message_name']: '';?></h5>
          <div class="ss_qstudy_list_bottom tab-pane active" id="all_list" role="tabpanel">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Module Name</th>
                    <th>Tracker Name</th>
                    <th>Individual Name</th>
                    <th>Subject</th>
                    <th>Chapter</th>
                  </tr>
                </thead>

                <tbody id="moduleTable">

                </tbody>
              </table>

            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>


<script>
  /*set chapters according to subject*/
  $(document).on('change', '#subjects', function(){
    var subjectId = $(this).val();
    $.ajax({
      url:'Student/renderedChapters/'+subjectId,
      method: 'POST',
      success: function(data){
        $('#subject_chapter').html(data);
      }
    })
  });

  $(document).on('click', '#moduleSearchBtn', function(event){
    event.preventDefault();
    var chapterId = $("#subject_chapter :selected").val();
    var subjectId = $("#subjects :selected").val();
    var tutorId  = <?php echo $tutorInfo[0]['id']; ?>;
    var moduleType = <?php echo $moduleType; ?>;
    $.ajax({
      url: 'Student/studentsModuleByQStudy',
      method: 'POST',
      data: {
        chapterId: chapterId, 
        subjectId: subjectId, 
        tutorId: tutorId, 
        moduleType: moduleType
      },
      beforeSend: function() {
        $.LoadingOverlay("show");
      },
      success: function(data) {
        $('#moduleTable').html(data);
      }
    });
  });

</script>

<script>
  function getTutorials() {
    var tutorId  = <?php echo $tutorInfo[0]['id']; ?>;
    var moduleType = <?php echo $moduleType; ?>;
    
    $.ajax({
      url: 'Student/studentsModuleByQStudy',
      method: 'POST',
      data: {
        tutorId:tutorId, 
        moduleType:moduleType
      },
      success: function(data){
        $('#moduleTable').html(data);
      }
    });
  }
  
  function get_permission(module_id) {
    var moduleType = <?php echo $moduleType; ?>;
    var first_module_id = $("#first_module_id").val();
    
    var flag = 0;
    
    if(moduleType != 1) {
      if(module_id == first_module_id) {
        flag = 1;
      } else {
        alert('Please Complete First Lesson');
      }
    } else {
      flag = 1;
            //window.location.href = 'get_tutor_tutorial_module/'+module_id+'/1';
          }
          
          if(flag == 1) {
            $.ajax({
              type: 'POST',
              url: 'student/get_permission',
              data: {
                module_id : module_id
              },
              dataType: 'html',
              success: function (results) {
                    //console.log(results);
                    if(results == 1) {
                      alert('Please Complete Wrong Question Answer');
                    } else {
                      window.location.href = results;
                    }
                  }
                });
          }
          
          
        }
        
        $(document).on('click', '#subjectName', function(){
          var subjectId = $(this).attr('subjectId');
          var tutorId  = <?php echo $tutorInfo[0]['id']; ?>;
          var moduleType = <?php echo $moduleType; ?>;
          $.ajax({
            url: 'Student/studentsModuleByQStudy',
            method: 'POST',
            data: {
                //chapterId:chapterId,
                subjectId : subjectId, 
                tutorId : tutorId, 
                moduleType : moduleType
              },
              success: function(data){
                $('#moduleTable').html(data);
              }
            })
        })

        //slick slider on course/subject name
        $(document).ready(function(){
          $('.mySlick').slick({
           infinite: true,
           slidesToShow: 3,
           slidesToScroll: 3,
           adaptiveHeight: true
         });
        });

      </script>
