<?php
date_default_timezone_set('Asia/Dhaka');
$module_time = time();
$key = $question_info_s[0]['question_order'];
if ($tutorial_ans_info) {
    $temp_table_ans_info = json_decode($tutorial_ans_info[0]['st_ans'], true);
    $desired = $temp_table_ans_info;
} else {
    $desired = $this->session->userdata('data');
}
//    echo '<pre>';print_r($desired);die;
//    For Question Time
$question_time = explode(':', $question_info_s[0]['questionTime']);

$hour = 0;
$minute = 0;
$second = 0;
if (is_numeric($question_time[0])) {
    $hour = $question_time[0];
} if (is_numeric($question_time[1])) {
    $minute = $question_time[1];
} if (is_numeric($question_time[2])) {
    $second = $question_time[2];
}

$question_time_in_second = ($hour * 3600) + ($minute * 60) + $second;

//    End For Question Time

$link_next = "javascript:void(0);";
$link = "javascript:void(0);";

if (is_array($desired)) {
    $link_key = $key - 1;
    if (array_key_exists($link_key, $desired) && !$tutorial_ans_info) {
        $link = $desired[$link_key]['link'];
    }
    $link_key_next = $key;
    if (array_key_exists($link_key_next, $desired) && !$tutorial_ans_info) {
        $question_id = $question_info_s[0]['question_order'] + 1;
        $link1 = base_url();
        $link_next = $link1 . 'get_tutor_tutorial_module/' . $question_info_s[0]['module_id'] . '/' . $question_id;
    }
}

$module_type = $question_info_s[0]['moduleType'];

//    For Algorithm only
if (isset($question_info['item'])) {
    end($question_info['item']);
    $last_item_index = key($question_info['item']);
}

 $videoName = strlen($module_info[0]['video_name'])>1 ? $module_info[0]['video_name'] : 'Instruction';
?>

<style>
    .operator_div{
        display: inline-block;
        position: relative;
        width: 0;
    }

    .operator_div span{
        position: absolute;
        bottom: -2px;
        left: -10px;
    }
</style>

<!--         ***** Only For Special Exam *****         -->
<?php if ($module_type == 3) { ?>
    <input type="hidden" id="exam_end" value="<?php echo strtotime($module_info[0]['exam_end']); ?>" name="exam_end" />
    <input type="hidden" id="now" value="<?php echo $module_time; ?>" name="now" />
    <input type="hidden" id="optionalTime" value="<?php echo $module_info[0]['optionalTime']; ?>" name="optionalTime" />
    <input type="hidden" id="exact_time" value="<?php echo $this->session->userdata('exact_time'); ?>" />
<?php } ?>

<!--         ***** For Tutorial & Everyday Study *****         -->    
<?php if ($module_type == 2 || $module_type == 1) { ?>
    <input type="hidden" id="exam_end" value="" name="exam_end" />
    <input type="hidden" id="now" value="<?php echo $module_time; ?>" name="now" />
    <input type="hidden" id="optionalTime" value="<?php echo $question_time_in_second; ?>" name="optionalTime" />
    <input type="hidden" id="exact_time" value="<?php echo $this->session->userdata('exact_time'); ?>" />
<?php } ?>


<div class="ss_student_board">
    <div class="ss_s_b_top">
    <div class="ss_index_menu <?php //if ($module_type == 3) {
    ?>col-md-3<?php
    //}?>">
    <?php if ($module_type == 1) { ?>
      <a href="all_tutors_by_type/<?php echo $total_question[0]['user_id'];?>/<?php echo $total_question[0]['moduleType'];?>" style="display: inline-block;">Index</a>
    <?php } else {?>
      <!-- <a >Index</a> -->
    <?php }?>
    <button class="btn btn_next" id="openVideo" ><i class="fa fa-play" style="color:#35B6E7;margin-right: 5px;"></i><?php echo $videoName;  ?></button>
  </div>
    <?php if ($module_type == 3 || (($module_type == 2 || $module_type == 1) && $question_time_in_second != 0)) { ?>
    <div class="col-sm-3" style="text-align: right">
      <div class="ss_timer" id="demo"><h1>00:00:00 </h1></div>
    </div>
    <?php }?>

  <div style="text-align: center;" class="text-center col-sm-6<?php //if ($module_type != 3) {
    //echo 'col-sm-7';
    //} else {
      //echo 'col-sm-6';
    //}?> ss_next_pre_top_menu">
    <?php if ($question_info_s[0]['isCalculator']) : ?>
      <input type="hidden" name="" id="scientificCalc">
    <?php endif; ?>
    <?php if ($_SESSION['userType']==3||$_SESSION['userType']==7) :
        ?> <!-- only tutor&qstudy will be able to back next -->
        <?php if ($question_info_s[0]['moduleType'] == 1) { ?>
        <a class="btn btn_next" href="<?php echo $link; ?>"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
        <a class="btn btn_next" href="<?php echo $link_next; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>                                  
        <?php } else {?>
        <a class="btn btn_next" href="javascript:void(0);"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
        <a class="btn btn_next" href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
        <?php }?>
    <?php endif; ?>

    <a class="btn btn_next" id="draw" onClick="test()" data-toggle="modal" data-target=".bs-example-modal-lg" >
     Workout <img src="assets/images/icon_draw.png">
   </a>
 </div>
</div>
    <div class="container-fluid">
        <form id="answer_form">

            <input type="hidden" value="<?php echo $question_info_s[0]['question_id']; ?>" name="question_id" id="question_id">

            <?php // if (array_key_exists($key, $total_question) && !$tutorial_ans_info) {  ?>
            <?php if ((count($total_question) - 1) > count($this->session->userdata('data')) && !$tutorial_ans_info) { ?>
                <input type="hidden" id="next_question" value="<?php echo $question_info_s[0]['question_order'] + 1; ?>" name="next_question" />
            <?php } else { ?>
                <input type="hidden" id="next_question" value="0" name="next_question" />
            <?php } ?>
            <input type="hidden" id="module_id" value="<?php echo $question_info_s[0]['module_id'] ?>" name="module_id">
            <input type="hidden" id="current_order" value="<?php echo $key; ?>" name="current_order">   
            <input type='hidden' id="module_type" value="<?php echo $question_info_s[0]['moduleType']; ?>" name='module_type'>

            <input type='hidden' id="student_question_time" value="" name='student_question_time'>

            <div class="row">
                <div class="ss_s_b_main" style="min-height: 70vh">
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span>
                                                <img src="assets/images/icon_draw.png"> Instruction
                                            </span> Question
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="math_plus" id="">
                                        <?php echo $question_info['questionName']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4" style="text-align: center">

                       <!--  <strong class="text-center">
                           <a href="javascript:void(0)" onClick="test()" data-toggle="modal" data-target=".bs-example-modal-lg" style="text-decoration: underline;padding:10px;" >Workout</a>
                       </strong> -->

                        <div class="row">

                            <div class="col-sm-12 times_table_div" style="<?php if ($question_info['operator'] != '/') {
                                ?>text-align: center<?php
} ?>">
                                <div style="font-size: 30px;">
                                    <?php if ($question_info['operator'] != '/') { ?>
                                        <div id="quesBody" style="padding: 0px 10px;border-bottom: 2px solid black;text-align: right;margin-bottom: 10px;">
                                            <?php $i = 1;
                                            foreach ($question_info['item'] as $row) {
                                                if ($i == $last_item_index) {
                                                    ?>
                                                    <div class="operator_div">
                                                        <span><?php echo $question_info['operator'] ?></span>
                                                    </div>
                                                <?php }
                                                foreach ($row as $key_data) {
                                                    ?>
                                                    <span><?php echo $key_data; ?></span>
                                                <?php } ?><br>
                                                <?php $i++;
                                            } ?>
                                        </div>
                                        <input type="text" class="form-control" id="" name="answer" autocomplete="off" autofocus style="font-size: 30px;">
                                    <?php } if ($question_info['operator'] == '/') { ?>
                                        <div style="display: block;margin-top: 55px;">
                                            <div class="form-group" style="float: left;">
                                                <input type="text" class="form-control" id="" name="answer[]" autocomplete="off" autofocus style="font-size: 30px;max-width: 160px !important">
                                            </div>
                                            <div class="form-group" style="float: left;margin-left: 30px;">
                                                <label>R</label>
                                                <input type="text" class="form-control" id="" name="answer[]" autocomplete="off" autofocus style="font-size: 30px;">
                                            </div>

                                        </div>

                                        <div>
                                            <div id="quesBody" style="float: left;padding: 5px;">
                                                <?php
                                                foreach ($question_info['divisor'] as $divisor) {
                                                    echo $divisor;
                                                }
                                                ?><span class="dividend">
                                                    <?php
                                                    foreach ($question_info['dividend'] as $dividend) {
                                                        echo $dividend;
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                            <!--<div style="float: left;padding: 5px;background-image: url('assets/images/44.png');padding: 3px 5px 6px 17px;background-repeat: no-repeat;background-position: 0px 0px;min-height: 41px;">
                                            <?php
                                            foreach ($question_info['dividend'] as $dividend) {
                                                echo $dividend;
                                            }
                                            ?>
                                            </div>-->
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-12" style="text-align: center;margin-top: 30px;">
                            <button class="btn btn_next" id="answer_matching" type="submit">Submit</button>
                        </div> 
                        
                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
                                            <span>Module Name: <?php echo isset($module_info[0]['moduleName'])?$module_info[0]['moduleName']:'Not found'; ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class=" ss_module_result">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>    
                                                        <tr>
                                                            <th></th>
                                                            <th>SL</th>
                                                            <th>Mark</th>
                                                            <th>Obtained</th>
                                                            <th>Description</th>                                                    
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1;
                                                        $total = 0;
                                                        foreach ($total_question as $ind) {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php if (isset($desired[$ind['question_order']]['ans_is_right']) && $ind['question_order'] == $desired[$ind['question_order']]['question_order_id']) {
                                                                        if ($desired[$ind['question_order']]['ans_is_right'] == 'correct') {
                                                                            ?>
                                                                            <span class="glyphicon glyphicon-ok" style="color: green;"></span>
                                                                        <?php } else { ?>
                                                                            <span class="glyphicon glyphicon-remove" style="color: red;"></span>
                                                                        <?php }
} ?>
                                                                </td>
                                                                <td style="<?php if ($question_info_s[0]['question_order'] == $ind['question_order']) {
                                                                    echo 'background-color: #99D9EA;';
} ?>">
                                                            <?php echo $ind['question_order']; ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    echo $ind['questionMarks'];
                                                                    $total = $total + $ind['questionMarks'];
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    if (isset($desired[$ind['question_order']]['student_question_marks'])) {
                                                                        echo $desired[$ind['question_order']]['student_question_marks'];
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td><a  class="text-center" onclick="showModalDes(<?php echo $i; ?>);"><img src="assets/images/icon_details.png"></a></td>
                                                            </tr>
                                                            <?php $i++;
                                                        } ?>
                                                        <tr>
                                                            <td colspan="2">Total</td>
                                                            <td colspan="3"><?php echo $total ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                        
                        </div>
                    </div>  

                    <div class="col-sm-4" id="draggable" style="display: none;">
                        <div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
                                    </h4>
                                </div>
                                <div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <textarea name="workout" class="mytextarea"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-12" style="text-align: center;">
                    <!--<a class="btn btn_next" id="answer_matching">Submit</a>-->
                </div>

            </div>
        </form>
    </div>
</div>


<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_sucess.png" class="pull-left"> 
                <span class="ss_extar_top20">Your answer is correct</span>
            </div>
            <div class="modal-footer">
                <button id="get_next_question" type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>        
            </div>
        </div>
    </div>
</div>

<div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <i class="fa fa-close" style="font-size:20px;color:red"></i> <span class="ss_extar_top20">Your answer is wrong</span>
                <br><?php echo strip_tags($question_info_s[0]['question_solution']); ?>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
            </div>
        </div>
    </div>
</div>

<div class="modal fade ss_modal" id="times_up_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">Times Up</h4>
            </div>
            <div class="modal-body row">
                <i class="fa fa-close" style="font-size:20px;color:red"></i> 
                <br><?php echo strip_tags($question_info_s[0]['question_solution']); ?>  
            </div>
            <div class="modal-footer">
                <button type="button" id="question_reload" class="btn btn_blue" data-dismiss="modal">close</button>         
            </div>
        </div>
    </div>
</div>

<?php $i = 1;
foreach ($total_question as $ind) {
    ?>
    <div class="modal fade ss_modal ew_ss_modal" id="show_description_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel"> Question Description </h4>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" name="questionDescription"><?php echo $ind['questionDescription']; ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php $i++;
} ?>

<script>

    var time_count = 0;

    $("#answer_form").on('submit', function (e) {
        e.preventDefault();
        var workout_val = CKEDITOR.instances['workout'].getData();
        if(workout_val == ''){
            alert('You have to do workout first');
        } else {
            var form = $("#answer_form");
            $.ajax({
                type: 'POST',
                url: 'st_answer_algorithm',
                data: form.serialize(),
                dataType: 'html',
                success: function (results) {
    //                alert(results);
                    if (results == 6) {
                        window.location.href = 'show_tutorial_result/' + $("#module_id").val();
                    }
                    if (results == 3) {
                        $('#ss_info_worng').modal('show');
                    }
                    if (results == 2) {
                        $('#ss_info_sucesss').modal('show');
                        $('#get_next_question').click(function () {
                            commonCall();
                        });
                    }
                    if (results == 5) {
                        commonCall();
                    }

                }
            });
        }

    });

    function commonCall() {
        $question_order = $('#next_question').val();
        $module_id = $('#module_id').val();

        <?php if ($tutorial_ans_info) { ?>
            window.location.href = 'show_tutorial_result/' + $module_id;
        <?php } ?>
        if ($question_order == 0) {
            window.location.href = 'show_tutorial_result/' + $module_id;
        }
        if ($question_order != 0) {
            window.location.href = 'get_tutor_tutorial_module/' + $module_id + '/' + $question_order;
        }
    }

    function showModalDes(e)
    {
        $('#show_description_' + e).modal('show');
    }

</script>

<script>

    function takeDecesion() {

        var exact_time = $('#exact_time').val();
        var countDownDate = $('#exam_end').val();


        var now = $('#now').val();
        var opt = $('#optionalTime').val();
        var h1 = document.getElementsByTagName('h1')[0];


        var distance = countDownDate - now;
        var hours = Math.floor(distance / 3600);

        var x = distance % 3600;

        var minutes = Math.floor(x / 60);

        var seconds = distance % 60;

        var t_h = hours * 60 * 60;
        var t_m = minutes * 60;
        var t_s = seconds;

        var total = parseInt(t_h) + parseInt(t_m) + parseInt(t_s);

        var remaining_time;
        var end_depend_optional = parseInt(exact_time) + parseInt(opt);

        if (opt > total) {
            remaining_time = total;
        } else {
            remaining_time = parseInt(end_depend_optional) - parseInt(now);
        }

        setInterval(circulate, 1000);

        function circulate() {
            time_count++;
            remaining_time = remaining_time - 1;

            var v_hours = Math.floor(remaining_time / 3600);
            var remain_seconds = remaining_time - v_hours * 3600;
            var v_minutes = Math.floor(remain_seconds / 60);
            var v_seconds = remain_seconds - v_minutes * 60;

            $("#student_question_time").val(time_count);

            if (remaining_time > 0) {
                h1.textContent = v_hours + " : " + v_minutes + " : " + v_seconds + "  ";
            } else {
                var form = $("#answer_form");
                $.ajax({
                    type: 'POST',
                    url: 'st_answer_algorithm',
                    data: form.serialize(),
                    dataType: 'html',
                    success: function (results) {
                        window.location.href = 'show_tutorial_result/' + $('#module_id').val();
                    }
                });
                h1.textContent = "EXPIRED";
            }
        }

    }

<?php if ($module_type == 3) { ?>
        takeDecesion();

<?php } ?>
</script>


<script>
    function takeDecesionForQuestion() {

        var exact_time = $('#exact_time').val();

        var now = $('#now').val();
        var opt = $('#optionalTime').val();
        var h1 = document.getElementsByTagName('h1')[0];

        var countDownDate = parseInt(now) + parseInt($('#optionalTime').val());

        var distance = countDownDate - now;
        var hours = Math.floor(distance / 3600);
//        alert(distance)
        var x = distance % 3600;

        var minutes = Math.floor(x / 60);

        var seconds = distance % 60;

        var t_h = hours * 60 * 60;
        var t_m = minutes * 60;
        var t_s = seconds;

        var total = parseInt(t_h) + parseInt(t_m) + parseInt(t_s);

        var remaining_time;
        var end_depend_optional = parseInt(exact_time) + parseInt(opt);

        if (opt > total) {
            remaining_time = total;
        } else {
            remaining_time = parseInt(end_depend_optional) - parseInt(now);
        }

        setInterval(circulate1, 1000);

        function circulate1() {
            time_count++;
            remaining_time = remaining_time - 1;

            var v_hours = Math.floor(remaining_time / 3600);
            var remain_seconds = remaining_time - v_hours * 3600;
            var v_minutes = Math.floor(remain_seconds / 60);
            var v_seconds = remain_seconds - v_minutes * 60;

            $("#student_question_time").val(time_count);

            if (remaining_time > 0) {
                h1.textContent = v_hours + " : " + v_minutes + " : " + v_seconds + "  ";
            } else {
                var form = $("#answer_form");
                $.ajax({
                    type: 'POST',
                    url: 'st_answer_algorithm',
                    data: form.serialize(),
                    dataType: 'html',
                    success: function (results) {
                        if (results == 3) {
                            $('#times_up_message').modal('show');
                            $('#question_reload').click(function () {
                                location.reload();
                            });
                        }
                        if (results == 2) {
                            $('#ss_info_sucesss').modal('show');
                            $('#get_next_question').click(function () {
                                commonCall();
                            });
                        }
                    }
                });
                h1.textContent = "EXPIRED";
            }
        }

    }

<?php if (($module_type == 1 || $module_type == 2) && $question_time_in_second != 0) { ?>
        takeDecesionForQuestion();
<?php } ?>
</script>
<?php $this->load->view('students/question_module_type_tutorial/module_video'); ?>
<?php $this->load->view('students/question_module_type_tutorial/drawingBoard'); ?>
