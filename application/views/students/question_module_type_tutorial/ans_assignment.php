<?php 
    $key = $question_info_s[0]['question_order'];
//    echo '<pre>';print_r(($total_question));
//    echo '<pre>';print_r($question_info_s[0]);die;
    date_default_timezone_set('Asia/Dhaka');
    $module_time = time();
    
    if($tutorial_ans_info){
        $temp_table_ans_info = json_decode($tutorial_ans_info[0]['st_ans'],TRUE);
        $desired = $temp_table_ans_info;
    }else{
        $desired = $this->session->userdata('data');
    }
    
//    For Question Time
    $question_time = explode(':',$question_info_s[0]['questionTime']);
    $hour = 0;
    $minute = 0;
    $second = 0;
    if(is_numeric($question_time[0])) {
        $hour = $question_time[0];
    } if(is_numeric($question_time[1])) {
        $minute = $question_time[1];
    } if(is_numeric($question_time[2])) {
        $second = $question_time[2];
    }

    $question_time_in_second = ($hour * 3600) + ($minute * 60) + $second ;

//    End For Question Time

    $link_next = null;
    if (is_array($desired)) {
        $link_key = $key - 1;
        if (array_key_exists($link_key, $desired)) {
            $link = $desired[$link_key]['link'];
        }
        $link_key_next = $key;
        if (array_key_exists($link_key_next, $desired)) {
            $question_id = $question_info_s[0]['question_order'] + 1;
            $link1 = base_url();
            $link_next = $link1 . 'get_tutor_tutorial_module/' . $question_info_s[0]['module_id'] . '/' . $question_id;
        }
    }

    $module_type = $question_info_s[0]['moduleType'];
?>
<div class="ss_student_board">
    <div class="ss_s_b_top">
        <div class="ss_index_menu <?php if ($module_type == 3) { ?>col-md-2<?php }?>">
            <?php if ($module_type == 1) { ?>
            <a href="all_module_by_type/<?php echo $total_question[0]['user_type'];?>/<?php echo $total_question[0]['moduleType'];?>">Index</a>
            <?php }else {?>
            <a >Index</a>
            <?php }?>
        </div>
        <?php if ($module_type == 3 || (($module_type == 2 || $module_type == 1) && $question_time_in_second != 0)) { ?>
            <div class="col-sm-4" style="text-align: right">
                <div class="ss_timer" id="demo"><h1>00:00:00 </h1></div>
            </div>
        <?php }?>
        
        <div class="<?php if ($module_type != 3) {echo 'col-sm-7'; }else{echo 'col-sm-6';}?> ss_next_pre_top_menu">
        <?php if ($question_info_s[0]['moduleType'] == 1) { ?>
            
            <a class="btn btn_next" href="<?php echo $link; ?>"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
            <a class="btn btn_next" href="<?php echo $link_next; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>									
        <?php }else{?>
            <a class="btn btn_next" href="javascript:void(0);"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
            <a class="btn btn_next" href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
        <?php }?>
            <a class="btn btn_next" id="draw" onClick="test()" data-toggle="modal" data-target=".bs-example-modal-lg">
               Draw <img src="assets/images/icon_draw.png">
            </a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <form id="answer_form">
                
                <input type="hidden" value="<?php echo $question_info_s[0]['question_id']; ?>" name="question_id" id="question_id">

                 <?php if ((count($total_question)-1) > count($this->session->userdata('data')) && !$tutorial_ans_info) {?>
                    <input type="hidden" id="next_question" value="<?php echo $question_info_s[0]['question_order'] + 1; ?>" name="next_question" />
                <?php } else { ?>
                    <input type="hidden" id="next_question" value="0" name="next_question" />
                <?php } ?>
                <input type="hidden" id="module_id" value="<?php echo $question_info_s[0]['module_id'] ?>" name="module_id">
                <input type="hidden" id="current_order" value="<?php echo $key; ?>" name="current_order">	
                <input type='hidden' id="module_type" value="<?php echo $question_info_s[0]['moduleType']; ?>" name='module_type'>

                <input type='hidden' id="student_question_time" value="" name='student_question_time'>

                
                <div class="ss_s_b_main" style="min-height: 100vh">
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span><img src="assets/images/icon_draw.png"> Instruction</span> Question
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <?php echo isset($questionBody)?$questionBody:''; ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">   Answer</a>
                                    </h4>
                                </div>
                                <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <textarea name="answer" class="student_assignment_textarea"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">   
                            <button type="button" class="btn btn_next" id="answer_matching">submit</button>
                        </div>                                  
                        <div class="col-sm-4"></div>
                    </div>

                    <div class="col-sm-4">

                        <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsefour" aria-expanded="true" aria-controls="collapseOne">  
                                            <span>Module Name: <?php echo $question_info_s[0]['moduleName'];?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsefour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">

                                        <div class=" ss_module_result">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>    
                                                        <tr>

                                                            <th>SL</th>
                                                            <th>Mark</th>
                                                             <th>Obtain</th> 
                                                            <th>Description</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody id="assListTbl">
                                                        <?php echo $assignment_list; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Save Sucessfully</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<!-- question details modal -->
<div class="modal fade" id="quesDtlsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Question Details</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body qDtlsModBody">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- assignment file submit success modal -->

<!-- Modal -->
<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_details.png" class="pull-left"> 
                <span class="ss_extar_top20" style="display: contents;">Examiner will scrutinise your work and go back to you.</span> 
            </div>
            <div class="modal-footer">
                <button type="button" id="get_next_question" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.qDtlsOpenModIcon').on('click', function(){
        var hiddenTaskDesc = $(this).closest('tr').children('#hiddenTaskDesc').val();
        $('.qDtlsModBody').html(hiddenTaskDesc);
    });
    
    
//    $('#answer_matching').click(function () {
//        $('#ss_info_sucesss').modal('show');
//    });
</script>


<script>
    var time_count = 0;
    
    $('#answer_matching').click(function () {

        var form = $("#answer_form");
        $.ajax({
            type: 'POST',
            url: 'st_answer_assignment',
            data: form.serialize(),
            dataType: 'html',
            success: function (results) {
                if (results == 2) {
                    $('#ss_info_sucesss').modal('show');
                    $('#get_next_question').click(function () {
                        commonCall();
                    });
                }
            }
        });

    });
    
    function commonCall() {
        $question_order = $('#next_question').val();
        $module_id = $('#module_id').val();

        if ($question_order == 0) {
            window.location.href = 'show_tutorial_result/' + $module_id ;
        }
        if ($question_order != 0) {
            window.location.href = 'get_tutor_tutorial_module/' + $module_id + '/' + $question_order;
        }
    }
    
    get_student_taken_time();
	
    function get_student_taken_time() {
        setInterval(circulate,1000);

        function circulate() {
            time_count++;
            $("#student_question_time").val(time_count);
        }
    }
</script>
