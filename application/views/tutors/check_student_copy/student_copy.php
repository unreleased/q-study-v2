<?php

	$key = $question_info_s[0]['question_order'];

	$temp_table_ans_info = isset($tutorial_ans_info[0]['st_ans']) ? json_decode($tutorial_ans_info[0]['st_ans'], true):[];
	$desired = $temp_table_ans_info;
	
	// echo '<pre>';print_r($desired);die;
	
	$link_next = "javascript:void(0);";
	$link = "javascript:void(0);";

	$lastElement = end($desired);

	//Print it out!
	//print_r($lastElement);
	if (is_array($desired)) {
		$link1 = base_url();
		$link_key = $key - 1;
		if (array_key_exists($link_key, $desired)) {
			$link = $link1 . 'check_student_copy/' . $question_info_s[0]['module_id'] . '/' . $user_info[0]['id']. '/' . $link_key;
		}
		$link_key_next = $key;
		if (array_key_exists($link_key_next, $desired) && $lastElement['question_order_id'] != $key) {
			$question_order = $question_info_s[0]['question_order'] + 1;
			
			$link_next = $link1 . 'check_student_copy/' . $question_info_s[0]['module_id'] . '/' . $user_info[0]['id']. '/' . $question_order;
		}
	}

	$module_type = $question_info_s[0]['moduleType'];
?>

<div class="ss_student_board">
	<div class="ss_s_b_top">
		<div class="ss_index_menu <?php if ($module_type == 3) { ?>col-md-2<?php }?>">
			<?php if ($module_type == 1) { ?>
				<a href="all_module_by_type/<?php echo $total_question[0]['user_type'];?>/<?php echo $total_question[0]['moduleType'];?>">Index</a>
			<?php } else {?>
				<a >Index</a>
			<?php }?>
		</div>
    
<!--        <div class="col-sm-4" style="text-align: right">
            <div class="ss_timer" id="demo"><h1>00:00:00 </h1></div>
          </div>-->
          
        <div class="col-sm-7 ss_next_pre_top_menu">

            <a class="btn btn_next" href="<?php echo $link; ?>"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
            <a class="btn btn_next" href="<?php echo $link_next; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>                                  
            
            <a class="btn btn_next" id="draw" onClick="show_workout()">
             Draw <img src="assets/images/icon_draw.png">
           </a>
        </div>
    </div>
       
       
       
    <div class="container-fluid">
        <div class="row">
			<div class="ss_s_b_main" style="min-height: 100vh">

				<?php echo $question_box;?>
            
				<div class="col-sm-4">
					<div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  
									<span>Module Name: <?php echo $question_info_s[0]['module_type'] ?></span></a>
								</h4>
							</div>
							<div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class=" ss_module_result">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>    
													<tr>
														<th></th>
														<th>SL</th>
														<th>Mark</th>
														<th>Obtained</th>
														<th>Description</th>                                                    
													</tr>
												</thead>
												<tbody>
												<?php $i = 1;
												$total = 0;
												$totalObtained = 0;
												foreach ($total_question as $ind) { ?>
													<tr>
														<td>
															<?php if(isset($desired[$ind['question_order']]['ans_is_right']) && $ind['question_order'] == $desired[$ind['question_order']]['question_order_id'] && ($ind['question_type'] != 12 && $ind['question_type'] != 11)){

															if($desired[$ind['question_order']]['ans_is_right'] == 'correct'){?>
																<a href="check_student_copy/<?=$question_info_s[0]['module_id']?>/<?=$user_info[0]['id']?>/<?=$ind['question_order']?>">
																  <span class="glyphicon glyphicon-ok" style="color: green;"></span>
																</a>
															<?php } else if($desired[$ind['question_order']]['ans_is_right'] == 'wrong') {?>
																<a href="check_student_copy/<?=$question_info_s[0]['module_id']?>/<?=$user_info[0]['id']?>/<?=$ind['question_order']?>">
																  <span class="glyphicon glyphicon-remove" style="color: red;"></span>
																</a>
															<?php }} if(isset($desired[$ind['question_order']]['ans_is_right']) && ($ind['question_type'] == 12 || $ind['question_type'] == 11)) {?>
																<a href="check_student_copy/<?=$question_info_s[0]['module_id']?>/<?=$user_info[0]['id']?>/<?=$ind['question_order']?>">
																  <span class="glyphicon glyphicon-pencil" style="color: #fb9c08;"></span>
																</a>
															<?php }?>
														</td>
														<td style="<?php if ($question_info_s[0]['question_order'] == $ind['question_order']) {
														  echo 'background-color: #99D9EA;';
														}?>">
															<?php echo $ind['question_order']; ?>
														</td>
														<td>
															<?php
															echo $ind['questionMarks'];
															$total = $total + $ind['questionMarks'];
															
															?>
														</td>
														<td>
															<?php 
																if(isset($desired[$ind['question_order']]['student_question_marks'])){
																	echo $desired[$ind['question_order']]['student_question_marks'];
																	$totalObtained += $desired[$ind['question_order']]['student_question_marks'];
																} 
															?>
														</td> <!-- obtained -->
														<td>
															<a  class="text-center" onclick="showModalDes(<?php echo $i; ?>);">
																<img src="assets/images/icon_details.png">
															</a>
														</td>
													</tr>
												<?php $i++; } ?>
													<tr style="background-color: #99D9EA;">
														<td colspan="2">Total</td>
														<td colspan="1"><?php echo $total?></td>
														<td colspan="1"><?php echo $totalObtained;?></td>
														<td colspan="1"></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>                                                        
					</div>
				</div>
				
				<?php if($question_info_s[0]['question_type'] == 9) {?>
                <div class="col-sm-12">
                    <div class="row">
                        <?php
                        $st_ans = json_decode($tutorial_ans_info[0]['st_ans'], TRUE);
                        $question_order = $question_info_s[0]['question_order'];
                        $student_ans = json_decode($st_ans[$question_order]['student_ans'], TRUE);
                        $question_info = json_decode($question_info_s[0]['questionName'], TRUE);
                        
                        $answer = array_combine(range(1, count($student_ans)), array_values($student_ans));
                        $question_info['sentence'] = array_combine(range(1, count($question_info['sentence'])), array_values($question_info['sentence']));
                        $ques_description = array_combine(range(1, count($question_description)), array_values($question_description));
                        $ques_answer = array_flip($answer);

//                         echo '<pre>';print_r($student_ans);
                //         echo '<pre>';print_r($answer);
//                         echo '<pre>';print_r($ques_description);
//                         echo '<pre>';print_r($question_answer);
                        ?>
                        <div class="col-sm-6">
                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-10">
                                    <a><button class="btn btn-default">Wrong Choice</button></a>
                                </div>
                                <div class="col-sm-1">

                                </div>
                            </div>

                            <?php $i = 0;
                            foreach ($student_ans as $key => $sentence) {
                                if((isset($question_answer[$i]) && $question_answer[$i] != $sentence) || !array_key_exists($key, $question_answer)){?>
                            
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-xs-2 col-sm-1">
                                        <span class="fa fa-times" style="line-height: 38px;color: red;"></span>
                                    </div>
                                    <div class="col-xs-8 col-sm-10">
                                        <span style="border: 1px solid black;padding: 10px;display: block">
                                            <?php echo $question_info['sentence'][$sentence];?>
                                        </span>
                                    </div>
                                    <div class="col-xs-2 col-sm-1">
                                        <span class="fa fa-exclamation-circle" data-toggle="modal" data-target="#correct_ans<?=$sentence?>" style="line-height: 38px;"></span>
                                    </div>
                                </div>
                            
                                <div class="modal fade ss_modal" id="correct_ans<?=$sentence?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                                            </div>
                                            <div class="modal-body row">
                                                <!--<img src="assets/images/icon_sucess.png" class="pull-left">--> 
                                                <span class="ss_extar_top20">
                                                    <?php if($ques_description[$sentence]){
                                                        echo $ques_description[$sentence];
                                                    } else {
                                                        echo 'This sentence is not in order';
                                                    }?>
                                                </span> 
                                            </div>
                                            <div class="modal-footer">
                                                <button id="get_next_question" type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php }?>
                            <?php $i++;} ?>
                        </div>

                        <div class="col-sm-6">
                            <div id="set_sentence" style="font-size: 13px;line-height: 2;">
                                <?php $i = 0;
                                foreach ($student_ans as $key => $sentence) {?>
                                    <span id="order_" style="<?php if((isset($question_answer[$i]) && $question_answer[$i] != $sentence) || !array_key_exists($key, $question_answer)){echo 'color: red;';}?>"><?php echo $question_info['sentence'][$sentence] ?></span>
                                <?php $i++;} ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php }?>

				<div class="col-sm-4" id="workout" style="display: none;">
				  <div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
					  <div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						  <a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
						</h4>
					  </div>
					  <div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						  <div class=" math_plus">
							<?php echo $desired[$key]['workout'] ?>
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>

			</div>               
		</div>
	</div>
</div>


<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
			</div>
			<div class="modal-body row">
				<img src="assets/images/icon_sucess.png" class="pull-left"> 
				<span class="ss_extar_top20">Your answer is correct	</span> 
			</div>
			<div class="modal-footer">
				<button id="get_next_question" type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>        
			</div>
        </div>
    </div>
</div>

    <div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
          </div>
          <div class="modal-body row">
            <i class="fa fa-close" style="font-size:20px;color:red"></i> <span class="ss_extar_top20">Your answer is wrong</span>
            <br><?php echo strip_tags($question_info_s[0]['answer']); ?>  
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
          </div>
        </div>
      </div>
    </div>

    <?php $i = 1;
    foreach ($total_question as $ind) { ?>
      <div class="modal fade ss_modal ew_ss_modal" id="show_description_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">

              <h4 class="modal-title" id="myModalLabel"> Question Description </h4>
            </div>
            <div class="modal-body">
              <textarea class="form-control" name="questionDescription"><?php echo $ind['questionDescription']; ?></textarea>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <?php $i++;
    } ?>


    <script>
      function show_workout(){
        $("#workout").show();
      }
    </script>

    <?php $this->load->view('tutors/check_student_copy/drawingBoard'); ?>
