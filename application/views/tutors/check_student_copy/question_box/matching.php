<?php 
    $question_info = json_decode($question_info_s[0]['questionName']);
    $st_ans = json_decode($tutorial_ans_info[0]['st_ans'],TRUE);
    $question_order = $question_info_s[0]['question_order'];
//    echo '<pre>';print_r($question_info);
?>

<div class="col-md-12"> 
    <div class="text-center">
        
        <div class="form-group ss_h_mi" style="margin-bottom: 10px">
            
<!--            <a role="button" aria-expanded="true" aria-controls="collapseOne" style="float: left;">
                <span onclick="setSolution()">
                    <img src="assets/images/icon_solution.png"> Solution
                </span> Question
            </a>-->
            
            <label for="exampleInputiamges1">Question</label>
            <div class="select">
                <input class="form-control" type="text" value="<?php echo strip_tags($question_info->questionName)?>" name="questionName">
            </div>

        </div>
    </div>
</div>


<?php
    $lettry_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'k', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
    $color_array = array('red', 'green', 'blue', '#00BFFF', '#FF6347', '#708090', '#2F4F4F', '#C71585', '#8B0000', '#808000', '#FF6347', '#FF4500', '#FFD700', '#FFA500', '#228B22', '#808000', '#00FFFF', '#66CDAA', '#7B68EE', '#FF69B4');
    $right_side_ans = json_decode(json_decode($st_ans[$question_order]['student_ans']),TRUE);
                                           // echo '<pre>';print_r($st_ans);die;
?>
<div class="col-sm-4">
    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">  Maching</a>
                </h4>
            </div>
            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="image_box_list ss_m_qu">
                        <?php $i = 1;
                        foreach ($question_info->left_side as $row) {
                            ?>
                            <div class="row">
                                <div class="col-xs-2">
                                    <p class="ss_lette">
                                        <?php echo $lettry_array[$i - 1]; ?>
                                    </p>
                                </div>
                                <div class="col-xs-8">
                                    <div class="box ">
                                        <div class="ss_w_box text-center">
                                            <?php echo $row[0]; ?>
                                        </div>                                                   
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <p class="ss_lette" id="color_left_side_<?php echo $i;?>" style="<?php if(($right_side_ans['student_ans'][$i-1])){?>background-color: <?php echo $color_array[$i-1];}?>">
                                        <input type="radio" id="left_side_<?php echo $i; ?>" name="left_side_<?php echo $i; ?>" value="<?php echo $i;?>" 
                                               data-id="1" class="left" style="min-height: 96px;" <?php if(($right_side_ans['student_ans'][$i-1])){echo 'checked';}?>>
                                    </p>
                                </div>
                            </div>
                        <?php $i++; }?>
                    </div>
                </div>

<!--                <div class="col-sm-4"></div>
                <div class="col-sm-4" style="margin-top: 10px;">     
                    <button type="button" class="btn btn_next" id="answer_matching">submit</button>
                </div>                                  
                <div class="col-sm-4"></div>-->

            </div>
        </div>                                                        
    </div>
</div>

<div class="col-sm-4">
    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne"> &nbsp </a>
                </h4>
            </div>
            <div id="collapseOne2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="image_box_list ss_m_qu">
                        <form id="answer_form">
                            <?php $i = 1;
                            foreach ($question_info->right_side as $row) {
                                ?>
                                <div class="row">

                                    <div class="col-xs-1">
                                        
                                        <p class="ss_lette" id="color_right_side_<?php echo $i; ?>" 
                                           style="<?php if($right_side_ans['student_ans'][$i-1] == $i){?>background-color: <?php echo $color_array[$i-1];}?>">
                                            
                                            <input type="radio" name="right_side_<?php echo $i; ?>" value="<?php echo $i; ?>" 
                                                <?php if(isset($right_side_ans['student_ans'][$i-1]) && $right_side_ans['student_ans'][$i-1] == $i){echo 'checked';}?> class="right" 
                                                style="min-height: 96px;">
                                        </p>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="box ">
                                            <div class="ss_w_box text-center">
                                                <p>
                                                    <?php echo $row[0]; echo '<br><br>';?>
                                                </p>
                                            </div>                                                   
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <p class="ss_lette" style="height: 100px;vertical-align: middle;display: table-cell;">
                                            <input type="number" class="form-control" name="answer_<?php echo $i; ?>" id="answer_<?php echo $i; ?>" data="1">
                                        </p>
                                    </div>
                                    <div class="col-xs-1">
                                        <span class="" id="message_<?php echo $i - 1; ?>"></span>
                                    </div>
                                    
                                </div>
                                <?php $i++;
                            }
                            ?>
                        </form> 

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<input type="hidden" name="image_quantity" id="image_quantity" value="">

<script>
    $('.right').attr('disabled', true);
    var left_arr = new Array();
    var right_arr = new Array();
    var color_array = new Array('red', 'green', 'blue', '#00BFFF', '#FF6347', '#708090', '#2F4F4F', '#C71585', '#8B0000', '#808000', '#FF6347', '#FF4500', '#FFD700', '#FFA500', '#228B22', '#808000', '#00FFFF', '#66CDAA', '#7B68EE', '#FF69B4');
    function getLeftVal(e)
    {
        var left_ans_val = e.value;

        left_arr.push(left_ans_val);
        
        $('.right').attr('disabled', false);
        $('.left').attr('disabled', true);
        //var last = left_arr.slice(-1)[0];
        var color_left = color_array[left_ans_val - 1];
        //document.getElementById("color_left_side_1").style.backgroundColor = color_left;
        document.getElementById("color_left_side_" + left_ans_val).setAttribute('style', 'background-color:' + color_left + ' !important');
        //console.log(last);
    }

    function getRightVal(e)
    {
        var last = left_arr.slice(-1)[0];

        var right_ans_val = e.value;

        document.getElementById("answer_" + right_ans_val).value = last;
        
        $('.right').attr('disabled', true);
        $('.left').attr('disabled', false);
        var color_right = color_array[last - 1];
        document.getElementById("color_right_side_" + right_ans_val).setAttribute('style', 'background-color:' + color_right + ' !important');
        console.log(right_arr);
    }


    var qtye = $("#box_qty").val();
    document.getElementById("image_quantity").value = qtye;
    common(qtye)
    function getImageBox() {
        var qty = $("#box_qty").val();
        if (qty < 4) {
            $("#box_qty").val(4);
        } else if (qty > 20) {
            $("#box_qty").val(20);
        } else {
            $('.editor_hide').hide();
            document.getElementById("image_quantity").value = qty;
            common(qty);
        }

    }
    function common(quantity)
    {
        for (var i = 1; i <= quantity; i++)
        {
            $('#list_box_1_' + i).show();
            $('#list_box_2_' + i).show();
        }
    }
    function getAnswer()
    {
        //alert(this.attr('data'));
    }
</script>