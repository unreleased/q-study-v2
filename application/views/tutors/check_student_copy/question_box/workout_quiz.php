<?php
	$question_info = $question_info_s[0]['questionName'];
	$st_ans = json_decode($tutorial_ans_info[0]['st_ans'], true);
	$question_order = $question_info_s[0]['question_order'];
	$workoutImage = $st_ans[$question_order]['workout'];
	$userType = $_SESSION['userType'];

	$questionId = $question_info_s[0]['question_id'];
	$stAnsId = $tutorial_ans_info[0]['id'];

	if ($userType==6) {
		$strutinizeData = $this->Admin_model->search('scrutinize_report', ['ans_id'=>$stAnsId, 'question_id'=>$questionId]); //noshto noshto
		
		$strutinizeData = $strutinizeData[0]['data'];
		$strutinizeData = json_decode($strutinizeData);
		$strutinizeImg = $strutinizeData->scrutinize_image;
	}

?>
<div id="workoutImage" style="display:none;">
    <?php if ($userType==6) : ?>
      <p><img src="<?php echo $strutinizeImg ?>" alt="no image"></p>
    <?php else : ?>
        <?php print_r(json_decode($st_ans[$question_order]['student_ans'])); ?>
    <?php endif ?>
</div>
<input type="hidden" id="answerId" value="<?php echo $stAnsId; ?>">
<input type="hidden" id="questionId" value="<?php echo $questionId; ?>">
<input type="hidden" id="userType" value="<?php echo $userType; ?>">

<div class="col-sm-4">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <span><img src="assets/images/icon_draw.png"> Instruction</span> Question
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <div class="math_plus">
            <?php echo $question_info; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="" id="draggable" style="display: none;">
    <div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
          </h4>
        </div>
        <div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <textarea name="workout" class="mytextarea"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-4">
  <strong class="text-center">
    <a href="javascript:void(0)" style="text-decoration: underline;padding:10px;" onclick="test()">Workout</a>
  </strong>
</div>
<div class="col-sm-4" style="display: none;">
  <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">   Answer</a>
        </h4>
      </div>
      <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body" id="quesBody">
          <div class="math_plus">
            <?php echo json_decode($st_ans[$question_order]['student_ans']);?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
