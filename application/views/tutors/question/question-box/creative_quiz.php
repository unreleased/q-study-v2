<input type="hidden" name="questionType" value="9">
<div class="col-sm-4">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" aria-expanded="true" aria-controls="collapseOne">
                        <span onclick="setSolution()">
                            <img src="assets/images/icon_solution.png"> Solution
                        </span> Question
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <textarea class="mytextarea" name="questionName"></textarea>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-8">
    <div class="text-left">
        <div class="form-group ss_h_mi">
            <label for="exampleInputiamges1">How many sentences</label>

            <div class="select">
                <input class="form-control" type="number" value="1" min="1" id="box_qty">
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body" id="questionBody">

            <div class="row sentence" style="margin-bottom:10px" serial="1"> 
                <div class="col-md-8">
                    <input type="text" class="form-control" value="" name="sentence[]">
                </div>
                <div class="col-md-1">
                    <input checkboxId="0" type="checkbox" id="quesChecked">
                </div>
                <div class="col-md-2">
                    <input  type="text" class="form-control questionOrder" value="" min="1" id="qOrdr1">
                </div>
                <div class="col-md-1">
                    <a data-toggle="modal" data-target="#exampleModal" class="text-center addDescIcon"><img src="assets/images/icon_details.png"></a>
                    <input style="display:none;" name="description[]" id="hiddenDesc_1" value="">
                </div>
            </div>
        </div>
    </div>

</div>

<!-- <div class="col-sm-2">
  <div class="text-right">
    <div class="form-group ss_h_mi">
      <label for="exampleInputiamges1">Description</label>
    </div>
  </div>
</div> -->

<!-- add description modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Save Description</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="message-text" class="control-label mb-2">Description:</label>
                    <textarea class="form-control" id="description"></textarea>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary" id="descSaveBtn">Save</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden"  id="modalOpenFor" value="">
<input type="hidden" name="ansSequence" id="ansSequence" value="">

<script>
    $('#box_qty').on('input', function(){
        ordr = 1;
        var boxes = $('#box_qty').val();
        var html = '';
        for (var i=1; i <=boxes ; i++) {
            html += `
            <div class="row sentence" style="margin-bottom:10px" serial="`+i+`"> 
            <div class="col-md-8">
            <input type="text" class="form-control" value="" name="sentence[]">
            </div>
            <div class="col-md-1">
            <input checkboxId="`+i+`" type="checkbox" id="quesChecked">
            </div>
            <div class="col-md-2">
            <input  type="text" class="form-control questionOrder" value="" min="1" id="qOrdr`+i+`">
            </div>
            <div class="col-md-1">
            <a data-toggle="modal" data-target="#exampleModal" class="text-center addDescIcon"><img src="assets/images/icon_details.png"></a>
            <input style="display:none;" name="description[]" id="hiddenDesc_`+i+`" value="">
            </div>
            </div>`;
        }
        $('#questionBody').html(html);

    })

    $(document).ready(function () {

        //question checkbox check functionality
        //generate order automatically
        var ordr = 1;
        var rightAnsSequence = [];
        $(document).on('change', '#quesChecked', function () {
          //var x = $(this).closest('div#sentence').children('#qOrdr');
            var id= $(this).attr("checkboxId");
            var qOrdr =  $("#qOrdr"+id);

            if (this.checked) {
                qOrdr.prop('disabled', true);
                qOrdr.val(ordr++);
                rightAnsSequence.push(id); //push the right ans index
            } else {
                qOrdr.prop('disabled', false);
                ordr--;
                qOrdr.val('');

            //remove the index from right ans item
                rightAnsSequence = jQuery.grep(rightAnsSequence, function(value) {
                    return value != id;
                });
            }
            
            var seq = (JSON.stringify(rightAnsSequence));
            $('#ansSequence').val(seq);
        });

    });

  //save description on hidden field
    $('#descSaveBtn').on('click', function(){
        $('#exampleModal').modal('toggle');
        var descValue = $('#description').val();
        var descForId = $('#modalOpenFor').val();
        $('#hiddenDesc_'+descForId).val(descValue);
    });

  //clear description modal text
    $(document).on('click','.addDescIcon', function(){

        $('#description').val('');
        var id = $(this).closest('div.sentence').attr('serial');
        $('#modalOpenFor').val(id);
    });
</script>
