<?php //print_r($_SESSION);die; ?>
<style>
  .sign_up_menu ul {
    display: none !important;
  }
  
  .select2-container{
    display: initial;
  }

  .form-group{
    width:135px !important;
  }
  .search_filter{
    margin-left: 85px;
    margin-bottom: 0px; 
  }
</style>

<div class="row"> 
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div style="float: right;margin-right: 0px; margin-top: 20px;">
      <a class="ss_q_link pull-left" href="q-dictionary/search">Q- Dictionary</a>
      <a style="color:#4BBCC0; background-color: #fff; font-size: 15px;" class="ss_q_link pull-left" href="subject/all">Delete Subject & Chapter</a>
    </div>
  </div>
</div>

<div class="row" >
  <!-- <div class="col-sm-2"></div> -->
  <div class="col-sm-12 ">
    <?php if ($this->session->flashdata('success_msg')) : ?>
      <div class="alert alert-warning alert-dismissible fade in" role="alert"> 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <?php echo $this->session->flashdata('success_msg') ?>
      </div>
    <?php endif; ?>
    <div class="ss_q_list_top">

      <div class="ss_student_progress">
        <div class="search_filter">
          <form class="form-inline" method="post" action="question-list">
            <div class="form-group">
              <label for="exampleInputName2">Module Name</label>
              <div class="select">
                <?php $modName = isset($_SESSION['modInfo']['moduleName']) ? $_SESSION['modInfo']['moduleName']:'';?>
                <input type="text" value="<?php echo $modName; ?>" class="form-control" name="moduleName" style="width:130px;" id="moduleName">
              </div>
            </div>
            
            <div class="form-group">
              <label for="exampleInputName2">Country</label>
              <div class="select">
                <?php $disabled = isset($_SESSION['selCountry'])||isset($_SESSION['modInfo']['country']) ? 'style="pointer-events:none;"' : ''; 
                  $selCountry = isset($_SESSION['modInfo']['country']) ? $_SESSION['modInfo']['country'] : (isset($_SESSION['selCountry']) ? $_SESSION['selCountry'] : '');
                ?>
                <select class="form-control" name="country" id="country" <?php echo $disabled; ?>>
                  <option value="">Select Country</option>
                  <?php foreach ($allCountry as $country) : ?>
                    <?php $sel = strlen($selCountry)&&($country['id']==$selCountry) ? 'selected' : ''; ?>
                    <option value="<?php echo $country['id'] ?>" <?php echo $sel; ?>><?php echo $country['countryName'] ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            
            <div class="form-group">

              <label for="exampleInputName2">Grade</label>
              <div class="select">
                <select class="form-control select-hidden" name="grade">
                  <option value="">Select Grade/Year/Level</option>
                  <?php $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; ?>
                  <?php foreach ($grades as $stGrade) { ?>
                    <?php $sel = isset($_SESSION['modInfo']['studentGrade'])&&($stGrade==$_SESSION['modInfo']['studentGrade']) ? 'selected' : '';?>
                    <option value="<?php echo $stGrade; ?>" <?php echo $sel; ?>>
                      <?php echo $stGrade; ?>
                    </option>
                  <?php } ?>
                  <option value="13">Upper Level</option>
                </select>
                
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Module Type</label>
              <div class="select">
                <select class="form-control select-hidden" name="moduleType">
                  <option value="">Select....</option>
                  <?php foreach ($all_module_type as $module_type) {?>
                    <?php $sel = isset($_SESSION['modInfo']['moduleType'])&&($module_type['id']==$_SESSION['modInfo']['moduleType']) ? 'selected' : '';?>
                    <option value="<?php echo $module_type['id']?>" <?php echo $sel; ?>>
                      <?php echo $module_type['module_type'];?>
                    </option>
                  <?php }?>
                </select>
                
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Subject</label>
              <div class="select">
                <select class="form-control select-hidden" id="subject" name="subject">
                  <option value="">Select....</option>
                  <?php foreach ($all_subject as $subject) {?>
                    <?php $sel = isset($_SESSION['modInfo']['subject'])&&($subject['subject_id']==$_SESSION['modInfo']['subject']) ? 'selected' : '';?>
                    <option value="<?php echo $subject['subject_id']?>" <?php echo $sel; ?>>
                      <?php echo $subject['subject_name'];?>
                    </option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Chapter</label>
              <div class="select">
                <select class="form-control select-hidden" name="chapter" id="chapter">
                  <option value="">Select....</option>
                  <?php if(isset($_SESSION['modInfo']['chapter'])): ?>
                    <?php echo $_SESSION['modInfo']['chapter']; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail2">Course</label>
              <div class="select">
                <select class="form-control select-hidden" id="course" name="course">
                  <option value="">Select....</option>
                  <?php foreach ($all_course as $course) {?>
                    <?php $sel = isset($_SESSION['modInfo'])&&($course['id']==$_SESSION['modInfo']['course']) ? 'selected' : '';?>
                    <option value="<?php echo $course['id']?>" <?php echo $sel; ?>>
                      <?php echo $course['courseName'];?>
                    </option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="form-group"  style="width:0px !important">
              <button type="submit" class="btn btn-primary" style="margin-top:20px !important">search</button>
            </div>
          </form>
        </div>
        
        <div class="row ss_q_list_top">
          <span>Quiz</span>    
        </div>

      </div>

    </div>
    <div class="ss_question_list">
      <?php foreach ($all_question_type as $key) {?>
        <div class="row">
          <div class="col-sm-3">
            <ul class="ss_q_left"> 
              <li>
                <a href="<?php echo base_url();?>create-question/<?=$key['id']?>"><?php echo $key['questionType'];?></a>
              </li>
            </ul>
          </div>

          <div class="col-sm-9">

            <ul class="ss_question_menu" id="quesType_<?php echo $key['id'];?>">
              <?php $i = 1;foreach ($all_question[$key['id']] as $row) {
                $color = $row['dictionary_item']?'#ED1C24':'';?>

                <li style="background-color: <?php echo $color; ?><?php if ($i > 10) {
                  ?>;display: none;<?php }?>" data-id="<?=$key['id']?>_<?=$row['id']?>" id="q_<?=$i?>_<?=$key['id']?>" >
                  <a href="question_edit/<?=$key['id']?>/<?=$row['id']?>">Q<?=$i?></a>
                </li>
                <?php $i++;}?>

                <li class="ss_q_u_d" <?php if ($i < 11) {?>style="display: none;"<?php }?>>
                  <a id="upbutton_<?=$key['id']?>" onclick="fn_show_upper(1, <?=$key['id']?>,<?=$i-1?>)">
                    <i class="fa fa-caret-up" aria-hidden="true"></i>
                  </a>
                  <span id="spinner_val_<?=$key['id']?>">1[<?php echo $i-1;?>]</span>
                  <a id="downbutton_6" onclick="fn_show_upper(0, <?=$key['id']?>,<?=$i-1?>)">
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                  </a>
                </li>
                <?php if ($i > 10) {?>
                  <li class="ss_q_last" data-id="<?=$key['id']?>_<?=$row['id']?>" id="q_<?=$i?>_<?=$key['id']?>">
                    <a href="question_edit/<?=$key['id']?>/<?=$row['id']?>">Q<?=$i-1?></a>
                  </li>
                  <li class="ss_q_total">
                    <a onclick="lastTenquestion(<?=$key['id']?>,<?=$i-1?>)" >Q<?=$i-1?></a>
                  </li>
                <?php }?>

              </ul>


            </div>
          </div>
        <?php }?>
      </div>
    </div>

  </div>
  <script>

    function fn_show_upper(aval, acat, acount){
      var vspinnerval = $("#spinner_val_" + acat +"").html();
      var spinnerval = vspinnerval.substr(0, vspinnerval.indexOf('['));

      var vinterval = acount / 10;
      vinterval = Math.round(vinterval);

      var vmod = acount % 10;

      if (aval == 1) {
        spinnerval++;
      } else {
        spinnerval--;
      }
      if (spinnerval < 1) {
        spinnerval = 1;
      }
      if (spinnerval > 500) {
        spinnerval = 500;
      }

      var vr = Math.round(10 / 10);
      var vd = 10 % 10;

        //alert('div:' + vr + ' mod:' + vd + ' inter:' + vinterval);

        if (vmod == 0){
          vinterval = vinterval;
        } else {
          if (vmod >= 5){
            vinterval = vinterval;
          } else {
            vinterval = vinterval + 1;
          }
        }
        if (spinnerval > vinterval) {
          spinnerval = vinterval;
        }

        //alert(vmod);

        $("#spinner_val_" + acat +"").html(spinnerval + '[' + acount + ']');

        for (var i=1;i <= acount;i++) {
          $("#q_" + i + "_" + acat).hide();
        }

        if (spinnerval == 1) {
          for (var i=1;i <= 10;i++){
            $("#q_" + i + "_" + acat).show();
          }
        } else {
          var vstart = 10 * spinnerval;
          vstart = (vstart - 10) + 1;

          for (var i = vstart;i <= (10 * spinnerval);i++) {
            $("#q_" + i + "_" + acat).show();
          }
        }
      }

      function lastTenquestion(acat, acount){
        var vinterval = acount / 10;
        vinterval = Math.ceil(vinterval) - 1;
        $("#spinner_val_" + acat +"").html(vinterval + '[' + acount + ']');
        fn_show_upper(1,acat, acount);
      }
    </script>

    <script>
      /*context menu (right click on question menu)*/
      

      for (var i = 1; i <= 12; i++) {
            //$(function(){ //workout quiz
              $('#quesType_'+i).contextMenu({
                selector: 'li', 
                callback: function(key, options) {
                  var li_item = $(this);
                  var qType_qId = $(this).attr('data-id');
                  temp = qType_qId.split('_');
                  qId = temp[1];
                  qType = temp[0]
                  if(key=='preview'){
                    window.location.href = "question_preview/"+qType+"/"+qId;
                  }else if(key=='delete'){

                    $.ajax({
                      url: "question_delete/"+qId,
                      method : 'POST',
                      success: function(data){
                        if(data=='true'){ alert('Question deleted successfully.'); li_item.fadeOut("slow"); }
                        else{ alert('Somethings wrong.'); }
                      }
                    })

                  }else if(key='duplicate'){
                   $.ajax({
                    url: "question_duplicate/"+qId,
                    method : 'POST',
                    success: function(data){
                      if(data=='true'){ alert('Question duplicated successfully.'); location.reload();}
                      else{ alert('Somethings wrong.'); }
                      console.log(data);
                    }
                  })
                 }
               },
               items: {
                "preview": {name: "Preview", icon: "fa-eye"},
                "delete": {name: "Delete", icon: "cut"},
                "duplicate": {name: "Duplicate", icon: "copy"},
              }

            });
            //});      
          }

          //get chapter on subject change
          $(document).on('change', '#subject', function(){
            var subject = $(this).val();
            $.ajax({
              url:'Tutor/get_chapter_name',
              method:'post',
              data:{'subject_id':subject},
              success: function(response){
                $('#chapter').html(response);
              }
            })
          });
        </script>