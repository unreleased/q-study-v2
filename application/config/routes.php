<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|   $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|   $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|       my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;

$route['signup'] = 'RegisterController/showSignUp';
$route['signup/parent'] ='RegisterController/showSignUpPlan';
$route['signup/upper_level_student'] ='RegisterController/showSignUpPlan';
$route['signup/tutor'] ='RegisterController/showSignUpPlan';
$route['signup/school'] ='RegisterController/showSignUpPlan';
$route['signup/corporate'] ='RegisterController/showSignUpPlan';

$route['select_country']='RegisterController/selectCountry';
$route['select_course']='RegisterController/selectCourse';
$route['student_form']='RegisterController/student_form';
$route['save_student']='RegisterController/save_student';
$route['sure_data_save']='RegisterController/sure_data_save';

$route['trial'] = 'RegisterController/showSignUp';
$route['trial/parent'] ='RegisterController/showSignUpPlan';
$route['trial/upper_level_student']='RegisterController/showSignUpPlan';
$route['trial/tutor']='RegisterController/showSignUpPlan';
$route['trial/school'] ='RegisterController/showSignUpPlan';
$route['trial/corporate'] ='RegisterController/showSignUpPlan';

$route['redirect_url'] = 'RegisterController/redirect_url';


/* for paypal payment*/
$route['paypal']='RegisterController/show_paypal_form';
$route['go_paypal']='RegisterController/go_paypal';
$route['paypal_success']='PaypalController/paypal_success';
$route['paypal_cancel']='PaypalController/paypal_cancel';
$route['subscription/cancel']='PaypalController/cancelSubscription';
$route['paypal_notify']='PaypalController/paypal_notify';


//upper_level_student
$route['upper_level_student_form']='RegisterController/upper_level_student_form';
$route['save_upper_student']='RegisterController/save_upper_student';
$route['sure_upper_student_data_save']='RegisterController/sure_upper_student_data_save';


//tutor
$route['tutor_form']='RegisterController/tutor_form';
$route['save_tutor']='RegisterController/save_tutor';
$route['sure_save_tutor']='RegisterController/sure_save_tutor';
$route['student_progress']='Student_Progress/viewStudentProgress';

$route['check_student_copy/(:any)/(:any)/(:any)']='Student_Copy/check_student_copy/$1/$2/$3';

//messaging
$route['message/type'] = 'Message/selectMessageType';
$route['message/topics'] = 'Message/showAllTopics';
$route['message/topics/add'] = 'Message/addMessageTopic';
$route['message/topics/delete/(:any)'] = 'Message/DeleteMessageTopic/$1';
$route['message/set'] = 'Message/setMessage';
$route['show_all_message/(:any)'] = 'Message/show_all_message/$1';
$route['edit_message/(:any)'] = 'Message/edit_message/$1';
$route['add_message/(:any)'] = 'Message/add_message/$1';
$route['message/delete/(:any)'] = 'Message/delete_message/$1';

//school
$route['school_form']='RegisterController/school_form';
$route['save_school']='RegisterController/save_school';
$route['sure_school_data_save']='RegisterController/sure_school_data_save';


//corporate
$route['corporate_form']='RegisterController/corporate_form';
$route['save_corporate']='RegisterController/save_corporate';
$route['sure_corporate_data_save']='RegisterController/sure_corporate_data_save';


//mail-temlete_parent
$route['parent_trial_mail']='RegisterController/parent_trial_mail';
$route['parent_signup_mail']='RegisterController/parent_signup_mail';


//mail-temlete_upper_student
$route['upper_student_trial_mail']='RegisterController/upper_student_trial_mail';
$route['upper_student_signup_mail']='RegisterController/upper_student_signup_mail';


//mail-temlete_tutor
$route['tutor_trial_mail']='RegisterController/tutor_trial_mail';
$route['tutor_signup_mail']='RegisterController/tutor_signup_mail';


//mail-temlete_school
$route['school_mail']='RegisterController/school_mail';


//mail-temlete_corporate
$route['corporate_mail']='RegisterController/corporate_mail';


//after-registration
$route['home_page']='RegisterController/home_page';


//parent-setting
$route['parent_setting']='Parents/parent_setting';
$route['my_details']='Parents/my_details';
$route['update_my_details']='Parents/update_my_details';
$route['upload_photo']='Parents/upload_photo';
$route['file-upload']='Parents/parent_dropzone_file';


//student-setting
$route['student_setting'] = 'Student/student_setting';
$route['student_details'] = 'Student/student_details';
$route['update_student_details'] = 'Student/update_student_details';
$route['my_enrollment'] = 'Student/my_enrollment';
$route['get_ref_link'] = 'Student/get_ref_link';
$route['save_ref_link'] = 'Student/save_ref_link';
$route['student_upload_photo'] = 'Student/student_upload_photo';
$route['sure_student_photo_upload'] = 'Student/sure_student_photo_upload';
$route['q_study_course'] = 'Student/q_study_course';
$route['tutorial/(:any)'] = 'Student/tutorial/$1';
$route['tutor_course'] = 'Student/tutor_course';
$route['all_module_by_type/(:any)/(:any)'] = 'Student/all_module_by_type/$1/$2';
$route['video_link/(:any)/(:any)'] = 'Student/video_link/$1/$2';
$route['ans_the_wrong_question/(:any)/(:any)/(:any)'] = 'Student/ans_the_wrong_question/$1/$2/$3';
$route['all_tutors_by_type/(:any)/(:any)'] = 'Student/all_tutors_by_type/$1/$2';
$route['finish_all_module_question/(:any)'] = 'Student/finish_all_module_question/$1';


//u-level-student-setting
$route['u_level_studen_setting']='Upper_level/u_level_studen_setting';
$route['u_level_student_details']='Upper_level/u_level_student_details';
$route['update_u_level_student_details']='Upper_level/update_u_level_student_details';
$route['u_level_upload_photo']='Upper_level/u_level_upload_photo';
$route['u_level_file-upload']='Upper_level/u_level_file_upload';
$route['u_level_enrollment']='Upper_level/u_level_enrollment';


//tutor
$route['tutor_setting']='Tutor/tutor_setting';
$route['tutor_details']='Tutor/tutor_details';
$route['update_tutor_details']='Tutor/update_tutor_details';
$route['tutor_upload_photo']='Tutor/tutor_upload_photo';
$route['tutor_file-upload']='Tutor/tutor_file_upload';
//find tutor,show,update profile
$route['tutor/profile/update'] = 'Tutor/updateProfile';
$route['tutor/profile/(:any)'] = 'CommonAccess/showTutorProfile/$1';
$route['tutor/search'] = 'CommonAccess/searchTutor';
$route['tutor/account/settings'] = 'Tutor/accountSettings';

//school
$route['school_setting']='School/school_setting';
$route['school_info_details']='School/school_info_details';
$route['update_school_details']='School/update_school_details';
$route['school_logo']='School/school_logo';
$route['school_logo_upload']='School/school_logo_upload';


//corportae
$route['corporate_setting']='corporate/corporate_setting';
$route['corporate_details']='corporate/corporate_details';
$route['update_corporate_details']='corporate/update_corporate_details';
$route['corporate_logo']='corporate/corporate_logo';
$route['corporate_logo_upload']='corporate/corporate_logo_upload';
/* for card payment*/
$route['card_form_submit']='CardController/card_form_submit';
// end

//login
$route['loginChk']='Login/loginChk';
$route['logout']='Logout';

//Settings
$route['cancel_subscription']='dashboard/cancel_subscription';
$route['cancel_confirm']='dashboard/cancel_confirm';

//************           Module Section         ***********
$route['view-course']='dashboard/view_course';
$route['course/country']='qstudy/courseCountrySelect';
$route['all-module']='Module/all_module';
$route['add-module']='Module/add_module';
$route['edit-module/(:any)']='Module/editModule/$1';
$route['reorder-module']='Module/reorderModule';
$route['module_preview/(:any)/(:any)']="Module/module_preview/$1/$2";
$route['getStudentByGradeCountry']='Module/getStudentByGradeCountry';
$route['getIndividualStudent']='Module/getIndividualStudent';
$route['module/repetition/(:any)'] = 'Module/setRepetitionDays/$1';
$route['module/types'] = 'Module/allModuleType';
$route['module/search'] = 'Module/searchModule';
$route['module/tutor_list/(:any)'] = 'Module/tutorList/$1'; //tutor_list/module_type
//Get Course By Usertype
$route['get_course'] = 'Module/get_course';


//************           Question Section         ***********
$route['question-list']='tutor/question_list';
$route['question_duplicate/(:any)']='Question/duplicateQuestion/$1';
$route['question_delete/(:any)']='Question/deleteQuestion/$1';
$route['create-question/(:any)']='tutor/create_question/$1';
$route['add_subject_name']='tutor/add_subject_name';
$route['get_chapter_name']='tutor/get_chapter_name';
$route['get_subject']='tutor/get_subject';

$route['subject/all']='Subject/showAllSubject';
$route['subject/delete/(:any)']='Subject/deleteSubject/$1';
$route['chapter/delete/(:any)']='Subject/deleteChapter/$1';

$route['add_chapter']='tutor/add_chapter';
$route['save_question_data']='tutor/save_question_data';
$route['imageUpload']='CommonAccess/imageUpload';

//q-dictionary
$route['q-dictionary/add'] = 'Question/addDictionaryWord';
$route['q-dictionary/search'] = 'CommonAccess/searchDictionaryWord';
$route['q-dictionary/view'] = 'CommonAccess/viewDictionaryWord';
$route['q-dictionary/wordlist'] = 'Admin/dictionaryWordList';
$route['q-dictionary/approve/(:any)'] = 'Admin/dicWordApprovePage/$1';
$route['q-dictionary/reject/(:any)'] = 'Admin/wordReject/$1';
$route['q-dictionary/payment'] = 'Admin/dictionaryPayment';

//************           Preview Section         ***********
$route['question_preview/(:any)/(:any)']='Preview/question_preview/$1/$2';
$route['question/preview/(:any)/(:any)']='IDontLikeIt/question_preview/$1/$2';//to avoid conflict of .... I dont know whats wrong.
$route['answer_matching']='Preview/answer_matching';
$route['preview_vocubulary/(:any)']='Preview/preview_vocubulary/$1';
$route['answer_multiple_matching']='Preview/answer_multiple_matching';
$route['answer_matching_true_false']='Preview/answer_matching_true_false';
$route['answer_matching_vocabolary']='Preview/answer_matching_vocabolary';
$route['answer_matching_multiple_choice']='Preview/answer_matching_multiple_choice';
$route['answer_matching_multiple_response']='Preview/answer_matching_multiple_response';
$route['answer_creative_quiz'] = 'Preview/answer_creative_quiz';
$route['answer_times_table'] = 'Preview/answer_times_table';
$route['answer_algorithm'] = 'Preview/answer_algorithm';
$route['answer_workout_quiz'] = 'Preview/answer_workout_quiz';


//************           Question Edit Section         ***********
$route['question_edit/(:any)/(:any)']='tutor/question_edit/$1/$2';
$route['update_question_data']='tutor/update_question_data';

//added by sobuj

$route['get_tutor_tutorial_module/(:any)/(:any)']='Student/get_tutor_tutorial_module/$1/$2';

$route['st_answer_matching']='Student/st_answer_matching';
$route['st_answer_matching_vocabolary']='Student/st_answer_matching_vocabolary';
$route['st_answer_matching_true_false']='Student/st_answer_matching_true_false';
$route['st_answer_matching_multiple_choice']='Student/st_answer_matching_multiple_choice';
$route['st_answer_matching_multiple_response']='Student/st_answer_matching_multiple_response';
$route['st_answer_multiple_matching']='Student/st_answer_multiple_matching';
$route['st_answer_assignment']='Student/st_answer_assignment';
$route['st_answer_creative_quiz']='Student/st_answer_creative_quiz';
$route['st_answer_times_table']='Student/st_answer_times_table';
$route['st_answer_algorithm']='Student/st_answer_algorithm';

$route['show_tutorial_result/(:any)']='Student/show_tutorial_result/$1';




//**********     Admin Section      *********
$route['course_theme'] = 'admin/course_theme';
$route['save_theme'] = 'admin/save_theme';
$route['all_area'] = 'admin/all_area';
//User Info
$route['user_list'] = 'admin/user_list';
$route['user_add'] = 'Admin/userAdd';
$route['edit_user/(:any)'] = 'Admin/edit_user/$1';
$route['tutor_create_50_vocabulary'] = 'admin/tutor_create_50_vocabulary';
$route['tutor_with_10_students'] = 'admin/tutor_with_10_students';
//Country Info
$route['country_list'] = 'admin/country_list';
$route['save_country'] = 'admin/save_country';
$route['update_country'] = 'admin/update_country';
$route['delete_country/(:any)'] = 'admin/delete_country/$1';
//Country Wise Course
$route['country_wise'] = 'admin/country_wise';
$route['course_schedule/(:any)'] = 'admin/course_schedule/$1';
$route['save_course_schedule'] = 'admin/save_course_schedule';
$route['delete_course'] = 'admin/delete_course';
//find tutor
$route['trial_period'] = 'admin/trial_period';
//faqs
$route['faq/add'] = 'Faq/addFaq';
$route['faq/all'] = 'Faq/allFaq';
$route['faq/edit/(:any)'] = 'Faq/editFaq/$1';
$route['faq/delete/(:any)'] = 'Faq/deleteFaq/$1';
$route['faq/view/(:any)'] = 'CommonAccess/viewFaq/$1';
$route['faq/add/other'] = 'Faq/addLandPageItems';
$route['faq/view/other/(:any)'] = 'CommonAccess/viewLandPageItem/$1';
$route['faq/video/upload'] = 'Faq/landPagevideoUpload';
$route['faq/video/list'] = 'Faq/landPagevideoList';
$route['contact_us'] = 'CommonAccess/contactUs';

//dialogue
$route['dialogue/add'] = 'Admin/addDialogue';
$route['dialogue/all'] = 'Admin/allDialogue';
$route['dialogue/delete/(:any)'] = 'Admin/deleteDialogue/$1';

//forgot password section
$route['forgot_password'] = 'Login/forgotPassView';
$route['pass_reset_link'] = 'Login/sendResetPassEmail';
$route['set_password'] = 'Login/saveNewPass';
$route['emailCheck'] = 'Login/emailCheck';

//suspended user
$route['suspended'] = 'CommonAccess/suspendUserRedirection';
//payment defaulter
$route['payment_defaulter'] = 'CommonAccess/paymentDefaulterRedirection';

//find tutor
$route['tutor/search'] = 'CommonAccess/searchTutor';
$route['sms_api/add'] = 'Admin/smsApiAdd';
$route['sms_message/add'] = 'Admin/sms_message';
