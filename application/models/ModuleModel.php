<?php

class ModuleModel extends CI_Model
{

    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);
    }

    public function deleteInfo($table, $colName, $colValue)
    {
        $this->db->where($colName, $colValue);
        $this->db->delete($table);
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();
    }
	
	public function getInfoByOrder($table, $colName, $colValue,$order_col,$order_by) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);
		
		$this->db->order_by($order_col, $order_by);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function moduleName($moduleId)
    {
        $res = $this->db
            ->select('moduleName')
            ->where('id', $moduleId)
            ->get('tbl_module')
            ->result_array();

        return isset($res[0]['moduleName']) ? $res[0]['moduleName'] : '';
    }//end moduleName()


    /**
     * Return module type name by a module Id
     *
     * @param integer $moduleTypeId module type id
     *
     * @return string  module type name
     */
    public function moduleTypeName($moduleTypeId)
    {
        $res = $this->db
            ->select('module_type')
            ->where('id', $moduleTypeId)
            ->get('tbl_moduletype')
            ->result_array();

        return isset($res[0]['module_type']) ? $res[0]['module_type'] : '';
    }//end moduleTypeName()


    /**
     * Return all module type
     *
     * @return array          all module type
     */
    public function allModuleType()
    {
        return $this->db
            ->select('*')
            ->get('tbl_moduletype')
            ->result_array();
    }//end allModuleType()

    
    /**
     * search from any table using conditions
     *
     * @param  string $tableName table to perform search
     * @param  array  $params    conditions array
     * @return [type]            [description]
     */
    public function search($tableName, $params)
    {
        $res = $this->db
            ->where($params)
            ->get($tableName)
            ->result_array();
    
        return $res;
    }
    

    /**
     * Insert batch operation will record multiple data at a time.
     *
     * @param string $tableName table name
     * @param array  $data      $arr = [ 0=>[1,2,3], 1=[1,2,3] ]
     *
     * @return mixed            insert id or null if failed to insert
     */
    public function insert($tableName, $data)
    {
        $res = $this->db->insert_batch($tableName, $data);

        return $res ? $this->db->insert_id() : null;
    }


    /**
     * This function will delete module info
     * and attached question from module question pivot table.
     *
     * @param  integer $moduleId module id to perform delete
     * @return bool           delete success/failed
     */
    public function delete($moduleId)
    {
        $this->db
            ->where('id', $moduleId)
            ->delete('tbl_module');

        $this->db
            ->where('module_id', $moduleId)
            ->delete('tbl_modulequestion');

        return 1;
    }

    /**
     * Delete module question from module question table.
     *
     * @param  integer $moduleId module id
     * @return void
     */
    public function deleteModuleQuestion($moduleId)
    {
        $this->db
            ->where('module_id', $moduleId)
            ->delete('tbl_modulequestion');
    }

    /**
     * Update multiple column with desired value
     *
     * @param string $tableName    table to get affect
     * @param string $selector     selector column on which where condition will apply
     * @param string $value        selected column value
     * @param array  $dataToUpdate array of data to update
     *                             ex:[  0=>['column_name'=>value],
     *                             1=['abc'=>'def']
     *                             ]
     *
     * @return null
     */
    public function update($tableName, $dataToUpdate, $selector)
    {
        //$dataToUpdate['updated_at'] = date("Y-m-d H:i:s");

        $this->db
            ->update_batch($tableName, $dataToUpdate, $selector);
    }//end update()


    /**
     * Get module Info
     *
     * @param integer $moduleId module id
     *
     * @return array           module info
     */
    public function moduleInfo($moduleId)
    {
        $res = $this->db
            ->where('id', $moduleId)
            ->get('tbl_module')
            ->result_array();

        return isset($res[0])?$res[0]:[];
    }//end moduleInfo()

    /**
     * Get all question ids,orderings,types of a module.
     *
     * @param integer $moduleId module id
     *
     * @return array   module question ids and orders
     */
    public function moduleQuestion($moduleId)
    {
        $res = $this->db
            ->where('module_id', $moduleId)
            ->get('tbl_modulequestion')
            ->result_array();

        return $res;
    }


     /**
      * Get all module for a loggedUser(without parent and student user).
      *
      * @return array all module of a user
      */
    public function allModule($conditions = [])
    {
        $loggedUserId = $this->session->userdata('user_id');
        $this->db
            //->select('tbl_module.*, tbl_subject.subject_name as subject_name, tbl_chapter.chapterName as chapterName')
            ->select('tbl_module.*, tbl_subject.subject_name as subject_name, tbl_chapter.chapterName as chapterName, tbl_useraccount.name creatorName, tbl_moduletype.module_type module_type')
            ->join('tbl_subject', 'tbl_subject.subject_id=tbl_module.subject', 'left')
            ->join('tbl_useraccount', 'tbl_module.user_id=tbl_useraccount.id', 'left')
            ->join('tbl_chapter', 'tbl_chapter.id=tbl_module.chapter', 'left')
            ->join('tbl_moduletype', 'tbl_module.moduleType=tbl_moduletype.id', 'left');
        
        if (count($conditions)) {
            $this->db->where($conditions);
        } else {
            $this->db->where('user_id', $loggedUserId);
        }
        
        if (isset($conditions['moduleType'])) {
            if ($conditions['moduleType'] == 3 || $conditions['moduleType'] == 4) {
                $sub_q = $this->db->query("SELECT module_id FROM tbl_student_answer")->result_array();
               
                if (!empty($sub_q)) {
                    $this->db->where_not_in('tbl_module.id', array_column($sub_q, 'module_id'));
                }
            }
        }
        
        
        $this->db->order_by('tbl_module.ordering', 'ASC');
        
        $res = $this->db->get('tbl_module') ->result_array();
        //echo $this->db->last_query();
        return $res;
    } // end allModule()
    
    public function getStudentByGradeCountry($student_grade, $country_id, $user_id)
    {
        $this->db->select('tbl_useraccount.*,tbl_enrollment.sct_id,sct_type,st_id');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_enrollment', 'tbl_useraccount.id = tbl_enrollment.st_id', 'LEFT');
        
        $this->db->where('tbl_useraccount.student_grade', $student_grade);
        $this->db->where('tbl_useraccount.country_id', $country_id);
        $this->db->where('tbl_enrollment.sct_id', $user_id);

        $query = $this->db->get();
               // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function getIndividualStudent($student_grade, $tutor_type, $country_id, $subject_name, $user_id, $course_id)
    {
        /*echo 'two';
        print_r(strip_tags(trim(htmlspecialchars($subject_name, ENT_QUOTES))));
        die;*/
        $this->db->select('tbl_useraccount.*');
        $this->db->from('tbl_useraccount');
        
        if ($tutor_type == 3) {
            $this->db->join('tbl_enrollment', 'tbl_useraccount.id = tbl_enrollment.st_id', 'LEFT');
            $this->db->where('tbl_enrollment.sct_id', $user_id);
        }
        if($tutor_type == 7 && $course_id != '') {
            $this->db->join('tbl_registered_course', 'tbl_useraccount.id = tbl_registered_course.user_id', 'LEFT');
            $this->db->join('tbl_course', 'tbl_registered_course.course_id = tbl_course.id', 'LEFT');
            $this->db->join('tbl_subject', 'tbl_course.courseName = tbl_subject.subject_name', 'LEFT');
            // $this->db->where('tbl_course.courseName', strip_tags(trim(html_entity_decode($subject_name, ENT_QUOTES))));
			$this->db->where('tbl_course.id', $course_id);
        }
        
        $this->db->where('tbl_useraccount.student_grade', $student_grade);
        if ($country_id != '') {
            $this->db->where('tbl_useraccount.country_id', $country_id);
        }
        
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }
}//end class
