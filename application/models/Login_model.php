<?php

class Login_model extends CI_Model{


	public function login_check_info($user_name,$password) {
		$this->db->select('*');
		$this->db->from('tbl_useraccount');
		$this->db->where('user_email',$user_name);
		$this->db->where('user_pawd',md5($password));
//        $this->db->where('status',1);

		$query_result = $this->db->get();
		$result = $query_result->row();
		return $result;
	}

    /**
	* Check if a user email exists in DB or not
	* @param  string $email user email
	* @return boolean       0=>not exists, 1=>exists
	*/
	public function emailCheck( $email )
	{
		$res = $this->db
		->where('user_email', $email)
		->get('tbl_useraccount')
		->result_array();
		return ( count( $res )>0 ) ? 1:0;
	}

}
