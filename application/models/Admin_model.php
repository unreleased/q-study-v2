<?php

class Admin_model extends CI_Model
{
   
    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);
    }

    public function deleteInfo($table, $colName, $colValue)
    {
        //echo $table . '+' .$colName.'+'.$colValue;//die;
        $this->db->where($colName, $colValue);
        $this->db->delete($table);
    }


    /**
     * Copy columns
     *
     * @param strign $source_table source table
     * @param string $conditionCol where column
     * @param mixed  $ConditionVal where column val
     * @param string $changeCol    column to change
     * @param mixed  $changeVal    val of column to change
     *
     * @return void
     */
    public function copy($source_table, $conditions, $changeCol = 0, $changeVal = 0)
    {
        $condition = '';
        
        $flag = 0;
        $totCond = count($conditions);
        foreach ($conditions as $key => $value) {
            $condition .= "`". $key ."`=".$value."";
            
            if (++$flag < $totCond) {
                $condition .= ' AND ';
            }
        }
        $this->db->query("CREATE TEMPORARY TABLE temp_table AS SELECT * FROM $source_table where $condition");
        if ($changeCol) {
            $this->db->query("UPDATE temp_table SET $changeCol=$changeVal,id=0");
        } else {
            $this->db->query("UPDATE temp_table SET id=0");
        }

        $this->db->query("INSERT INTO $source_table SELECT * FROM temp_table");
        $insertId = $this->db->insert_id();
        $this->db->query("DROP TEMPORARY TABLE temp_table");

        return $insertId;
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->result_array();
    }

    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function total_registered()
    {
        $this->db->select('tbl_useraccount.*,tbl_usertype.userType,tbl_country.countryName');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_usertype', 'tbl_useraccount.user_type = tbl_usertype.id', 'LEFT');
        $this->db->join('tbl_country', 'tbl_useraccount.country_id = tbl_country.id', 'LEFT');
        
        $this->db->where('user_type != ', 0);
        $this->db->where('user_type != ', 7);

        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function today_registered()
    {
        $this->db->select('count(*) AS total_registered');
        $this->db->from('tbl_useraccount');
        
        $this->db->where('user_type != ', 0);
        $this->db->where('user_type != ', 7);
//        $this->db->where('user_type != ', 7);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function tutor_with_10_student()
    {
        $query = $this->db->query('SELECT *
                                    FROM tbl_useraccount t 
                                    LEFT JOIN tbl_country ON t.country_id = tbl_country.id 
                                    LEFT JOIN tbl_usertype ON t.user_type = tbl_usertype.id 
                                    WHERE ((SELECT count(*) FROM tbl_enrollment c WHERE c.sct_id = t.id)) >= 10 AND 
                                    user_type = 3');
        return $query->result_array();
    }
    
    public function tutor_with_50_vocabulary()
    {
        $query = $this->db->query('SELECT * '
                                . 'FROM tbl_useraccount t '
                                . 'LEFT JOIN tbl_country ON t.country_id = tbl_country.id 
                                   LEFT JOIN tbl_usertype ON t.user_type = tbl_usertype.id '
                                . 'WHERE ((SELECT COUNT(*) FROM tbl_question q WHERE q.user_id = t.id AND q.questionType = 3 HAVING COUNT(*))) >= 50 AND '
                                . 'user_type = 3 ');
        return $query->result_array();
    }
    
    public function get_todays_data($date)
    {
        $query = $this->db->query('SELECT COUNT(*) AS today_registered FROM `tbl_useraccount` WHERE DATE(FROM_UNIXTIME(created)) LIKE "%'.$date.'%" ');
        return $query->result_array();
    }
    
    
    //Course Schedule
    public function get_course($subscription_type, $user_type, $country_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_course');
        
        $this->db->where('subscription_type', $subscription_type);
        $this->db->where('user_type', $user_type);
        $this->db->where('country_id', $country_id);

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Course name by course ID
     *
     * @param integer $courseId course ID
     *
     * @return string            course name
     */
    public function courseName($courseId)
    {
        $res = $this->db
            ->select('courseName')
            ->where('id', $courseId)
            ->get('tbl_course')
            ->result_array();

        return isset($res[0]) ? $res[0]['courseName'] : '';
    }

    /**
     * Where not in functionality
     *
     * @param string $table       table name to filter through
     * @param string $selectorCol selector column
     * @param array  $filter      get all columns without these items
     *
     * @return array               result array
     */
    public function whereNotIn($table, $selectorCol, $filter)
    {
        $res = $this->db
            ->where_not_in($selectorCol, $filter)
            ->get($table)
            ->result_array();

        return $res;
    }

    /**
     * Where in functionality
     *
     * @param string $table       table name to filter through
     * @param string $selectorCol selector column
     * @param array  $values      get all columns with these items
     *
     * @return array               result array
     */
    public function whereIn($table, $selectorCol, $values = [], $conditions = [])
    {
        $res = $this->db
            ->where_in($selectorCol, $values)
            ->where($conditions)
            ->get($table)
            ->result_array();

        return $res;
    }

    /**
     * Insert multiple data
     *
     * @param string $table table to insert
     * @param array  $data  data to insert
     *
     * @return id            last insert if
     */
    public function insertBatch($table, $data)
    {
        $this->db->insert_batch($table, $data);
        return $this->db->insert_id();
    }
    public function updateSmsApiSettings($a_settings_grop, $a_settings_key, $a_settings_value)
    {
        $this->db->set('setting_value', $a_settings_value);
        $this->db->where('setting_type', $a_settings_grop);
        $this->db->where('setting_key', $a_settings_key);
        $rs = $this->db->update('tbl_setting');
        return 1;
    }
    
    public function getSmsApiKeySettings()
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', 'sms_api_settings');

        $query_result = $this->db->get();
        return $num_rows = $query_result->result_array();
    }
    
    public function getSmsMessageSettings()
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', 'sms_message_settings');

        $query_result = $this->db->get();
        return $num_rows = $query_result->result_array();
    }
    
    public function get_settings_info($setting_type, $setting_key)
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', $setting_type);
        $this->db->where('setting_key', $setting_key);

        $query_result = $this->db->get();
        return $query_result->result_array();
    }

    /**
     * search from any table using conditions
     *
     * @param  string $tableName table to perform search
     * @param  array  $params    conditions array
     * @return [type]            [description]
     */
    public function search($tableName, $params)
    {
        $res = $this->db
            ->where($params)
            ->get($tableName)
            ->result_array();

        return $res;
    }

    /**
     * Multiple row delete
     *
     * @param string $table $table to perform
     * @param array  $ids   ids to delete
     *
     * @return void
     */
    public function delMulti($table, $ids)
    {
        $this->db
        ->where_in('id', $ids)
        ->delete($table);
    }
}
