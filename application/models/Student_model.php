<?php

class Student_model extends CI_Model
{

    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);
    }

    public function deleteInfo($table, $colName, $colValue)
    {
        $this->db->where($colName, $colValue);
        $this->db->delete($table);
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    
    //    Module Section
    public function userInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_country', 'tbl_useraccount.country_id = tbl_country.id', 'LEFT');
        $this->db->join('zone', 'UPPER(tbl_country.countryCode) = zone.country_code', 'LEFT');
        $this->db->where('tbl_useraccount.id', $user_id);

        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function module_info($module_id, $module_type, $student_grade)
    {
        $this->db->select('*');
        $this->db->from('tbl_module');

        $this->db->where('id', $module_id);
        $this->db->where('moduleType', $module_type);
        $this->db->where('studentGrade', $student_grade);
        
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_sct_enrollment_info($stId, $sctType)
    {
        $this->db->select('tbl_useraccount.*,tbl_enrollment.sct_id,tbl_enrollment.st_id');
        $this->db->from('tbl_useraccount');
        
        $this->db->join('tbl_enrollment', 'tbl_useraccount.id=tbl_enrollment.sct_id');
        $this->db->where('tbl_useraccount.user_type', $sctType);
        $this->db->where('tbl_useraccount.parent_id', null);
        $this->db->where('tbl_enrollment.st_id', $stId);
        
        return $this->db->get()->result_array();
    }
    
    public function getLinkInfo($table, $colName1, $colName2, $colValue1, $colValue2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName1, $colValue1);
        $this->db->where($colName2, $colValue2);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function delete_enrollment($userType, $user_id)
    {
        $this->db->where('sct_type', $userType);
        $this->db->where('st_id', $user_id);
        $this->db->delete('tbl_enrollment');
        return;
    }
    
    public function getStudentRefLink($stId)
    {
        $this->db->select('tbl_useraccount.*,tbl_enrollment.sct_id,tbl_enrollment.st_id');
        $this->db->from('tbl_useraccount');
        $this->db->join('tbl_enrollment', 'tbl_useraccount.id=tbl_enrollment.sct_id');
        $this->db->where('tbl_enrollment.st_id', $stId);
        return $this->db->get()->result_array();
    }

    public function studentProgress($conditions)
    {

        $tutorType  = $_SESSION['userType'];
        $userId  = $_SESSION['user_id'];
        $tutorModule = $this->db
            ->select('id')
            ->where('user_id', $userId)
            ->get('tbl_module')
            ->result_array();
         $tutorModule = array_column($tutorModule, 'id');
         
        $res = $this->db
            ->where($conditions);
            //->where("module in (SELECT * from tbl_module where user_id = $userId)");
        if ($tutorType==7 ||$tutorType==3) {
            $res = $res->where_in('module', $tutorModule);
        }
            $res= $res->order_by('answerTime', 'ASC')
            ->get('tbl_studentprogress')
            ->result_array();
        return $res;
    }

    public function studentByClass($class)
    {
        $res = $this->db
            ->where('student_grade', $class)
            ->get('tbl_useraccount')
            ->result_array();

        return $res;
    }

    public function studentName($studentId)
    {
        $res = $this->db
            ->select('name')
            ->where('id', $studentId)
            ->get('tbl_useraccount')
            ->result_array();

        return isset($res[0]['name'])?$res[0]['name']:'';
    }

    /**
     * Get all students of a specific tutor/school/corporate/parent.
     *
     * @param array $conditions [column_name=>value,...]
     *                          for tutor,qstudy etc:['sct_id'=>loggedUserId]
     *
     * @return array             studentIds ex:[1,2,3,4,5]
     */
    public function allStudents($sct_id, $country_id)
    {
        $loggedUserId = $this->session->userdata('user_id');
        $loggedUserType = $this->session->userdata('userType');
        
        if ($loggedUserType == 1) { //parent
            $res = $this->db
                ->select('id as `st_id`')
                ->where('parent_id', $loggedUserId)
                ->get('tbl_useraccount')
                ->result_array();
        } elseif ($loggedUserType == 7) { //q-stydy will get all students
            $this->db->select('id as `st_id`');
//            $this->db->where('user_type', 2); //upper student
            $this->db->where_in('user_type', array(2,6)); //upper student
//            $this->db->or_where('user_type', 6); //lower student
            if (isset($country_id) && $country_id != '') {
                $this->db->where('country_id', $country_id);
            }

            $query = $this->db->get('tbl_useraccount');
            $res = $query->result_array();
        } else { //corporate/school/tutor
            $this->db->select('st_id');
            $this->db->where('sct_id', $sct_id);
            
            $query = $this->db->get('tbl_enrollment');
            $res = $query->result_array();
        }
//        echo $this->db->last_query();
        return array_column($res, 'st_id');
    }
    
    //    public function all_module_by_type($tutorType, $module_type, $desired_result,$registered_course,$conditions)
    public function all_module_by_type($tutorType, $desired_result, $registered_course, $conditions)
    {
        $this->db->select('tbl_module.*,tbl_subject.subject_name,tbl_chapter.chapterName,tbl_useraccount.image');
        $this->db->from('tbl_module');
        
        $this->db->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT');
        $this->db->join('tbl_chapter', 'tbl_module.chapter = tbl_chapter.id', 'LEFT');
        $this->db->join('tbl_useraccount', 'tbl_useraccount.user_type = tbl_module.user_type', 'LEFT');
        
        if (count($conditions)) {
            $this->db->where($conditions);
        }
        
        $this->db->where_in('tbl_module.course_id', $registered_course);
//        Newly Added
        if ($desired_result != '') {
            $this->db->where('tbl_module.subject', $desired_result);
        }
      
        if ($tutorType == 7) {
            $this->db->where('tbl_useraccount.user_type', $tutorType);
        } else {
            $this->db->where('tbl_module.user_id', $tutorType);
        }
      
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function subjectInfo($tutorType)
    {
        $this->db->select('*');
        $this->db->from('tbl_subject');
        
        $this->db->join('tbl_useraccount', 'tbl_subject.created_by = tbl_useraccount.id', 'LEFT');
        
        $this->db->where('tbl_useraccount.user_type', $tutorType);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_tutor_link($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_enrollment');
        
        $this->db->join('tbl_useraccount', 'tbl_enrollment.sct_id = tbl_useraccount.id', 'LEFT');
        
        $this->db->where('tbl_enrollment.st_id', $user_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }

    
    //unreleased(7-31-18)
    public function studentClass($studentId)
    {
        $res = $this->db->select('student_grade')
            ->where('id', $studentId)
            ->get('tbl_useraccount')
            ->result_array();


        return isset($res[0]['student_grade']) ? $res[0]['student_grade']:0;
    }
    //unreleased(7-31-18)
    
    
    public function get_all_tutor_link_with_module($user_id, $module_type)
    {
        $this->db->select('tbl_enrollment.*,tbl_useraccount.name, tbl_module.id AS module_id,moduleName,trackerName,individualName,subject,chapter,country,tbl_subject.subject_name,tbl_chapter.chapterName');
        $this->db->from('tbl_enrollment');
        
        $this->db->join('tbl_useraccount', 'tbl_enrollment.sct_id = tbl_useraccount.id', 'LEFT');
        $this->db->join('tbl_module', 'tbl_useraccount.id = tbl_module.user_id', 'LEFT');
        $this->db->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT');
        $this->db->join('tbl_chapter', 'tbl_module.chapter = tbl_chapter.id', 'LEFT');
        
        $this->db->where('tbl_enrollment.st_id', $user_id);
        $this->db->where('tbl_module.moduleType', $module_type);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_subject($user_type)
    {
        $this->db->select('subject_id');
        $this->db->from('tbl_course');
        
        $this->db->join('tbl_subject', 'tbl_subject.subject_name = tbl_course.courseName', 'LEFT');
        $this->db->join('tbl_useraccount', 'tbl_useraccount.id = tbl_subject.created_by', 'LEFT');

        $this->db->where('tbl_useraccount.user_type', $user_type);
        $this->db->distinct();
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_all_subject_for_registered_student($id)
    {
        $this->db->select('subject_id');
        $this->db->from('tbl_registered_course');
        
        $this->db->join('tbl_course', 'tbl_course.id = tbl_registered_course.course_id', 'LEFT');
        $this->db->join('tbl_subject', 'tbl_subject.subject_name = tbl_course.courseName', 'LEFT');

        $this->db->where('tbl_registered_course.user_id', $id);
        $this->db->distinct();
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    
    /**
     * Match student enrolled courses with subject table
     *
     * @param  integer $studentId logged student id
     * @return array              subjects info
     */
    public function studentSubjects($studentId)
    {
        $temp = $this->studentCourses($studentId);
//        $studentCourses = array_column($temp, 'courseName');
        $studentCourses = array_column($temp, 'id');
        
        $res = $this->db
        ->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT')//Newly Added
        ->where_in('tbl_module.course_id', $studentCourses)
        ->get('tbl_module')
//        ->where_in('tbl_subject.subject_name', $studentCourses)
//        ->get('tbl_subject')
        ->result_array();
//        echo $this->db->last_query();
        return $res;
    }
    
    public function get_tutor_subject($tutor_id)
    {
        $res = $this->db
        ->join('tbl_subject', 'tbl_module.subject = tbl_subject.subject_id', 'LEFT')//Newly Added
        ->where('tbl_module.user_id', $tutor_id)
        ->get('tbl_module')
//        ->where_in('tbl_subject.subject_name', $studentCourses)
//        ->get('tbl_subject')
        ->result_array();
//        echo $this->db->last_query();
        return $res;
    }

    /**
     * Student enrolled courses.
     *
     * @param  integer $studentId student id
     * @return array              all courses of a student
     */
    public function studentCourses($studentId)
    {
        $res = $this->db
            ->join('tbl_course', 'tbl_course.id = tbl_registered_course.course_id', 'left')
            ->where('user_id', $studentId)
            ->get('tbl_registered_course')
            ->result_array();

        return $res;
    }

    /**
     * Get chapters of a individual/multiple subject/s.
     *
     * @param  integer $subjectId subject id/s
     * @return array              chapters
     */
    public function chaptersOfSubject($subjectId)
    {
        
        if (count($subjectId)>1) {
            $this->db->where_in('subjectId', $subjectId);
        } else {
            $this->db->where('subjectId', $subjectId);
        }
        
        $res = $this->db
            ->get('tbl_chapter')
            ->result_array();

        return $res;
    }
    
    /**
     * Get all tutors info of a student
     *
     * @param  integer $studentId student id
     * @return array            tutor ids
     */
    public function allTutor($studentId)
    {
        return $this->db
            ->join('tbl_useraccount', 'tbl_useraccount.id=tbl_enrollment.sct_id', 'left')
            ->where('st_id', $studentId)
            ->get('tbl_enrollment')
            ->result_array();
    }
   
   
    //Cron Job Controller Section
    public function get_all_time_zone()
    {
        $this->db->select('*');
        $this->db->from('tbl_country');

        $this->db->join('zone', 'UPPER(tbl_country.countryCode) = zone.country_code', 'LEFT');
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_parent_with_children($country_id, $today_time)
    {
        $query = $this->db->query(
            'select parent.name AS parent_name,parent.id AS parentid,parent.user_mobile AS parent_mobile,countryCode,countryName,parent.country_id, child.id AS childid,child.name AS child_name,tbl_student_answer.created_at '
                . 'from tbl_useraccount child '
                . 'left join tbl_useraccount parent on child.parent_id = parent.id '
                . 'left join tbl_country on parent.country_id = tbl_country.id '
                . 'left join tbl_student_answer on child.id = tbl_student_answer.st_id '
            . 'WHERE parent.id = child.parent_id AND tbl_student_answer.`created_at` < "'.$today_time.'" AND parent.country_id = '.$country_id
        );
        
        return $query->result_array();
    }
    
    public function send_msg($parent_key, $today_time)
    {
        $this->db->select('*');
        $this->db->from('user_send_msg');

        $this->db->where('parent_id', $parent_key);
        $this->db->like('created_at', $today_time);
        
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_student_today_ans($childid, $today_time)
    {
        $this->db->select('*');
        $this->db->from('tbl_student_answer');

        $this->db->where('st_id', $childid);
        $this->db->like('created_at', $today_time);
        
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    // End Cronjob Controller Section
    
    public function country_code($countryCode)
    {
        $this->db->select('*');
        $this->db->from('zone');

        $this->db->where('country_code', $countryCode);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function getTutorialAnsInfo($table_name, $module_id, $std_id)
    {
        $this->db->select('*');
        $this->db->from($table_name);

        $this->db->where('st_id', $std_id);
        $this->db->where('module_id', $module_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function update_st_error_ans($question_order_id, $module_id, $user_id)
    {
        $this->db->set('error_count', 'error_count+1', false);

        $this->db->where('st_id', $user_id);
        $this->db->where('module_id', $module_id);
        $this->db->where('question_order_id', $question_order_id);
        
        $this->db->update('tbl_st_error_ans');
        //        echo $this->db->last_query();
    }
    
    public function get_specific_error_ans($question_id, $question_order_id, $module_id, $user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_st_error_ans');

        $this->db->where('st_id', $user_id);
        $this->db->where('module_id', $module_id);
        $this->db->where('question_id', $question_id);
        $this->db->where('question_order_id', $question_order_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
   
    public function get_count_std_error_ans($question_order_id, $module_id, $user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_st_error_ans');
        
        $this->db->where('st_id', $user_id);
        $this->db->where('module_id', $module_id);
        $this->db->where('question_order_id', $question_order_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function delete_st_error_ans($question_order_id, $module_id, $user_id)
    {
        $this->db->where('st_id', $user_id);
        $this->db->where('module_id', $module_id);
        $this->db->where('question_order_id', $question_order_id);
        
        $this->db->delete('tbl_st_error_ans');
    }
    
    public function student_error_ans_info($std_id, $module_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_st_error_ans');

        $this->db->where('st_id', $std_id);
        $this->db->where('module_id', $module_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_std_error_ans($std_id, $modle_id, $question_order_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_st_error_ans');

        $this->db->join('tbl_modulequestion', 'tbl_modulequestion.question_id = tbl_st_error_ans.question_id', 'LEFT');
        
        $this->db->where('tbl_st_error_ans.st_id', $std_id);
        $this->db->where('tbl_modulequestion.question_order', $question_order_id);
        $this->db->where('tbl_modulequestion.module_id', $modle_id);
        
        $query = $this->db->get();
        //        echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function student_module_ans_info($std_id, $module_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_student_answer');

        $this->db->where('st_id', $std_id);
        $this->db->where('module_id', $module_id);
        
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    
    public function get_student_progress($user_id, $module)
    {
        $this->db->select('*');
        $this->db->from('tbl_studentprogress');

        $this->db->where('student_id', $user_id);
        $this->db->where('module', $module);
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_answer_repeated_module($user_id, $module, $today)
    {
        $this->db->select('*');
        $this->db->from('tbl_answer_repeated_module');

        $this->db->where('std_id', $user_id);
        $this->db->where('repeat_module_id', $module);
        $this->db->where('answered_date', $today);
        
        $query = $this->db->get();
        return $query->result_array();
    }
   
   
    /**
     * Get all tutuor of a student by a module type
     *
     * @param  integer $moduleType          ex:1,2,3,4//tutorial,everyday study etc
     * @param  array   $loggedStudentsTutor array of tutor enrolled by a student
     * @return array                      tutor list
     */
    public function allTutorByModuleType($moduleType, $loggedStudentsTutor)
    {
        $res = $this->db
            -> distinct()
            ->select('user_id, tbl_useraccount.name tutorName')
            ->join('tbl_useraccount', 'tbl_useraccount.id=tbl_module.user_id', 'left')
            ->where('moduleType', $moduleType)
            ->where_in('user_id', $loggedStudentsTutor)
            ->get('tbl_module')
            ->result_array();
       
        return $res;
    }
    
    //Get All assigners. Q-study, enrolled tutor, schools and corporate
    public function all_assigners($loggedStudentId)
    {
        return $this->db->query(
            "SELECT * FROM `tbl_useraccount` "
                . "WHERE user_type = 7 OR "
                . "id IN (SELECT sct_id FROM tbl_enrollment WHERE st_id = $loggedStudentId) AND "
            . "`tbl_useraccount`.parent_id IS NULL AND user_type != 1"
        )->result_array();
    }
    
    public function getSmsApiKeySettings()
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', 'sms_api_settings');

        $query_result = $this->db->get();
        return $query_result->result_array();
    }

    public function get_sms_response_after_module()
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', 'sms_message_settings');
        $this->db->where('setting_key', 'send_sms_after_module_ans');

        $query_result = $this->db->get();
        return $query_result->result_array();
    }
    
    public function get_sms_response_after_9pm()
    {
        $this->db->select('*');
        $this->db->from('tbl_setting');
        $this->db->where('setting_type', 'sms_message_settings');
        $this->db->where('setting_key', '9_pm_Sms');

        $query_result = $this->db->get();
        return $query_result->result_array();
    }
    
    public function get_today_dialogue($today_date)
    {
        $this->db->select('*');
        $this->db->from('dialogue');
        $this->db->where('date_to_show', $today_date);

        $query_result = $this->db->get();
        return $query_result->result_array();
    }
    
    public function get_whole_year_dialogue($year)
    {
        $this->db->select('*');
        $this->db->from('dialogue');
        $this->db->where('show_whole_year', $year);
        
        $this->db->limit(1);

        $query_result = $this->db->get();
        return $query_result->result_array();
    }

        /**
     * search from any table using conditions
     *
     * @param  string $tableName table to perform search
     * @param  array  $params    conditions array
     * @return [type]            [description]
     */
    public function search($tableName, $params)
    {
        $res = $this->db
            ->where($params)
            ->get($tableName)
            ->result_array();

        return $res;
    }

    public function delete($tableName, $conditions)
    {
        return $res = $this->db
            ->where($conditions)
            ->delete($tableName);
    }
}
