<?php

class Student extends CI_Controller
{
    public $loggedUserId, $loggedUserType;
    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');
        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->model('tutor_model');
        $this->load->model('Preview_model');
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('upload');

        $this->loggedUserId = $this->session->userdata('user_id');
        
        $user_info = $this->Preview_model->userInfo($user_id);

        if ($user_info[0]['countryCode'] == 'any') {
            $user_info[0]['zone_name'] = 'Australia/Lord_Howe';
        }

        $this->site_user_data = array(
            'userType' => $user_type,
            'zone_name' => $user_info[0]['zone_name'],
            'student_grade' => $user_info[0]['student_grade'],
        );
    }

    public function index()
    {
        //removing tutorial module answer
        $this->Student_model->deleteInfo('tbl_temp_tutorial_mod_ques', 'st_id', $this->session->userdata('user_id'));
        
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/students_dashboard', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function student_setting()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/student_setting', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function student_details()
    {
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['studentRefLink'] = $this->Student_model->getStudentRefLink($this->session->userdata('user_id'));
        $data['student_course'] = $this->Student_model->studentCourses($this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/student_details', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function update_student_details()
    {
        // $this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]');
        // $this->form_validation->set_rules('passconf', 'passconf', 'trim|matches[password]');
        // if ($this->form_validation->run() == false) {
            // redirect('student_details');
        // } else {
            // $password = md5($this->input->post('password'));
            // $grade = $this->input->post('student_grade');

            // $data = array(
                // 'user_pawd' => $password,
                // 'student_grade' => $grade,
            // );
            // $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->loggedUserId, $data);
            // $this->session->set_flashdata('success_msg', 'Account updated successfully!');
            // redirect('student_details');
        // }
        
        if ($this->input->post('password')) {
            $data['user_pawd'] = md5($this->input->post('password'));
        }
        
        $data['student_grade'] = $this->input->post('student_grade');

        $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->loggedUserId, $data);
        $this->session->set_flashdata('success_msg', 'Account updated successfully!');
        redirect('student_details');
    }

    public function my_enrollment()
    {
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['get_involved_teacher'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 3);
        $data['get_involved_school'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 4);
        $data['get_involved_corporate'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 5);
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/my_enrollment_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function save_ref_link()
    {
        $data_link = $this->input->post('link');
        if (!empty($data_link)) {
            $userType = $this->input->post('userType');
            $j = 0;
            foreach ($data_link as $single_link) {
                if ($single_link) {
                    $get_link_validate = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);
                    if (!$get_link_validate) {
                        $j++;
                    }
                }
            }
            
            if ($j > 0) {
                echo 2;
            } else {
                //                $this->Student_model->delete_enrollment($userType, $this->session->userdata('user_id'));
                foreach ($data_link as $single_link) {
                    $get_link_status = $this->Student_model->getInfo('tbl_useraccount', 'SCT_link', $single_link);
                    //                    $get_link_status = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);

                    if ($get_link_status) {
                        foreach ($get_link_status as $row) {
                            $enrollment_info = $this->Student_model->getLinkInfo('tbl_enrollment', 'sct_id', 'st_id', $row['id'], $this->session->userdata('user_id'));
                            if (!$enrollment_info) {
                                $link['sct_id'] = $row['id'];
                                $link['sct_type'] = $row['user_type'];
                                $link['st_id'] = $this->session->userdata('user_id');
                                $this->Student_model->insertInfo('tbl_enrollment', $link);
                            }
                        }
                    }
                }

                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function get_ref_link()
    {
        $user_type = $this->input->post('user_type');
        $st_id = $this->session->userdata('user_id');
        $enrollment_info = $this->Student_model->get_sct_enrollment_info($st_id, $user_type);
        echo json_encode($enrollment_info);
    }

    public function removeRefLink()
    {
        $post = $this->input->post();
        $ref= $post['sct_link'];
        $tutorInfo = $this->Student_model->search('tbl_useraccount', ['sct_link'=>$ref]);
        if (!isset($tutorInfo[0]['id'])) {
            echo 'Tutor not exists';
            return 0;
        }
        $tutorId = $tutorInfo[0]['id'];

        $conditions = [
            'st_id'=>$this->loggedUserId,
            'sct_id' =>$tutorId,
        ];
        $this->Student_model->delete('tbl_enrollment', $conditions);
        echo $this->db->last_query();
    }

    public function student_upload_photo()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/upload', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function upload_user_photo_options()
    {
        $config = array();
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_width'] = 1080;
        // $config['max_height'] = 640;
        // $config['min_width']  = 150;
        // $config['min_height'] = 150;
        $config['overwrite'] = false;
        return $config;
    }

    public function sure_student_photo_upload()
    {
        $this->upload->initialize($this->upload_user_photo_options());
        if (!$this->upload->do_upload('file')) {
            echo 0;
        } else {
            $imageName = $this->upload->data();
            $user_profile_picture = $imageName['file_name'];
            $data = array(
                'image' => $user_profile_picture
            );
            $rs['res'] = $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    }
    
    public function view_course()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/view_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function q_study_course()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 7;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function all_module_by_type($tutorType, $module_type)
    {
        $session_module_info = $this->session->userdata('data');
        
        $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $session_module_info[1]['module_id'], $this->session->userdata('user_id'));
        if ($tutorial_ans_info) {
            $this->Student_model->deleteInfo('tbl_temp_tutorial_mod_ques', 'id', $tutorial_ans_info[0]['id']);
        }
        
        $this->session->unset_userdata('data');
        $this->session->unset_userdata('obtained_marks');
        $this->session->unset_userdata('total_marks');
        $this->session->unset_userdata('isFirst');
        
        $user_country = $this->session->userdata('country_id');
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);
        $data['tutorType']    = $tutorType;
        $data['moduleType']   = $module_type;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['studentSubjects'] = $this->Student_model->studentSubjects($this->loggedUserId);

        if ($tutorType == 7) {
            $data['tutorInfo'] = $this->Student_model->getInfo('tbl_useraccount', 'user_type', 7);
            $data['tutorImage'] = isset($data['tutorInfo'])?$data['tutorInfo'][0]['image'] : '';


            $data['all_subject_qStudy'] =$this->Student_model->get_all_subject($tutorType);

            $data['all_subject_student'] =$this->Student_model->get_all_subject_for_registered_student($this->session->userdata('user_id'));

            $first_array_q = array_column($data['all_subject_qStudy'], 'subject_id');
            $second_array_st = array_column($data['all_subject_student'], 'subject_id');

            $desired_result = '';
            $result = array_intersect($first_array_q, $second_array_st);
            if ($result) {
                $desired_result = implode(', ', $result);
            }

            $data['all_module'] = $this->Student_model->all_module_by_type($tutorType, $module_type, $desired_result);
            $data['maincontent'] = $this->load->view('students/qstudy_module/all_tutorial_list', $data, true);
        }
        
        if ($tutorType == 3) {
            if ($module_type == 1) { //tutorial
                $data['maincontent'] = $this->load->view('students/qstudy_module/all_tutorial_list', $data, true);
            } else { //everyday study, special exam, assignment
                $loggedStudentId  = $this->loggedUserId;
                $studentsTutor = $this->Student_model->allTutor($loggedStudentId);

                $data['sct_info'] = $studentsTutor;

                $allTutors = $data['sct_info'];
                $data['module_info'] = $data['sct_info'];

                $studentClass = $this->Student_model->studentClass($this->loggedUserId);

                $studentMods = [];
                foreach ($allTutors as $tutor) {
                    //get all module by tutor_id and logged student class
                    $allModuleConditions = ['user_id'=>$tutor['id'], 'studentGrade'=>$data['user_info'][0]['student_grade'],'moduleType'=>$module_type];

                    $mods = $this->ModuleModel->allModule($allModuleConditions);

                    $sct_info = array();
                    //$sct_info[$tutor['name']]['tutor_id']=$tutor['sct_id'];
                    //if module checked for all students then following students obviously there, so grab it
                    //if module checked for individual student, then check if student id is there -> if yes grab it
                    foreach ($mods as $module) {
                        if ($module['isAllStudent']) {
                            $sct_info[$tutor['name']][] = $module;
                        } elseif (sizeof($module['individualStudent'])) {
                            if ($module['individualStudent']) {
                                $stIds = json_decode($module['individualStudent']);

                                if (in_array($loggedStudentId, $stIds)) {
                                    $sct_info[$tutor['name']][] = $module;
                                }
                            }
                        }
                    }
                }
                
                $data['module_info'] = isset($sct_info)?$sct_info:null;
                // echo "<pre>";
                // print_r($data['module_info']);

                // die;
                
                $data['maincontent'] = $this->load->view('students/tutor_module/tutor_list', $data, true);
            }
        }

        $this->load->view('master_dashboard', $data);
    }

    public function tutorial($tutorType)
    {
        $user_country = $this->session->userdata('country_id');
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);

        if ($tutorType == 7) {
            $data['all_module'] = $this->Student_model->all_module_by_type($user_country, $tutorType);
        }
        if ($tutorType == 3) {
            $sct_info = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), $this->session->userdata('userType'));
            $data['all_module'] = $this->Student_model->module_by_type($user_country, $sct_info[0]['sct_id']);
        }


        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function tutor_course()
    {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 3;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    
    public function video_link($module_id, $module_type)
    {
        $this->session->unset_userdata('data');
        $this->session->unset_userdata('obtained_marks');
        $this->session->unset_userdata('total_marks');

        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));

        $data['module_info'] = $this->Student_model->module_info($module_id, $module_type, $data['user_info'][0]['student_grade']);
        $data['tutor_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $data['module_info'][0]['user_id']);
        
        date_default_timezone_set($data['user_info'][0]['zone_name']);
        $module_time = time();
        //        if($data['module_info'][0]['exam_date'] == $module_time){
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/tutor_module/video_link_by_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    // added by sobuj
    private function openModuleByTutorialBased($modle_id, $question_order_id)
    {
        
        $data['module_info'] = $this->Student_model->getInfo('tbl_module', 'id', $modle_id);
        if (!$data['module_info'][0]) {
            show_404();
        }
        $isFirst = 1;
        
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        // Special Exam
        if (!$this->session->userdata('isFirst')) {
            $this->session->set_userdata('isFirst', $isFirst);
            if ($question_order_id == 1) {
                date_default_timezone_set($this->site_user_data['zone_name']);
                //echo 'Exam Time: '.$data['module_info'][0]['exam_start'].'<br>';
                $exact_time = time();
                $this->session->set_userdata('exact_time', $exact_time);
            }
        }
        
        // Everyday Study & Tutorial
        if ($data['module_info'][0]['moduleType'] == 1
            || $data['module_info'][0]['moduleType'] == 2
            || $data['module_info'][0]['moduleType'] == 4
        ) {
            date_default_timezone_set($this->site_user_data['zone_name']);
            $exact_time = time();
            $this->session->set_userdata('exact_time', $exact_time);
            // echo date('Y-m-d H:i:s',$exact_time);die;
            if ($question_order_id == 1) {
                $this->session->set_userdata('exam_start', $exact_time);
            }
        }

        //****** Get Temp table data for Tutorial Module Type ******
    
        if ($data['module_info'][0]['moduleType'] == 2) {
            $table = 'tbl_student_answer';
        } else {
            $table = 'tbl_temp_tutorial_mod_ques';
        }
    
        $data['tutorial_ans_info'] = $this->Student_model->getTutorialAnsInfo($table, $modle_id, $data['user_info'][0]['id']);
    
        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);
        
       
        /*if (!isset($data['question_info_s'][0])) {
            //question not exists
            show_404();
        }*/
        
        if (!$data['question_info_s'][0]['id']) {
            $question_order_id = $question_order_id + 1;
            redirect('get_tutor_tutorial_module/'.$modle_id.'/'.$question_order_id);
        }
        $data['total_question'] = $this->tutor_model->getModuleQuestion($modle_id, null, 1);
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        //video link classify
        $moduleVidLinks = json_decode($data['module_info'][0]['video_link']);
        
        $data['moduleVid'] = count($moduleVidLinks) ? trim($moduleVidLinks[0]):'';
        
        $url = $data['moduleVid'];
        $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
        $match;

        if (preg_match($regex_pattern, $url, $match)) {
            //echo "Youtube video id is: ".$match[4];
            $data['moduleVidType'] = 'youtube';
        } else {
            //echo "Sorry, not a youtube URL";
            $data['moduleVidType'] = 'general';
        }
        
        
        
        if ($data['question_info_s'][0]['question_type'] == 1) {
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_general', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 2) {
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_true_false', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 3) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_vocabulary', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 4) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_multiple_choice', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 5) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_multiple_response', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 6) {
            $quesInfo = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);

            $data['question_info_s'] = $quesInfo;
            $questionType = $quesInfo[0]['questionType'];
            $quesInfo = json_decode($quesInfo[0]['questionName']);
            $data['question_info_skip'] = json_decode($data['question_info_s'][0]['questionName']);

            $data['numOfRows'] = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols'] = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';

            $data['questionId'] = $data['question_info_s'][0]['question_id'];
            $data['question_id'] = $data['question_info_s'][0]['question_id'];
            
            $quesAnsItem = $quesInfo->skp_quiz_box;
            $items = $this->indexQuesAns($quesAnsItem);
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);

            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_skip', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 7) {
            $data['question_info_left_right'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_matching', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 8) {
            $quesInfo     = json_decode($data['question_info_s'][0]['questionName']);
            $questionBody            = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionBody']    = $questionBody;
            $items                   = $quesInfo->assignment_tasks;
            $data['totalItems']      = count($items);
            $data['assignment_list'] = $this->renderAssignmentTasks($items);
            
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_assignment', $data, true);
            $this->load->view('master_dashboard', $data);
        } elseif ($data['question_info_s'][0]['questionType'] == 9) {
            $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
            $data['question_answer'] = json_decode($data['question_info_s'][0]['answer'], true);
            $data['question_description'] = json_decode($data['question_info_s'][0]['questionDescription'], true);
            //            echo '<pre>';
            //            print_r($data['question_info_s']);die;
            $data['maincontent']     = $this->load->view('students/question_module_type_tutorial/ans_creative_quiz', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 10) {
            $data['question_info']   = json_decode($data['question_info_s'][0]['questionName'], true);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_times_table', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 11) {
            $data['question_info']   = json_decode($data['question_info_s'][0]['questionName'], true);
            $data['maincontent']     = $this->load->view('students/question_module_type_tutorial/ans_algorithm', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 12) {
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_workout_quiz', $data, true);
        } elseif ($data['question_info_s'][0]['question_type'] == 13) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('students/question_module_type_tutorial/ans_matching_workout', $data, true);
        }
        if ($data['question_info_s'][0]['question_type'] != 8) {
            $this->load->view('master_dashboard', $data);
        }
    }
    
    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    public function renderAssignmentTasks(array $items)
    {
        $row = '';
        foreach ($items as $task) {
            $task = json_decode($task);
            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            $row .= '<td><img src="assets/images/icon_details.png"></td>';
            $row .= '<input type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;
    }//end renderAssignmentTasks()

    public function get_tutor_tutorial_module($modle_id, $question_order_id)
    {
        $select = '*';
        $table = 'tbl_module';
        
        $columnName = 'id';
        $columnValue = $modle_id;

        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $module_type = $this->tutor_model->get_all_where($select, $table, $columnName, $columnValue);
        // Get Student Ans From tbl_student_answer
        $flag = 0;
        $get_student_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_student_answer', $modle_id, $this->session->userdata('user_id'));
        if ($module_type[0]['moduleType'] != 2) {
            if ($get_student_ans_info) {
                $flag = 1;
            }
        } if ($module_type[0]['moduleType'] == 2) {
            $repition_days = json_decode($module_type[0]['repetition_days']);
            function fix($n)
            {
                if ($n) {
                    $val = (explode('_', $n));
                    return $val[1];
                }
            }
            $b = array();
            if ($repition_days) {
                $b = array_map("fix", $repition_days);
            }
            
            $today = date('Y-m-d');
            
            if (in_array($today, $b) && $get_student_ans_info) {
                $st_ans = json_decode($get_student_ans_info[0]['st_ans'], true);
                
                foreach ($st_ans as $row) {
                    $get_specific_error_ans = $this->Student_model->get_specific_error_ans($row['question_id'], $question_order_id, $modle_id, $this->session->userdata('user_id'));
                    
                    if ($row['question_order_id'] == $question_order_id && !$get_specific_error_ans) {
                        //$flag = 1;
                    }
                    
                    if ($row['ans_is_right'] == 'correct' && $row['question_order_id'] == $question_order_id) {
                        $flag = 1;
                    }
                }
            }
        }
        
        if (!$module_type || $flag) {
            redirect('error_page');
        }
        
        if ($module_type[0]['moduleType'] == 1) {
            $this->openModuleByTutorialBased($modle_id, $question_order_id);
        } elseif ($module_type[0]['moduleType'] == 2) {
            $this->openModuleByTutorialBased($modle_id, $question_order_id);
        } elseif ($module_type[0]['moduleType'] == 3) {
            $data['module_info'] = $this->Student_model->module_info($modle_id, $module_type[0]['moduleType'], $data['user_info'][0]['student_grade']);
            
            date_default_timezone_set($data['user_info'][0]['zone_name']);
            // date_default_timezone_set('Asia/Dhaka');
            $module_time = time();

            /*echo ($data['module_info'][0]['exam_start']).'<pre>'.date('Y-m-d H:i:s').'<pre>'.($data['module_info'][0]['exam_end']);
            die;*/
            
            if ((strtotime($data['module_info'][0]['exam_start']) < $module_time) && (strtotime($data['module_info'][0]['exam_end']) > $module_time)) {
                $this->openModuleByTutorialBased($modle_id, $question_order_id);
            } elseif (strtotime($data['module_info'][0]['exam_end']) < $module_time) {
                show_404();
            } else {
                $this->session->set_flashdata('message_name', 'Your exam will start at '.date('h:i A', strtotime($data['module_info'][0]['exam_start'])));
                redirect('all_tutors_by_type/'.$module_type[0]['user_id'].'/'.$data['module_info'][0]['moduleType']);
            }
        } elseif ($module_type[0]['moduleType'] == 4) {
            $this->openModuleByTutorialBased($modle_id, $question_order_id);
        }
    }

    private function check_browser_back_next_previlige($gurd_array, $question_order_id)
    {
        if (is_array($gurd_array)) {
            if (array_key_exists($question_order_id, $gurd_array)) {
                $has_module_type = $gurd_array[$question_order_id]['module_type'];
                if ($has_module_type) {
                    if ($has_module_type == 1) {
                        return;
                    } else {
                        $hasData = $gurd_array[$question_order_id]['question_id'];
                        if ($hasData) {
                            $redirect_order_id = $question_order_id + 1;
                            redirect('get_tutor_tutorial_module/' . $gurd_array[$question_order_id]['module_id'] . '/' . $redirect_order_id);
                        } else {
                            return;
                        }
                    }
                }
            }
        } else {
            return;
        }
    }

    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     *
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        // print_r($items);die;
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            //print_r($cr);die;
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
            );
        }
        return $arr;
    }

    /**
     * Render the indexed item to table data for preview
     *
     * @param array   $items   ques ans as indexed item
     * @param int     $rows    num of row in table
     * @param int     $cols    num of cols in table
     * @param integer $showAns optional, set 1 will show the answers too
     *
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {
        //print_r($items);die;
        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .='<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="' . $items[$i][$j]['val'] . '" name="skip_counting[]" class="form-control input-box  rsskpinpt' . $i . '_' . $j . '" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = array(
                        'cr' => $i . '_' . $j,
                        'val' => $items[$i][$j]['val'],
                        'type' => 'a',
                    );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns == 1) ? ' value="' . $items[$i][$j]['val'] . '"' : '';

                    $row .= '<td><input autocomplete="off" type="text" ' . $val . ' data_q_type="0" data_num_colofrow="' . $i . '_' . $j . '" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt' . $i . '_' . $j . '"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }

        return $row;
    }

    private function take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, $answer_info = null)
    {
        //****** Get Temp table data for Tutorial Module Type ******
        $user_id = $this->session->userdata('user_id');
        $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $module_id, $user_id);
        //        print_r($tutorial_ans_info);die;
        $obtained_marks = $this->session->userdata('obtained_marks');
        $total_marks = $this->session->userdata('total_marks');
        $ans_array = $this->session->userdata('data');
        
        $flag = 0;
        
        if (!is_array($ans_array)) {
            $ans_array = array();
            $obtained_marks = 0;
            $total_marks = 0;
            $flag = 0;
        } else {
            $question_idd = '';
            if (isset($ans_array[$question_order_id]['question_id'])) {
                $question_idd = $ans_array[$question_order_id]['question_id'];
            }

            if ($question_id == $question_idd) {
                $flag = 1;
            } else {
                $flag = 0;
            }
        }
        
        $student_ans = '';
        if ($this->input->post('answer')) {
            $student_ans = $this->input->post('answer');
        }
        
        if ($tutorial_ans_info) {
            $temp_table_ans_info = json_decode($tutorial_ans_info[0]['st_ans'], true);
            $flag = 2;
        }
        

        if ($ans_is_right == 'correct') {
            //            echo $ans_is_right;die;
           
            if ($answer_info != null) {
                $student_ans = $answer_info;
                echo $answer_info;
            } else {
                //                    if ($flag != 2) {
                echo 2;
                //                    }
            }
        } else {
            if ($answer_info != null) {
                $student_ans = $answer_info;
                echo $answer_info;
            } else {
                //                if ($flag != 2) {
                echo 3;
                //                }
            }
     
            $question_marks = 0;
        }

        if ($flag == 0) {
            $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);

            $link1 = base_url();
    
            $obtained_marks = $obtained_marks + $question_marks;
            $total_marks = $total_marks + $question_info_ai[0]['questionMarks'];
            $link2 = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;

            $ind_ans = array(
               'question_order_id' => $question_info_ai[0]['question_order'],
               'module_type' => $question_info_ai[0]['moduleType'],
               'module_id' => $question_info_ai[0]['module_id'],
               'question_id' => $question_info_ai[0]['question_id'],
               'link' => $link2,
               'student_ans' => json_encode($student_ans),
               'workout' => $_POST['workout'],
               'student_taken_time' => $_POST['student_question_time'],
               'student_question_marks' => $question_marks,
               'student_marks' => $obtained_marks,
               'ans_is_right' => $ans_is_right
            );
            $ans_array[$question_order_id] = $ind_ans;

            $this->session->set_userdata('data', $ans_array);
            $this->session->set_userdata('obtained_marks', $obtained_marks);
            $this->session->set_userdata('total_marks', $total_marks);
    
            if ($_POST['next_question'] == 0) {
                $total_ans = $this->session->userdata('data', $ans_array);

                $data['st_ans'] = json_encode($total_ans);
                $data['st_id'] = $this->session->userdata('user_id');
                $data['module_id'] = $module_id;

                if (!$tutorial_ans_info) {
                    $this->db->insert('tbl_temp_tutorial_mod_ques', $data);
                }
        
                //                $dec = $this->decesion_redirect($this->session->userdata('user_id'), $module_id);
                //                echo $dec;
            }
        }

        if ($flag == 2) {
            //            for ($i = 1; $i <= count($temp_table_ans_info); $i++) {
            foreach ($temp_table_ans_info as $key => $val) {
                //                echo $question_order_id.'<pre>';
                //                echo '<pre>';print_r($temp_table_ans_info[$key]);
                if ($temp_table_ans_info[$key]['question_order_id'] == $question_order_id) {
                    $temp_table_ans_info[$key]['ans_is_right'] = $ans_is_right;
                }
            }
            $update_value = json_encode($temp_table_ans_info);
            $st_ans['st_ans'] = $update_value;
            $this->Student_model->updateInfo('tbl_temp_tutorial_mod_ques', 'id', $tutorial_ans_info[0]['id'], $st_ans);
    
            //            echo 6;
        }
    }

    private function take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, $answer_info = null)
    {
           // print_r($text_1);die;
        $obtained_marks = $this->session->userdata('obtained_marks');
        $total_marks = $this->session->userdata('total_marks');
        $ans_array = $this->session->userdata('data');
        $user_id = $this->session->userdata('user_id');
    
        $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_student_answer', $module_id, $user_id);
        $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);
    
        $flag = 0;
    
        if ($tutorial_ans_info) {
            $temp_table_ans_info = json_decode($tutorial_ans_info[0]['st_ans'], true);
            $flag = 2;
        }
    
        if (!is_array($ans_array)) {
            $ans_array = array();
            $obtained_marks = 0;
            $total_marks = 0;
        } else {
            $question_idd = '';
            if (isset($ans_array[$question_order_id]['question_id'])) {
                $question_idd = $ans_array[$question_order_id]['question_id'];
            }

            if ($question_id == $question_idd) {
                $flag = 1;
            } else {
                $flag = 0;
            }
        }
    
        if (isset($_POST['answer'])) {
            $student_ans = $_POST['answer'];
        }
    
        if ($ans_is_right == 'correct') {
            if ($answer_info != null && $question_info_ai[0]['moduleType'] == 2) {
                echo $answer_info;
                $student_ans = $answer_info;
            }
        
            if ($answer_info == null && $question_info_ai[0]['moduleType'] == 2) {
                echo 2;
            }
        } else {
            if ($question_info_ai[0]['moduleType'] == 2) {
                if ($answer_info != null && $question_info_ai[0]['moduleType'] == 2) {
                    echo $answer_info;
                    $student_ans = $answer_info;
                } if ($answer_info == null && $question_info_ai[0]['moduleType'] == 2) {
                    echo 3;
                }
            }
        
            $question_marks = 0;
        }
    
        // echo $text;echo 'Flag: ';echo $flag;die;
    
        if ($flag == 2) {
            foreach ($temp_table_ans_info as $key => $val) {
                if ($temp_table_ans_info[$key]['question_order_id'] == $question_order_id) {
                    $temp_table_ans_info[$key]['ans_is_right'] = $ans_is_right;
                    $temp_table_ans_info[$key]['student_ans'] = json_encode($student_ans);
                }
            }
        
            $update_value = json_encode($temp_table_ans_info);
            $st_ans['st_ans'] = $update_value;
            // $this->Student_model->updateInfo('tbl_student_answer', 'id', $tutorial_ans_info[0]['id'], $st_ans);
        
            $count_std_error_ans = $this->Student_model->get_count_std_error_ans($question_order_id, $module_id, $user_id);
        
            if (isset($count_std_error_ans[0]['error_count']) && $count_std_error_ans[0]['error_count'] == 3) {
                $this->Student_model->delete_st_error_ans($question_order_id, $module_id, $user_id);
            } else {
                if ($ans_is_right == 'wrong') {
                    $this->Student_model->update_st_error_ans($question_order_id, $module_id, $user_id);
                }
                if ($ans_is_right == 'correct') {
                    $this->Student_model->delete_st_error_ans($question_order_id, $module_id, $user_id);
                }
            }
        
            if ($question_info_ai[0]['moduleType'] != 2) {
                echo 5;
            }
        } if ($flag == 0) {
            $link1 = base_url();
            $link2 = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;
        
            $obtained_marks = $obtained_marks + $question_marks;
            $total_marks = $total_marks + $question_info_ai[0]['questionMarks'];
        
            $ind_ans = array(
            'question_order_id' => $question_info_ai[0]['question_order'],
            'module_type' => $question_info_ai[0]['moduleType'],
            'module_id' => $question_info_ai[0]['module_id'],
            'question_id' => $question_info_ai[0]['question_id'],
            'link' => $link2,
            'student_ans' => json_encode($student_ans),
            'workout' => isset($_POST['workout']) ? $_POST['workout']:'',
            'student_taken_time' => $_POST['student_question_time'],
            'student_question_marks' => $question_marks,
            'student_marks' => $obtained_marks,
            'ans_is_right' => $ans_is_right
            );
            //echo '<pre>';print_r($ind_ans);die;
            $ans_array[$question_order_id] = $ind_ans;

            $this->session->set_userdata('data', $ans_array);
            $this->session->set_userdata('obtained_marks', $obtained_marks);
            $this->session->set_userdata('total_marks', $total_marks);
            if ($question_info_ai[0]['moduleType'] != 2) {
                echo 5;
            }
        
            if ($_POST['next_question'] == 0) {
                date_default_timezone_set($this->site_user_data['zone_name']);
                $end_time = time();
                $this->session->set_userdata('end_time', $end_time);
            
                $this->save_student_answer($module_id);
            }
        }
    }

    public function save_student_answer($module_id)
    {
        $get_module_info = $this->Student_model->getInfo('tbl_module', 'id', $module_id);
    
        $ans_array = $this->session->userdata('data');
        $obtained_marks = $this->session->userdata('obtained_marks');
        $total_marks = $this->session->userdata('total_marks');
    
        $student_taken_time = $this->session->userdata('end_time') - $this->session->userdata('exam_start');
        foreach ($ans_array as $ans) {
            if ($ans['ans_is_right'] == 'wrong') {
                $data_er['st_id'] = $this->session->userdata('user_id');
                $data_er['question_id'] = $ans['question_id'];
                $data_er['question_order_id'] = $ans['question_order_id'];
                $data_er['module_id'] = $ans['module_id'];
                $data_er['error_count'] = 1;

                $this->db->insert('tbl_st_error_ans', $data_er);
            }
        
            // $student_taken_time = $student_taken_time + $ans['student_taken_time'];
        }
    
        //        $total_ans = $this->session->userdata('data', $ans_array);
    
        $time['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
    
        date_default_timezone_set($time['user_info'][0]['zone_name']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['st_ans'] = json_encode($ans_array);
        $data['st_id'] = $this->session->userdata('user_id');
        $data['module_id'] = $module_id;
        $this->db->insert('tbl_student_answer', $data);
    
        $p_data['timeTaken'] = $student_taken_time;
        $p_data['answerTime'] = $this->session->userdata('exam_start');
        $p_data['originalMark'] = $total_marks;
        $p_data['studentMark'] = $obtained_marks;
        $p_data['student_id'] = $data['st_id'];
        $p_data['module'] = $data['module_id'];
        $p_data['percentage'] = ($obtained_marks * 100) / $total_marks;
        $p_data['moduletype'] = $get_module_info[0]['moduleType'];
    
        $this->db->insert('tbl_studentprogress', $p_data);
    
        $this->session->unset_userdata('data', $ans_array);
    
        //      *****  For Send SMS Message to Parents  *****
        if ($get_module_info[0]['isSMS'] == 1) {
            $v_hours = floor($student_taken_time / 3600);
            $remain_seconds = $student_taken_time - $v_hours * 3600;
            $v_minutes = floor($remain_seconds / 60);
            $v_seconds = $remain_seconds - $v_minutes * 60;
            $time_hour_minute_sec = $v_hours . " : "  . $v_minutes . " : " . $v_seconds ;
        
            $settins_Api_key = $this->Student_model->getSmsApiKeySettings();
            $settins_sms_messsage = $this->Student_model->get_sms_response_after_module();

            $user_email = $this->session->userdata('user_email');
            $totProgress = $this->Student_model->getInfo('tbl_studentprogress', 'student_id', $data['st_id']);
            
            $avg_mark = 0 ;
            if (count($totProgress)) {
                $tot = 0;
                foreach ($totProgress as $progress) {
                    $tot+=$progress['percentage'];
                }
                $avg_mark = round($tot/count($totProgress), 2);
            }

            $get_all_module_question = $this->Student_model->getInfo('tbl_modulequestion', 'module_id', $module_id);
            $get_child_parent_info = $this->Student_model->getInfo('tbl_useraccount', 'id', $time['user_info'][0]['parent_id']);

            $register_code_string = $settins_sms_messsage[0]['setting_value'];
            $find = array("{{user_email}}", "{{marks}}", "{{total_marks}}","{{student_taken_time}}","{{module_name}}","{{avg_mark}}");
            $replace = array($user_email, $obtained_marks, $total_marks, $time_hour_minute_sec, $get_module_info[0]['moduleName'], $avg_mark);

            $message = str_replace($find, $replace, $register_code_string);

            $api_key = $settins_Api_key[0]['setting_value'];
            $content = urlencode($message);
            $url = "https://platform.clickatell.com/messages/http/send?apiKey=$api_key&to=" . $get_child_parent_info[0]['user_mobile'] . "&content=$content";
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            //execute post
            $result = curl_exec($ch);
            curl_close($ch);
        }
    
        //      *****  End For Sending SMS Message to Parents  *****
    
        if ($get_module_info[0]['moduleType'] != 2) {
            // $dec = $this->decesion_redirect($data['st_id'], $data['module_id']);
            // echo $dec;
        }
    }

    private function decesion_redirect($st_id, $module_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_module');
        $this->db->where('id', $module_id);
        $info = $this->db->get()->result_array();
        //        if($info[0]['moduleType'] == 1 || $info[0]['moduleType'] == 2){
        //            return 6;
        //        }
        ////        elseif($info[0]['moduleType'] == 2){
        ////            return 7;
        ////        }
        //        elseif($info[0]['moduleType'] == 3){
        //            return 8;
        //        }
        return 6;
    }

    public function st_answer_matching()
    {
        $question_id = $this->input->post('question_id');
        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['check_order_id'] - 1;
        $question_order_id = $this->input->post('current_order');
        $text = $this->input->post('answer');
        //
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text = strip_tags($text);
        $text = str_replace($find, $repleace, $text);
        $text = trim($text);

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);


        $question_marks = $answer_info[0]['questionMarks'];

        $text_1 = $answer_info[0]['answer'];
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text_1 = strip_tags($text_1);
        $text_1 = str_replace($find, $repleace, $text_1);
        $text_1 = trim($text_1);
    
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }

    public function st_answer_matching_true_false()
    {
        //        echo '<pre>';print_r($_POST);
        $text = $this->input->post('answer');
        $question_id = $this->input->post('question_id');
        $module_id = $this->input->post('module_id');
        $question_order_id = $this->input->post('current_order');
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

        $text_1 = $answer_info[0]['answer'];
    
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }
    
        $question_marks = $answer_info[0]['questionMarks'];
        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }

    public function st_answer_matching_vocabolary()
    {

        $text = strtolower($this->input->post('answer'));
        $question_id = $this->input->post('question_id');
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

        $module_id = $_POST['module_id'];
        // $question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $text_1 = strtolower($answer_info[0]['answer']);
        $question_marks = $this->input->post('obtain_marks');
    
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }

    public function st_answer_matching_multiple_choice()
    {
        //print_r($_POST);die;

        $question_id = $_POST['id'];
        if (isset($_POST['answer'])) {
            $text_1 = $_POST['answer'];
        } else {
            $text_1 = '';
        }

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
    
        $text = $answer_info[0]['answer'];
        $question_marks = $answer_info[0]['questionMarks'];
        $module_id = $_POST['module_id'];
        $question_order_id = $_POST['current_order'];
    
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }
    
        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }

    public function st_answer_matching_multiple_response()
    {
        $question_id = $_POST['id'];
        if (isset($_POST['answer'])) {
            $text_1 = $_POST['answer'];
        } else {
            $text_1 = array();
        }

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

        $question_marks = $answer_info[0]['questionMarks'];
        $text = json_decode($answer_info[0]['answer']);
        $result_count = 1;
        if ($text_1) {
            $result_count = count(array_intersect($text_1, $text));
        }

        $module_id = $_POST['module_id'];
        $question_order_id = $_POST['current_order'];
    
        $ans_is_right = 'correct';
        if (count($text_1) != $result_count) {
            $ans_is_right = 'wrong';
        }

        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }

    public function st_answer_multiple_matching()
    {
        $total = $_POST['total_ans'];

        $question_id = $_POST['id'];
        $st_ans = array();
        for ($i = 1; $i <= $total; $i++) {
            $ans_id = 'answer_' . $i;
            $st_ans[] = $_POST[$ans_id];
        }
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];
        $answer_info['student_ans'] = $st_ans;

        $module_id = $_POST['module_id'];
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $text = 0;
        $text_1 = 0;
        for ($k = 0; $k < sizeof($answer_info['student_ans']); $k++) {
            if ($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
            }
        }
    
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }

        if ($_POST['module_type'] == 1) {
            //echo 11111111;die;
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, json_encode($answer_info));
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, json_encode($answer_info));
        }
    }

    public function st_answer_skip()
    {
        $module_id = $_POST['module_id'];
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $post = $this->input->post();
        $questionId = $this->input->post('id');
        $text = 0;
        $text_1 = 0;
    
        $temp = $this->tutor_model->getInfo('tbl_question', 'id', $questionId);
    
        $answer_info = array();
    
        $question_marks = $temp[0]['questionMarks'];
    
        if (strlen(implode($post['given_ans'])) != 0) {
            $givenAns = $this->indexQuesAns($post['given_ans']);
        
            $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

            $temp2 = json_decode($temp[0]['questionName']);
            $numOfRows = $temp2->numOfRows;
            $numOfCols = $temp2->numOfCols;
            //echo $numOfRows .' ' . $numOfCols;
            $wrongAnsIndices = [];

            $answer_info = $givenAns;
        
            for ($row = 1; $row <= $numOfRows; $row++) {
                for ($col = 1; $col <= $numOfCols; $col++) {
                    if (isset($savedAns[$row][$col])) {
                        $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row . '_' . $col : null;
                    }
                }
            }

            $wrongAnsIndices = array_filter($wrongAnsIndices);
            if (count($wrongAnsIndices)) {//For False Condition
                $text_1 = 1;
            }
        } if (strlen(implode($post['given_ans'])) == 0) {
            $text_1 = 1;
        }
        
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }

        if ($_POST['module_type'] == 1) {
            //echo $text_1;die;
            $this->take_decesion_1($question_marks, $questionId, $module_id, $question_order_id, $ans_is_right, array());
        } else {
            $this->take_decesion_2($question_marks, $questionId, $module_id, $question_order_id, $ans_is_right, array());
        }
    }
    
    public function st_answer_creative_quiz()
    {
        $question_id = $this->input->post('question_id');
        $student_ans = $this->input->post('answer');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];
        
        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $this->input->post('current_order');
        $text = 0;
        $text_1 = 0;
        for ($k = 0; $k < sizeof($student_ans); $k++) {
            if ($student_ans[$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
                $flag = 0;
            }
        }
        
        $ans_is_right = 'correct';
        if ($text != $text_1) {
            $ans_is_right = 'wrong';
        }

        if ($this->input->post('module_type') == 1) {
            //echo 11111111;die;
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        }
    }
    
    public function st_answer_times_table()
    {
        $question_id = $this->input->post('question_id');
        $result = $this->input->post('answer');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];
        $answer_info['student_ans'] = $result;
        
        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $this->input->post('current_order');
        $text = 0;
        $text_1 = 0;
        $flag = 1;
        
        for ($k = 0; $k < sizeof($answer_info['student_ans']); $k++) {
            if ($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
                $flag = 0;
            }
        }
        // echo 'Flag: ';echo $flag;die;
        $ans_is_right = 'correct';
        if ($flag == 0) {
            $ans_is_right = 'wrong';
        }
        
        $answer_info['student_ans'] = $result;
        $answer_info['flag'] = $flag;

        if ($this->input->post('module_type') == 1) {
            //echo 11111111;die;
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        }
    }
    
    public function st_answer_algorithm()
    {
        $question_id = $this->input->post('question_id');
        $result = $this->input->post('answer');
        //        $result['reminder'] = $this->input->post('reminder');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $question_info = json_decode($answer[0]['questionName'], true);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        //        $question_marks = $answer[0]['questionMarks'];
        $question_marks = 0;
        //        $answer_info['student_ans'] = $result;
        
        $module_id = $this->input->post('module_id');
        $question_order_id = $this->input->post('current_order');
        
        
        if ($question_info['operator'] != '/' && $result == $answer_info['tutor_ans']) {
            $ans_is_right = 'correct';
        } elseif ($question_info['operator'] == '/' && $result[1] == $answer_info['tutor_ans']) {
            $ans_is_right = 'correct';
        } else {
            $ans_is_right = 'wrong';
        }
        //        echo $ans_is_right;die;
        if ($this->input->post('module_type') == 1) {
            //echo 11111111;die;
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right, array());
        }
    }
    
    public function st_answer_workout_quiz()
    {
        $question_id = $this->input->post('question_id');
        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['check_order_id'] - 1;
        $question_order_id = $this->input->post('current_order');
        $text = $this->input->post('answer');
        

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $ans_is_right = 'correct';

        $question_marks = 0;


        if ($_POST['module_type'] == 1) {
            $this->take_decesion_1($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        } else {
            $this->take_decesion_2($question_marks, $question_id, $module_id, $question_order_id, $ans_is_right);
        }
    }
    
    public function st_answer_assignment()
    {
        //        echo '<pre>';print_r($_POST);die;
        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $this->input->post('current_order');
        $questionId = $this->input->post('question_id');
        $answer_info = $this->input->post('answer');
        $module_type = $this->input->post('module_type');
        
        $this->take_decesion_for_assignment($questionId, $module_id, $question_order_id, json_encode($answer_info));
    }
    
    public function take_decesion_for_assignment($questionId, $module_id, $question_order_id, $answer_info)
    {
        
        $ans_array = $this->session->userdata('data');
        $user_id = $this->session->userdata('user_id');
        
        $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);
        
        $link1 = base_url();
        $link2 = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;
        $obtained_marks = 0;
        $ans_is_right = 'correct';
        
        $ind_ans = array(
            'question_order_id' => $question_info_ai[0]['question_order'],
            'module_type' => $question_info_ai[0]['moduleType'],
            'module_id' => $question_info_ai[0]['module_id'],
            'question_id' => $question_info_ai[0]['question_id'],
            'link' => $link2,
            'student_ans' => $answer_info,
            'workout' => $this->input->post('workout'),
            'student_taken_time' => $this->input->post('student_question_time'),
            'student_question_marks' => 0,
            'student_marks' => $obtained_marks,
            'ans_is_right' => $ans_is_right
        );

        $ans_array[$question_order_id] = $ind_ans;

        $this->session->set_userdata('data', $ans_array);

        if ($this->input->post('next_question') == 0) {
            $ans_array = $this->session->userdata('data');
            
            date_default_timezone_set($this->site_user_data['zone_name']);
            $end_time = time();
            
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['st_ans'] = json_encode($ans_array);
            $data['st_id'] = $user_id;
            $data['module_id'] = $module_id;
            $this->db->insert('tbl_student_answer', $data);
            
            //            Save on Student Progress Table
            $student_taken_time = $end_time - $this->session->userdata('exam_start');
            $p_data['timeTaken'] = $student_taken_time;
            $p_data['answerTime'] = $this->session->userdata('exam_start');
            $p_data['originalMark'] = 0;
            $p_data['studentMark'] = 0;
            $p_data['student_id'] = $data['st_id'];
            $p_data['module'] = $data['module_id'];
            $p_data['percentage'] = 0;
            $p_data['moduletype'] = $question_info_ai[0]['moduleType'];

            $this->db->insert('tbl_studentprogress', $p_data);

            $this->session->unset_userdata('data', $ans_array);
        }
        
        echo 2;
    }

    public function error()
    {
        $module_id = $this->session->userdata('module_id');

        $student_id = $this->session->userdata('user_id');

        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $student_id);
        $data['student_marks'] = $this->Student_model->student_marks($student_id, $module_id);
        $data['data_error'] = $this->session->userdata('error_data');


        $total_question = json_decode($data['student_marks'][0]['st_ans']);

        $data['total_question'] = count((array) $total_question);

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('error_question_templete/error', $data, true);

        $this->load->view('master_dashboard', $data);
    }

    public function all_tutors_by_type($tutor_id, $module_type)
    {
        $session_module_info = $this->session->userdata('data');
        
        /*if ($session_module_info) {
            $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $session_module_info[1]['module_id'], $this->session->userdata('user_id'));
        }

        if (isset($tutorial_ans_info) && !empty($tutorial_ans_info)) {*/
        $this->Student_model->deleteInfo('tbl_temp_tutorial_mod_ques', 'st_id', $this->session->userdata('user_id'));
        //}
        
        $this->session->unset_userdata('data');
        $this->session->unset_userdata('obtained_marks');
        $this->session->unset_userdata('total_marks');
        $this->session->unset_userdata('isFirst');
        
        
        $data['moduleType'] = $module_type;
        $data['tutorInfo'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $tutor_id);
        
        //If not match with today date
        //$this->delete_st_error_ans(date('Y-m-d'));
        

        $data['user_info'] = $this->Student_model->userInfo($this->loggedUserId);
        if ($data['tutorInfo'][0]['user_type'] == 7) {
            //            $data['studentSubjects'] = $this->Student_model->studentSubjects($this->loggedUserId);
            $subject_with_course = $this->Student_model->studentSubjects($this->loggedUserId);
        }
        
        // if ($data['tutorInfo'][0]['user_type'] == 3) {
            // $data['studentSubjects'] = $this->Student_model->getInfo('tbl_subject', 'created_by', $tutor_id);
        // }
        
        if ($data['tutorInfo'][0]['user_type'] == 3) {
            $subject_with_course = $this->Student_model->get_tutor_subject($tutor_id);
            // $data['studentSubjects'] = array_values(array_column($students_all_subject, null, 'subject_id'));
        }
        
        $students_all_subject = array();
        
        foreach ($subject_with_course as $subject_course) {
            $set_subject = 1;
            if ($subject_course['isAllStudent'] == 0) {
                $individualStudent = json_decode($subject_course['individualStudent']);
                $individualStudent = is_null($individualStudent) ? [] : $individualStudent;
                if (sizeof($individualStudent) && in_array($this->loggedUserId, $individualStudent)) {
                    $set_subject = 1;
                } else {
                    $set_subject = 0;
                }
            }
            if ($set_subject == 1) {
                $students_all_subject[] = $subject_course;
            }
        }
        
        
        $data['studentSubjects'] = array_values(array_column($students_all_subject, null, 'subject_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('students/module/all_module_list', $data, true);

        $this->load->view('master_dashboard', $data);
    }
    
    /**
     * All module added by Q-study for student(ajax hit).
     *
     * First we picked students enrolled course and match the course with subject recorded.
     * We can choose subject and chapter to filter module search
     *
     * @return string render module items with table tag
     */
    public function studentsModuleByQStudy()
    {
        $data['student_error_ans'] = $this->Student_model->getInfo('tbl_st_error_ans', 'st_id', $this->session->userdata('user_id'));
        
        $post         = $this->input->post();
        $subjectId    = isset($post['subjectId']) ? $post['subjectId'] : '';
        $chapterId    = isset($post['chapterId']) ? $post['chapterId'] : '';
        $moduleType   = isset($post['moduleType']) ? $post['moduleType'] : '';
        //        $tutorType  = isset($post['tutorId']) ? $post['tutorId'] : '';
        $tutorId      = isset($post['tutorId']) ? $post['tutorId'] : '';
        $tutorInfo = $this->Student_model->getInfo('tbl_useraccount', 'id', $tutorId);
        
        $studentGrade = $this->Student_model->studentClass($this->loggedUserId);

        if ($subjectId == 'all') {
            $subjectId = '';
        }
        
        if (isset($tutorInfo[0]['user_type']) && $tutorInfo[0]['user_type'] == 7) {//q-study
            $conditions = array(
            'subject'              => $subjectId,
            'chapter'              => $chapterId,
            'moduleType'           => $moduleType,
            'tbl_module.user_type' => 7,
            'studentGrade'         => $studentGrade,
            );

            $conditions = array_filter($conditions);
           
            // Newly Added
            $data['all_subject_student'] =$this->Student_model->getInfo('tbl_registered_course', 'user_id', $this->session->userdata('user_id'));
            $result = array_column($data['all_subject_student'], 'course_id');
           
            $registered_course = implode(', ', $result);
            if ($subjectId == 'all' || $subjectId == '') {
                $desired_result = '';
            } else {
                $desired_result = $subjectId;
            }
        
            // $data['all_subject_qStudy'] =$this->Student_model->get_all_subject($tutorInfo[0]['user_type']);
            // $data['all_subject_student'] =$this->Student_model->get_all_subject_for_registered_student($this->session->userdata('user_id'));

            // if ($subjectId == 'all' || $subjectId == '') {
                // $first_array_q = array_column($data['all_subject_qStudy'], 'subject_id');
                // $second_array_st = array_column($data['all_subject_student'], 'subject_id');

                // $desired_result = '';
                // $result = array_intersect($first_array_q, $second_array_st);
                // if ($result) {
                    // $desired_result = implode(', ', $result);
                // }
            // } else {
                // $desired_result = $subjectId;
            // }
        
        
            $all_module = $this->Student_model->all_module_by_type($tutorInfo[0]['user_type'], $desired_result, $result, $conditions);
            // $data['maincontent'] = $this->load->view('students/qstudy_module/all_tutorial_list', $data, true);
        } else { //module created by general tutor
            $conditions = array(
            'subject'              => $subjectId,
            'chapter'              => $chapterId,
            'moduleType'           => $moduleType,
            //            'tbl_module.user_type' => $tutorType,
            'studentGrade'         => $studentGrade,
            'user_id'              => $tutorId,
            );

            $conditions = array_filter($conditions);
            $all_module = $this->ModuleModel->allModule(array_filter($conditions));
        }
    
        // $all_module = $this->ModuleModel->allModule(array_filter($conditions));
    
        $new_array  = array();
        $sct_info  = array();
    
        
        foreach ($all_module as $module) {
            if ($module['isAllStudent']) {
                $sct_info[] = $module;
            } elseif (strlen($module['individualStudent'])) {
                if ($module['individualStudent']) {
                    $stIds = json_decode($module['individualStudent']);

                    if (in_array($this->loggedUserId, $stIds)) {
                        $sct_info[] = $module;
                    }
                }
            }
        }
    
        if ($moduleType == 2) {
            foreach ($sct_info as $idx => $module) {
                $get_student_ans_by_module = $this->Student_model->student_module_ans_info($this->session->userdata('user_id'), $module['id']);
            
                if ($this->site_user_data['student_grade'] != $module['studentGrade']) {
                    unset($sct_info[$idx]);
                } elseif ($module['repetition_days']) {
                    $repition_days = json_decode($module['repetition_days']);
                
                    $b = array_map(array($this, 'get_repitition_days'), $repition_days);//array_map("fix1", $repition_days);
                    
                    date_default_timezone_set($this->site_user_data['zone_name']);
                    $today = date('Y-m-d');
                    
                    // If Date match with repeated date And module ans is available for this student
                    if (in_array($today, $b) && $get_student_ans_by_module) {
                        $get_answer_repeated_module = $this->Student_model->get_answer_repeated_module($this->session->userdata('user_id'), $module['id'], $today);
                        $st_ans = json_decode($get_student_ans_by_module[0]['st_ans'], true);
                        
                        // If no ans is available for wrong and data is found in tbl_answer_repeated_module for this user id, module id and today date
                        if (!in_array('wrong', array_column($st_ans, 'ans_is_right')) || $get_answer_repeated_module) { // search value in the array
                            unset($sct_info[$idx]);
                        } else { // If wrong ans is available
                            $this->insert_error_question('', $st_ans);
                            $sct_info[$idx]['is_repeated'] = 1;
                        }
                    }
                    
                    // If today not match with repeated date But module ans is available for this student
                    elseif ($get_student_ans_by_module) {
                        unset($sct_info[$idx]);
                    }
                } elseif (($module['repetition_days'] == '' && $get_student_ans_by_module)) {
                    unset($sct_info[$idx]);
                }
            }
            
            // Keep array with same index to match for all type of module
            foreach ($sct_info as $module) {
                $new_array[] = $module;
            }
            
            $this->show_all_module($new_array);
        } else {
            $this->show_all_module($all_module);
        }
    }
    
    function get_repitition_days($n)
    {
        if ($n) {
            $val = (explode('_', $n));
            return $val[1];
        }
    }
    
    
    public function show_all_module($allModule)
    {
        date_default_timezone_set($this->site_user_data['zone_name']);
        $now_time = date('Y-m-d H:i:s');
        
        // echo $allModule[0]['exam_end'].'<pre>';
        // echo strtotime($now_time);die;
        
        $row = '';
        if ($allModule) {
            $row .= '<input type="hidden" id="first_module_id" value="'.$allModule[0]['id'].'">';
            
            foreach ($allModule as $module) {
                if ($module['moduleType'] != 3 || ($module['moduleType'] == 3 && strtotime($now_time) < strtotime($module['exam_end']))) {
                    $is_repeated = '';
                    if (isset($module['is_repeated']) && $module['is_repeated'] == 1) {
                        $is_repeated = '(Repeated Module)';
                    }
                    
                    $video_link = json_decode($module['video_link']);
                    $link = 'get_tutor_tutorial_module/'.$module['id'].'/1';
                    /*if ($video_link) {
                        $link = 'video_link/'.$module['id'].'/'.$module['moduleType'];
                    }*/
                    
                    $row .= '<tr>';
                    //$row .= '<td><a onclick="get_permission('.$module['id'].')" href="' . $link .'">' . $module['moduleName'] . '</a></td>';
                    $row .= '<td><a onclick="get_permission('.$module['id'].')" href="javascript:;">' . $module['moduleName'] . '</a></td>';
                    //$row .= '<td style="cursor:pointer;"><a onclick="get_permission('.$module['id'].')">' . $module['moduleName'] . $is_repeated . '</a></td>';
                    // $row .= '<td>'.$module['creatorName'].'</td>';
                    $row .= '<td>' . $module['trackerName'] . '</td>';
                    $row .= '<td>' . $module['individualName'] . '</td>';
                    $row .= '<td>' . $module['subject_name'] . '</td>';
                    $row .= '<td>' . $module['chapterName'] . '</td>';
                    $row .= '</tr>';
                }
            }
        }
        echo strlen($row)?$row:'no module found';
    }
    

    public function module_details($module_id)
    {
        echo $module_id;
    }

    public function get_draw_image()
    {
        $this->load->library('image_lib');
        $img = $_POST['imageData'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $path = 'assets/uploads/draw_images/';
        $draw_file_name = 'draw' . uniqid();
        $file = $path . $draw_file_name . '.png';
        file_put_contents($file, $data);

        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['maintain_ratio'] = true;
        $config['width'] = 400;
        $config['height'] = 250;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();

        echo base_url().$file;
    }
    
    public function show_tutorial_result($module)
    {
        $user_id = $this->session->userdata('user_id');
        $data['module_info'] = $this->tutor_model->getInfo('tbl_module', 'id', $module);
        $data['obtained_marks'] = $this->Student_model->get_student_progress($user_id, $module);
        
        $get_dialogue = $this->Student_model->get_today_dialogue(date('m/d/Y'));
        if (!$get_dialogue) {
            $get_dialogue = $this->Student_model->get_whole_year_dialogue(date('Y'));
        }
        
        $data['dialogue'] = $get_dialogue;
        
        // echo date('m/d/Y');
        // echo '<pre>';print_r($data['dialogue']);die;
        
        $tutorial_ans_info = array();
        if ($data['module_info']) {
            if ($data['module_info'][0]['moduleType'] == 1) {
                $get_tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $module, $user_id);
                $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'], true);
                
                $data['obtained_marks'] = $this->session->userdata('obtained_marks');
                $data['total_marks'] = $this->session->userdata('total_marks');
            } elseif ($data['module_info'][0]['moduleType'] == 2) {
                $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_st_error_ans', $module, $user_id);
                //            $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'],TRUE);
                $module_id = $module;
            } else {
                $get_tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_student_answer', $module, $user_id);
                $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'], true);
            }
            
            // if($tutorial_ans_info) {
            $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $user_id);
            
            $data['tutorial_ans_info'] = $tutorial_ans_info;
            $data['module_info'] = $this->tutor_model->getInfo('tbl_module', 'id', $module);
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = '';
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('students/show_module_result', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            redirect('error');
        }
    }

    /**
     * All chapters of a subject(ajax hit)
     *
     * @param  integer $subjectId subject id
     * @return string             rendered chapter items
     */
    public function renderedChapters($subjectId)
    {
        $chapters = $this->Student_model->chaptersOfSubject($subjectId);
        $row ='<option value="">Select Chapter</option>';
        foreach ($chapters as $chapter) {
            $row .= '<option value="'.$chapter['id'].'">'.$chapter['chapterName'].'</option>';
        }
        echo $row;
    }
    
    public function get_permission()
    {
        $module_id = $this->input->post('module_id');
        $get_student_ans_by_module = $this->Student_model->student_module_ans_info($this->session->userdata('user_id'), $module_id);
        $get_student_error_ans_info = $this->Student_model->student_error_ans_info($this->session->userdata('user_id'), $module_id);
        $module = $this->Student_model->getInfo('tbl_module', 'id', $module_id);
        
        $link = '';
        $b = [];
        
        
        // First check module's repitition availability
        // IF match with repeated date and data found in student ans table
        // Do insert on st_error_ans
        
        if ($module[0]['repetition_days']) {
            $repition_days = strlen($module[0]['repetition_days']) ? json_decode($module[0]['repetition_days']) : [1,2,3];
            
            function fix($n)
            {
                if ($n) {
                    $val = (explode('_', $n));
                    return $val[1];
                }
            }
            
            $b = array_map("fix", $repition_days);
            $b = count($b) ? $b : [];
            
            date_default_timezone_set($this->site_user_data['zone_name']);
            $today = date('Y-m-d');
            
            if (in_array($today, $b) && $get_student_ans_by_module) {
                $st_ans = json_decode($get_student_ans_by_module[0]['st_ans'], true);
                if ($st_ans) {
                    $this->insert_error_question($get_student_error_ans_info, $st_ans);
                    
                    foreach ($st_ans as $row) {
                        if ($row['ans_is_right'] == 'wrong') {
                            $link = 'get_tutor_tutorial_module/'. $module_id . '/' . $row['question_order_id'];
                            break;
                        }
                    }
                }
            }
            
            if (!$get_student_ans_by_module) {
                $link = 'get_tutor_tutorial_module/'. $module_id . '/1';
            }
        } else {
            $video_link = json_decode($module[0]['video_link']);
            $link = 'get_tutor_tutorial_module/' . $module[0]['id'] . '/1';
            if ($video_link) {
                $link = 'video_link/' . $module[0]['id'] . '/' . $module[0]['moduleType'];
            }
        }
        
        echo $link;
    }
    
    public function insert_error_question($get_student_error_ans_info, $st_ans)
    {
        foreach ($st_ans as $row) {
            // Insert only when 'tbl_st_error_ans' is empty for this student and for this module and if the worng answer is available
            if ($row['ans_is_right'] == 'wrong') {
                $data_err['st_id'] = $this->session->userdata('user_id');
                $data_err['question_id'] = $row['question_id'];
                $data_err['question_order_id'] = $row['question_order_id'];
                $data_err['module_id'] = $row['module_id'];
                $data_err['error_count'] = 1;
                
                $get_specific_error_data = $this->Student_model->get_count_std_error_ans($row['question_order_id'], $row['module_id'], $this->session->userdata('user_id'));
                
                if (!$get_specific_error_data) {
                    $this->db->insert('tbl_st_error_ans', $data_err);
                }
            }
        }
    }
    
    
    public function finish_all_module_question($module_id)
    {
        $user_id = $this->session->userdata('user_id');
        $module = $this->Student_model->getInfo('tbl_module', 'id', $module_id);
        
        $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $module_id, $user_id);
        if ($tutorial_ans_info) {
            $this->Student_model->deleteInfo('tbl_temp_tutorial_mod_ques', 'id', $tutorial_ans_info[0]['id']);
        }
        
        $b = array();
        date_default_timezone_set($this->site_user_data['zone_name']);
        $today = date('Y-m-d');
        
        //        First check in module has repitition days
        //        Second check if available then check is repitition days match with today

        if ($module[0]['repetition_days']) {
            $repition_days = strlen($module[0]['repetition_days']) ? json_decode($module[0]['repetition_days']) : [1, 2, 3];

            function fix($n)
            {
                if ($n) {
                    $val = (explode('_', $n));
                    return $val[1];
                }
            }

            $b = array_map("fix", $repition_days);
            $b = count($b) ? $b : [];
        }
        
        //        if today is not available in repitition days then delete
        //        Delete tbl_st_error_ans data for Everyday Study

        if (!in_array($today, $b)) {
            $student_error_ans_info = $this->Student_model->student_error_ans_info($user_id, $module_id);
            if ($student_error_ans_info) {
                $this->Student_model->delete_all_st_error_ans($module_id, $user_id);
            }
        }
        
        if (in_array($today, $b)) {
            $data['std_id'] = $user_id;
            $data['repeat_module_id'] = $module_id;
            $data['answered_date'] = $today;
            
            $this->db->insert('tbl_answer_repeated_module', $data);
        }
        
        redirect('all_tutors_by_type/'.$module[0]['user_id'].'/'.$module[0]['moduleType']);
    }

    /*public function test()
    {
        $this->load->library('email');
        $config['protocol']    = 'smtp';
        $config['smtp_crypto']    = 'ssl';
        $config['smtp_port']    = '465';
        $config['mailtype']    = 'text';
        $config['smtp_host']    = 'email-smtp.us-east-1.amazonaws.com';
        $config['smtp_user']    = 'AKIAJASMGQXCHUGFOX2A';
        $config['smtp_pass']    = 'AhQPyL02MEAjbohY82vZLikIwY1O2sU4sOrdI6vC3HYk';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
         $this->email->initialize($config);

        $this->email->from('admin@q-study.com', 'myname');
        $this->email->to('shakil147258@gmail.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        $this->email->send();

        echo $this->email->print_debugger();

        //mail('shakil147258@gmail.com', 'hit', 'hit');
    }*/
}
