<?php

class Faq extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;
        
        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        if ($user_type != 0) { //Only admin
            redirect('welcome');
        }

        $this->load->model('FaqModel');
    }

    public function addFaq()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$clean) {
            $data['maxSL'] = $this->FaqModel->lastItemId(); //last item id
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('faqs/add_faq', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            /*update all serial below requested serial by +1*/
            $this->FaqModel->reorderSerial($clean['serial_num']);

            /*insert requested faq after serial update*/
            $dataToInsert = [
                'serial'       => $clean['serial_num'],
                'title'        => $clean['title'],
                'show_in_home' => isset($clean['show_in_home'])?$clean['show_in_home']:1,
                'item_type'    => strtolower(str_replace(' ', '_', $clean['title'])),

            ];
            
            $insID = $this->FaqModel->insert($dataToInsert);
            if ($insID) {
                $this->session->set_flashdata('success_msg', 'FAQ added successfully');
            } else {
                $this->session->set_flashdata('error_msg', 'FAQ added successfully');
            }

            redirect('faq/all');
        }
    }

    public function allFaq()
    {

        $data['allFaqs'] = $this->FaqModel->allFaqs();

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('faqs/all_faq', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function editFaq($faqId)
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        //$this->form->validation
        if (!$clean) {
            $data['maxSL'] = $this->FaqModel->lastItemId();
            $data['faq'] = $this->FaqModel->info(['id'=>$faqId]);
            
            if (!count($data['faq'])) {
                show_404();
            }

            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
            $data['maincontent'] = $this->load->view('faqs/edit_faq', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToUpdate = [
                'title' =>$clean['faq_title'],
                'body' =>$post['faq_body'],
                'show_in_home' =>isset($clean['show_in_home']) ? 1 : 0,
                'item_type'    => strtolower(str_replace(' ', '_', $clean['faq_title'])),
            ];
            
            $conditions = ['id'=>$faqId];
            $this->FaqModel->update($conditions, $dataToUpdate);
            $this->session->set_flashdata('success_msg', 'FAQ updated successfully');
            redirect("faq/edit/".$faqId);
        }
    }

    
    /**
     * Delete a faq item
     * @param integer $faqId faq id
     *
     * @return string
     */
    public function deleteFaq($faqId)
    {
        $this->FaqModel->delete($faqId);
        echo 'true';
    }

    /**
     * Pricing,How it works, About us, Disclaimer,... etc fields
     *
     * @return void
     */
    public function addLandPageItems()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$post) {
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['leftnav'] = $this->load->view('faqs/frontPageItems/addOtherItemsLeftNav', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('faqs/frontPageItems/addItems', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToInsert = [
                'title'=>$clean['title'],
                'body'=>$post['body'],
                'item_type' => $clean['itemType'],
                'show_in_home' => 0,
            ];
            
            //video items
            if ($clean['itemType']=='how_it_works ') {
                $dataToInsert['body'] = strip_tags($clean['body']);
            }
            
            $itemExists = $this->FaqModel->info(['item_type'=>$clean['itemType']]);
            if (count($itemExists)) {
                $cond = [ 'item_type'=>$clean['itemType'] ];
                $this->FaqModel->update($cond, $dataToInsert);
            } else {
                $this->FaqModel->insert($dataToInsert);
            }
            $this->session->set_flashdata('success_msg', 'Item recorded successfully!');
            redirect('faq/add/other');
        }
    }

    /**
     * Ajax call to get front page item info(while admin add item).
     *
     * @return string item info
     */
    public function getItemInfo()
    {
        $post = $this->input->post();
        $itemExists = $this->FaqModel->info(['item_type'=>$post['itemType']]);

        if (count($itemExists)) {
            $arr = [
                'title'=> $itemExists['title'],
                'body'=> $itemExists['body'],
            ];
            echo json_encode($arr);
        } else {
            echo 'false';
        }
    }
    
    public function landPagevideoUpload()
    {
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('faqs/video_help/video_upload', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function landPagevideoList()
    {
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['leftnav'] = $this->load->view('dashboard_template/leftnav', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('faqs/video_help/video_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }
}
