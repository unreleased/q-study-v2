<?php
/**
 * Question controller class.
 *
 * @author Shakil Ahmed (initial part author)
 */

class Question extends CI_Controller
{

    public $loggedUserId, $loggedUserType;


    public function __construct()
    {
        parent::__construct();

        $user_id              = $this->session->userdata('user_id');
        $user_type            = $this->session->userdata('userType');
        $this->loggedUserId   = $user_id;
        $this->loggedUserType = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        $this->load->model('QuestionModel');
    }//end __construct()


    /**
     * Delete a question from database.
     *
     * @param integer $questionId question to delete
     *
     * @return void
     */
    public function deleteQuestion($questionId = 0)
    {
        
        /*delete question info from tbl_question
        delete all question module relationship*/
        $delItems = $this->QuestionModel->delete('tbl_question', 'id', $questionId);
        $this->QuestionModel->delete('tbl_modulequestion', 'question_id', $questionId);
        if ($delItems) {
            echo 'true';
        } else {
            echo 'false';
        }
    }//end deleteQuestion()


    /**
     * This method will simply duplicate a question
     *
     * @param integer $questionId question to duplicate
     *
     * @return void
     */
    public function duplicateQuestion($questionId = 0)
    {
        $parentQuestion = $this->QuestionModel->info($questionId);
        unset($parentQuestion['id']);
        if (count($parentQuestion)) {
            $parentQuestion['country'] = $parentQuestion['country'] ? $parentQuestion['country']: 1;
            $this->QuestionModel->insert('tbl_question', $parentQuestion);
            echo 'true';
        } else {
            echo 'false';
        }
    }//end duplicateQuestion()

    /**
     * User can duplicate a dictionary item to his/her section
     *
     * @param integer $questionId dictionary item to duplicate
     *
     * @return void
     */
    public function duplicateDictionaryItem($questionId = 0)
    {
        $parentQuestion = $this->QuestionModel->info($questionId);
        unset($parentQuestion['id']);
        $parentQuestion['user_id'] = $this->loggedUserId;
        if (count($parentQuestion)) {
            $this->QuestionModel->insert('tbl_question', $parentQuestion);
            echo 'true';
        } else {
            echo 'false';
        }
    }//end duplicateQuestion()


    /**
     * Add dictionary word item(view)
     *
     * @return void
     */
    public function addDictionaryWord()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $question_box = 'tutors/question/question-box/q-dictionary';
        $data['question_box']=$this->load->view($question_box, '', true);
        $data['maincontent'] = $this->load->view($question_box, $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Save dictionary word item
     *
     * @return void
     */
    public function saveDictionaryWord()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);

        $data['studentgrade'] = 1;//$this->input->post('studentgrade');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['dictionary_item'] = 1;
        $data['questionType'] = 3; //vocabulary type
        $data['chapter'] = 1;//$this->input->post('chapter');
        $data['questionName'] = $this->processDictionaryWord($post);//$this->input->post('questionName');
        $data['answer'] = $clean['answer'];
        $data['questionMarks'] = 1;//$questionMarks;
        $data['questionDescription'] = '1';//$this->input->post('questionDescription');
        $data['isCalculator'] = 1;//$this->input->post('isCalculator');
        $data['question_solution'] = 1;//$this->input->post('question_solution');
        $data['created'] = time();
        $hour = 1;//$this->input->post('hour');
        $minute = 1;//$this->input->post('minute');
        $second = 1;//$this->input->post('second');

        $data['questionTime'] = $hour.":".$minute.":".$second;
        $questionId = $this->tutor_model->insertId('tbl_question', $data);
        //redirect('q-dictionary/view/'.$data['answer']);
        $this->session->set_flashdata('success_msg', 'Dictionary item created successfully');
        redirect('question-list');
    }

    /**
     * Grab item from post method for question field
     *
     * @param array $items post array
     *
     * @return string json encoded data to record as question
     */
    public function processDictionaryWord($items)
    {
        $arr['itemType'] ="dictionary"; //
        $arr['definition'] = $items['definition'];
        $arr['parts_of_speech'] = $items['parts_of_speech'];
        $arr['synonym'] = $items['synonym'];
        $arr['antonym'] = $items['antonym'];
        $arr['sentence'] = $items['sentence'];
        $arr['near_antonym'] = $items['near_antonym'];
        $arr['speech_to_text'] = $items['speech_to_text'];

        //$arr['vocubulary_image'] = $items['vocubulary_image'];
        for ($i = 1; $i <= $items['image_quantity']; $i++) {
            //$image = 'vocubulary_image_' . $i . '[]';
            $desired_image[] = $items['vocubulary_image_'.$i];
        }
        if ($desired_image) {
            $arr['vocubulary_image'] = $desired_image;
        }
        
        $files = $_FILES;
        if (isset($_FILES['videoFile']) && $_FILES['videoFile']['error'][0] != 4) {
            $_FILES['videoFile']['name'] = $files['videoFile']['name'];
            $_FILES['videoFile']['type'] = $files['videoFile']['type'];
            $_FILES['videoFile']['tmp_name'] = $files['videoFile']['tmp_name'];
            $_FILES['videoFile']['error'] = $files['videoFile']['error'];
            $_FILES['videoFile']['size'] = $files['videoFile']['size'];
            
            $_FILES['audioFile']['name'] = $files['audioFile']['name'];
            $_FILES['audioFile']['type'] = $files['audioFile']['type'];
            $_FILES['audioFile']['tmp_name'] = $files['audioFile']['tmp_name'];
            $_FILES['audioFile']['error'] = $files['audioFile']['error'];
            $_FILES['audioFile']['size'] = $files['audioFile']['size'];

            $config['upload_path'] = 'assets/uploads/question_media/';
            $config['allowed_types'] = 'mp3|mp4|3gp|ogg|wmv';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload();

            $error = array();
            if (!$this->upload->do_upload('videoFile')) {
                $error = $this->upload->display_errors();
            } else {
                $fdata = $this->upload->data();
                $arr['videoFile'] = $config['upload_path'] . $fdata['file_name'];
            }
            
            if (!$this->upload->do_upload('audioFile')) {
                $error = $this->upload->display_errors();
            } else {
                $fdata1 = $this->upload->data();
                $arr['audioFile'] = $config['upload_path'] . $fdata1['file_name'];
            }
        }
        return json_encode($arr);
    }
}//end class
