<?php

class Preview extends CI_Controller
{

    public $loggedUserId;

    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        if ($user_type != 3 && $user_type != 7) {
            redirect('welcome');
        }
        
        $this->load->model('Preview_model');
        $this->load->model('tutor_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
        
        $this->loggedUserId = $this->session->userdata('user_id');
        
        $user_info = $this->Preview_model->userInfo($user_id);

        if ($user_info[0]['countryCode'] == 'any') {
            $user_info[0]['zone_name'] = 'Australia/Lord_Howe';
        }
        
        $this->site_user_data = array(
            'userType' => $user_type,
            'zone_name' => $user_info[0]['zone_name'],
            'student_grade' => $user_info[0]['student_grade'],
        );
    }

    public function question_preview($question_item, $question_id)
    {
    
        date_default_timezone_set($this->site_user_data['zone_name']);
        $exact_time = time();
        $this->session->set_userdata('exact_time', $exact_time);
    
        if ($question_item == 1) {
            $this->general($question_item, $question_id);
        } elseif ($question_item == 2) {
            $this->true_false($question_item, $question_id);
        } elseif ($question_item == 3) {
            $this->preview_vocubulary($question_item, $question_id);
        } elseif ($question_item==4) {
            $this->preview_multiple_choice($question_item, $question_id);
        } elseif ($question_item==5) {
            $this->preview_multiple_response($question_item, $question_id);
        } elseif ($question_item == 6) {
            $this->preview_skip($question_item, $question_id);
        } elseif ($question_item == 7) {
            $this->preview_matching($question_item, $question_id);
        } elseif ($question_item == 8) {
            $this->preview_skip($question_item, $question_id);
        } elseif ($question_item == 9) {
            $this->preview_creative_quiz($question_item, $question_id);
        } elseif ($question_item == 10) {
            $this->preview_times_table($question_item, $question_id);
        } elseif ($question_item == 11) {
            $this->preview_algorithm($question_item, $question_id);
        } elseif ($question_item == 12) {
            $this->preview_workout_quiz($question_item, $question_id);
        } elseif ($question_item == 13) {
            $this->preview_multiple_choice($question_item, $question_id);
        }
    }

    private function general($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/question_image', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function true_false($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('preview/true_false', $data, true);
        
        $this->load->view('master_dashboard', $data);
    }

    private function preview_vocubulary($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_vocubulary', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_multiple_choice($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_multiple_choice', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_multiple_response($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_multiple_response', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_matching($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_total'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_left_right'] = json_decode($data['question_info_total'][0]['questionName']);

        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('preview/preview_matching', $data, true);
        
        $this->load->view('master_dashboard', $data);
    }

    /**
     * preview question type skip quiz
     * @param  int $questionId questionId
     * @return void
     */
    public function preview_skip($question_item, $questionId)
    {
        
        $quesInfo     = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $data['question_info_s']   = $quesInfo;
        $questionType = $quesInfo[0]['questionType'];
        $quesInfo     = json_decode($quesInfo[0]['questionName']);
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        // common view file
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['question_id'] = $questionId;
        $data['question_item'] = $question_item;

        if ($questionType == 8) {
            // Assignment
            $questionBody            = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionBody']    = $questionBody;
            $items                   = $quesInfo->assignment_tasks;
            $data['totalItems']      = count($items);
            $data['assignment_list'] = $this->renderAssignmentTasks($items);
            $data['maincontent']     = $this->load->view('preview/assignment', $data, true);
        } elseif ($questionType == 6) {
            // skip quiz
            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionId']   = $questionId;
            
            $quesAnsItem          = $quesInfo->skp_quiz_box;

            $items = $this->indexQuesAns($quesAnsItem);

            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);

            $user_id             = $this->session->userdata('user_id');
            $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
            $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
            $data['maincontent'] = $this->load->view('preview/skip_quiz', $data, true);
        }//end if
        
        $this->load->view('master_dashboard', $data);
    }
    
    public function preview_creative_quiz($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
//        echo '<pre>';print_r($data);die;
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_creative_quiz', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function preview_times_table($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_times_table', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function preview_algorithm($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_algorithm', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function preview_workout_quiz($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/workout_quiz', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function answer_matching()
    {
        $question_id = $this->input->post('question_id');
        $module_id = $this->input->post('module_id');
        $question_order_id = $this->input->post('current_order');
        $text = $this->input->post('answer');
        
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text = strip_tags($text);
        $text = str_replace($find, $repleace, $text);
        $text = trim($text);

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $question_marks = $answer_info[0]['questionMarks'];
        
        $text_1 = $answer_info[0]['answer'];
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text_1 = strip_tags($text_1);
        $text_1 = str_replace($find, $repleace, $text_1);
        $text_1 = trim($text_1);
        
        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1);
    }

    public function answer_matching_true_false()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $text = $this->input->post('answer');
            $question_id = $this->input->post('question_id');

            $module_id = $_POST['module_id'];
            // $question_order_id = $_POST['next_question'] - 1;
            $question_order_id = $_POST['current_order'];
            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
            $text_1 = $answer_info[0]['answer'];
            $question_marks = $answer_info[0]['questionMarks'];
            
            $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1);
        }
    }

    public function answer_matching_vocabolary()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $text = strtolower($this->input->post('answer'));
            $question_id = $this->input->post('question_id');
            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

            $module_id = $_POST['module_id'];
            // $question_order_id = $_POST['next_question'] - 1;
            $question_order_id = $_POST['current_order'];
            $text_1 = strtolower($answer_info[0]['answer']);

            $question_marks = $answer_info[0]['questionMarks'];

            $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1);
        }
    }
    
    public function answer_matching_multiple_choice()
    {
        $question_id = $_POST['question_id'];
        $text_1 = $_POST['answer_reply'];

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $text = $answer_info[0]['answer'];
        $question_marks = $answer_info[0]['questionMarks'];
        $module_id = $_POST['module_id'];
        
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        
        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1);
    }
    
    public function answer_matching_multiple_response()
    {
        $question_id = $_POST['question_id'];
        $text_1 = $_POST['answer_reply'];

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        //$text = $answer_info[0]['answer'];
        $question_marks = $answer_info[0]['questionMarks'];
        $text = json_decode($answer_info[0]['answer']);
        $result_count = count(array_intersect($text_1, $text));

        $module_id = $_POST['module_id'];
        // $question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];

        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, count($text), $result_count);
    }
    
    public function st_answer_skip()
    {
        $module_id = $_POST['module_id'];
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $post = $this->input->post();
        $questionId = $this->input->post('question_id');
        $givenAns = $this->indexQuesAns($post['given_ans']);

        $temp = $this->tutor_model->getInfo('tbl_question', 'id', $questionId);
        $question_marks = $temp[0]['questionMarks'];
        $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

        $temp2 = json_decode($temp[0]['questionName']);
        $numOfRows = $temp2->numOfRows;
        $numOfCols = $temp2->numOfCols;
        //echo $numOfRows .' ' . $numOfCols;
        $wrongAnsIndices = [];

        $text = 0;
        $text_1 = 0;
        for ($row = 1; $row <= $numOfRows; $row++) {
            for ($col = 1; $col <= $numOfCols; $col++) {
                if (isset($savedAns[$row][$col])) {
                    $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row . '_' . $col : null;
                }
            }
        }

        $wrongAnsIndices = array_filter($wrongAnsIndices);
        if (count($wrongAnsIndices)) {//For False Condition
            $text_1 = 1;
        }

        $this->take_decesion($question_marks, $questionId, $module_id, $question_order_id, $text, $text_1);
    }
    
    public function answer_multiple_matching()
    {
        $total = $_POST['total_ans'];

        $question_id = $_POST['question_id'];
        $st_ans = array();
        
        for ($i = 1; $i <= $total; $i++) {
            $ans_id = 'answer_' . $i;
            $st_ans[] = $_POST[$ans_id];
        }
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];
        $answer_info['student_ans'] = $st_ans;

        $module_id = $_POST['module_id'];
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $text = 0;
        $text_1 = 0;
        $flag = 1;
        for ($k = 0; $k < sizeof($answer_info['student_ans']); $k++) {
            if ($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
                $flag = 0;
            }
        }
        $answer_info['student_ans'] = $st_ans;
        $answer_info['flag'] = $flag;

        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, json_encode($answer_info));
    }
    
    public function answer_creative_quiz()
    {
        $question_id = $this->input->post('question_id');
        $student_ans = $this->input->post('student_ans');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];

        $module_id = $this->input->post('module_id');
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $this->input->post('current_order');
        $text = 0;
        $text_1 = 0;
        $flag = 1;
        
//        echo '<pre>';print_r($student_ans);
//        echo '<pre>';print_r($answer_info);die;
        
        for ($k = 0; $k < sizeof($student_ans); $k++) {
            if ($student_ans[$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
                $flag = 0;
            }
        }
        
        $answer_info['student_ans'] = $student_ans;
        $answer_info['flag'] = $flag;

        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, array());
    }
    
    public function answer_workout_quiz()
    {
        $question_id = $this->input->post('question_id');
        $module_id = $this->input->post('module_id');
       //$question_order_id = $_POST['check_order_id'] - 1;
        $question_order_id = $this->input->post('current_order');
        //$text = $this->input->post('answer');
        $text = 0;
        $text_1 = 0;

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $ans_is_right = 'correct';

        $question_marks = 0;

        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, array());
    }
    
    public function answer_times_table()
    {

        $question_id = $this->input->post('question_id');
        $result = $this->input->post('result');
        $st_ans = array();
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $question_marks = $answer[0]['questionMarks'];
        $answer_info['student_ans'] = $result;
        
        $module_id = $_POST['module_id'];
        //$question_order_id = $_POST['next_question'] - 1;
        $question_order_id = $_POST['current_order'];
        $text = 0;
        $text_1 = 0;
        $flag = 1;
        
        for ($k = 0; $k < sizeof($answer_info['student_ans']); $k++) {
            if ($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k]) {
                $text++;
                $flag = 0;
            }
        }
        
        $answer_info['student_ans'] = $result;
        $answer_info['flag'] = $flag;

//       $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, json_encode($answer_info));
        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, array());
    }
    
    public function answer_algorithm()
    {
        $question_id = $this->input->post('question_id');
        $result = $this->input->post('answer');
        $text = 1;
        $module_id = $this->input->post('module_id');
        
        $question_order_id = $this->input->post('current_order');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $question_info = json_decode($answer[0]['questionName'], true);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        $text_1 = 1;
        
//        echo '<pre>';print_r($question_info['operator']);die;
        
        $question_marks = $answer[0]['questionMarks'];
        if ($question_info['operator'] != '/' && $result == $answer_info['tutor_ans']) {
            $text_1 = 1;
        } elseif ($question_info['operator'] == '/' && $result[1] == $answer_info['tutor_ans']) {
            $text_1 = 1;
        } else {
            $text_1++;
        }
        
        $this->take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, array());
    }
    
    public function take_decesion($question_marks, $question_id, $module_id, $question_order_id, $text, $text_1, $answer_info = null)
    {
        //****** Get Temp table data for Tutorial Module Type ******
        $user_id = $this->session->userdata('user_id');
//        $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $module_id, $user_id);
        
        $obtained_marks = $this->session->userdata('obtained_marks');
        $total_marks = $this->session->userdata('total_marks');
        $ans_array = $this->session->userdata('data');

        $flag = 0;
        if (!is_array($ans_array)) {
            $ans_array = array();
            $obtained_marks = 0;
            $total_marks = 0;
            $flag = 0;
        } else {
            $question_idd = '';
            if (isset($ans_array[$question_order_id]['question_id'])) {
                $question_idd = $ans_array[$question_order_id]['question_id'];
            }

            if ($question_id == $question_idd) {
                $flag = 1;
            } else {
                $flag = 0;
            }
        }
//        if ($tutorial_ans_info) {
//            $temp_table_ans_info = json_decode($tutorial_ans_info[0]['st_ans'], true);
//            $flag = 2;
//        }

        if ($text == $text_1) {
            $ans_is_right = 'correct';
            if ($answer_info != null) {
                $student_ans = $answer_info;
                echo $answer_info;
            } else {
//                    if ($flag != 2) {
                   echo 2;
//                    }
            }
            //echo $text;die;
            // if ($_POST['next_question'] == 0) {
                // if ($_POST['module_type'] == 1 || $_POST['module_type'] == 2) {
                    // echo 2;
                // } else {
                    // echo 6;
                // }
            // } else {
                // if ($answer_info != null) {
                    // echo $answer_info;
                // } else {
                    // if ($_POST['module_type'] == 1 || $_POST['module_type'] == 2) {
                        // echo 2;
                    // } else {
                        // echo 5;
                    // }
                // }
            // }
        } else {
            $ans_is_right = 'wrong';
            if ($answer_info != null) {
                $student_ans = $answer_info;
                echo $answer_info;
            } else {
//                if ($flag != 2) {
                   echo 3;
//                }
            }
            $question_marks = 0;
            // if ($_POST['next_question'] == 0) {
                // if ($_POST['module_type'] == 1 || $_POST['module_type'] == 2) {
                    // echo 3;
                // } else {
                    // echo 6;
                // }
            // } else {
                // if ($answer_info != null) {
                    // echo $answer_info;
                // } else {
                    // if ($_POST['module_type'] == 1 || $_POST['module_type'] == 2) {
                        // echo 3;
                    // } else {
                        // echo 5;
                    // }
                // }
                // $question_marks = 0;
            // }
        }

        if ($flag == 0) {
            $question_info_ai = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);

            $link1 = base_url();
            $total_marks = $total_marks + $question_marks;
            $obtained_marks = $obtained_marks + $question_marks;

            $link2 = $link1 . 'get_tutor_tutorial_module/' . $module_id . '/' . $question_order_id;

            $ind_ans = array(
                'question_order_id' => $question_info_ai[0]['question_order'],
                'module_type' => $question_info_ai[0]['moduleType'],
                'module_id' => $question_info_ai[0]['module_id'],
                'question_id' => $question_info_ai[0]['question_id'],
                'link' => $link2,
                'ans_is_right' => $ans_is_right
                );
            $ans_array[$question_order_id] = $ind_ans;

            $this->session->set_userdata('data', $ans_array);
            $this->session->set_userdata('obtained_marks', $obtained_marks);
            $this->session->set_userdata('total_marks', $total_marks);
        }
        
//        if ($flag == 2) {
//            for ($i = 1; $i <= count($temp_table_ans_info); $i++) {
//                if ($temp_table_ans_info[$i]['question_order_id'] == $question_order_id) {
//                    $temp_table_ans_info[$i]['ans_is_right'] = $ans_is_right;
//                }
//            }
//            $update_value = json_encode($temp_table_ans_info);
//            $st_ans['st_ans'] = $update_value;
//            $this->Student_model->updateInfo('tbl_temp_tutorial_mod_ques', 'id', $tutorial_ans_info[0]['id'], $st_ans);
//
//            //            echo 6;
//        }
    }
    
    public function show_tutorial_result($module)
    {
        $user_id = $this->session->userdata('user_id');
        $data['module_info'] = $this->tutor_model->getInfo('tbl_module', 'id', $module);
//        $data['obtained_marks'] = $this->Student_model->get_student_progress($user_id, $module);
        $tutorial_ans_info = $this->session->userdata('data');
        $data['obtained_marks'] = $this->session->userdata('obtained_marks');
        
//        $tutorial_ans_info = array();
//        if ($data['module_info'][0]['moduleType'] == 1) {
//            $get_tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_temp_tutorial_mod_ques', $module, $user_id);
//            $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'], true);
//            $module_id = $tutorial_ans_info[1]['module_id'];
//            $data['obtained_marks'] = $this->session->userdata('obtained_marks');
//        } elseif ($data['module_info'][0]['moduleType'] == 2) {
//            $tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_st_error_ans', $module, $user_id);
//            //            $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'],TRUE);
//            $module_id = $tutorial_ans_info[0]['module_id'];
//        } else {
//            $get_tutorial_ans_info = $this->Student_model->getTutorialAnsInfo('tbl_student_answer', $module, $user_id);
//            $tutorial_ans_info = json_decode($get_tutorial_ans_info[0]['st_ans'], true);
//            $module_id = $tutorial_ans_info[1]['module_id'];
//        }
        
        // if($tutorial_ans_info) {
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $user_id);
        
        $data['tutorial_ans_info'] = $tutorial_ans_info;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('module/preview/show_module_result', $data, true);
        $this->load->view('master_dashboard', $data);
        // } else {
            // redirect('error');
        // }
    }

//    Question Edit Portion

    public function question_edit($question_item, $question_id)
    {
        if ($question_item == 1) {
            $this->edit_general($question_item, $question_id);
        } elseif ($question_item == 2) {
            $this->edit_true_false($question_item, $question_id);
        } elseif ($question_item == 3) {
            $this->edit_preview_vocubulary($question_item, $question_id);
        }
    }

    private function edit_general($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/general', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function edit_true_false($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/true-false', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function edit_preview_vocubulary($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/edit_vocubulary', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
                );
        }
        return $arr;
    }
    
    /**
     * render the indexed item to table data for preview
     * @param  array  $items   ques ans as indexed item
     * @param  int  $rows    num of row in table
     * @param  int  $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {
        //print_r($items);die;
        $row = '';
        for ($i=1; $i<=$rows; $i++) {
            $row .='<tr>';
            for ($j=1; $j<=$cols; $j++) {
                if ($items[$i][$j]['type']=='q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = array(
                        'cr'=>$i.'_'.$j,
                        'val'=> $items[$i][$j]['val'],
                        'type'=> 'a',
                        );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns==1)?' value="'.$items[$i][$j]['val'].'"' : '';
                    
                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt'.$i.'_'.$j.'"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }
        
        return $row;
    }
    
    
    
    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    public function renderAssignmentTasks(array $items)
    {
        $row = '';
        foreach ($items as $task) {
            $task = json_decode($task);
            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            //$row .= '<td>'.$task->obtnMark.'</td>';
            $row .= '<td><i class="fa fa-eye qDtlsOpenModIcon" data-toggle="modal" data-target="#quesDtlsModal"></i></td>';
            $row .= '<input type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;
    }//end renderAssignmentTasks()
}
