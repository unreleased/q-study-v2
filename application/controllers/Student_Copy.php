<?php

class Student_Copy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->loggedUserId = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->model('Admin_model');
        $this->load->model('Tutor_model');
        $this->load->model('Preview_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('upload');
        
        $user_info = $this->Preview_model->userInfo($user_id);

        if ($user_info[0]['countryCode'] == 'any') {
            $user_info[0]['zone_name'] = 'Australia/Lord_Howe';
        }

        $this->site_user_data = array(
            'userType' => $user_type,
            'zone_name' => $user_info[0]['zone_name']
        );
    }
    
    public function check_student_copy($module_id, $student_id, $question_order_id)
    {
        $data['module_info'] = $this->Student_model->getInfo('tbl_module', 'id', $module_id);
               
        if (!$data['module_info']) {
            redirect('error');
        }
        
        $data['user_info'] = $this->Tutor_model->getInfo('tbl_useraccount', 'id', $student_id);
        
        //****** Get Temp table data for Tutorial Module Type ******
        if ($data['module_info'][0]['moduleType'] != 1) {
            $table = 'tbl_student_answer';
        } else {
            $table = 'tbl_temp_tutorial_mod_ques';
        }
        
        $data['tutorial_ans_info'] = $this->Student_model->getTutorialAnsInfo($table, $module_id, $student_id);
       
        $workoutImage = isset($data['tutorial_ans_info'][0]['st_ans']) ? $data['tutorial_ans_info'][0]['st_ans'] : '';
        $workoutImage = json_decode($workoutImage);
        

        $data['question_info_s'] = $this->Tutor_model->getModuleQuestion($module_id, $question_order_id, null);
        
        if (!$data['question_info_s']) {
            $question_order_id = $question_order_id + 1;
            redirect('check_student_copy/' . $module_id . '/' . $student_id . '/' . $question_order_id);
        }

        $data['total_question'] = $this->Tutor_model->getModuleQuestion($module_id, null, 1);
        $question_box = 'tutors/check_student_copy/question_box';
        if ($data['question_info_s'][0]['question_type'] == 1) {
            $question_box .= '/general';
        }
        if ($data['question_info_s'][0]['question_type'] == 2) {
            $question_box .= '/true-false';
        }
        if ($data['question_info_s'][0]['question_type'] == 3) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/vocabulary';
        }
        if ($data['question_info_s'][0]['question_type'] == 4) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/multiple-choice';
        }
        if ($data['question_info_s'][0]['question_type'] == 5) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/multiple-response';
        }
        if ($data['question_info_s'][0]['question_type'] == 7) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['question_answer'] = json_decode($data['question_info_s'][0]['answer']);
            $question_box .= '/matching';
        }
        if ($data['question_info_s'][0]['question_type'] == 6) {
            $quesInfo1 = json_decode($data['question_info_s'][0]['questionName']);
            $student_all_ans = json_decode($data['tutorial_ans_info'][0]['st_ans'], true);
            $student_ans = json_decode($student_all_ans[$question_order_id]['student_ans']);
            $student_ans = json_decode($student_ans, true);
            
            // echo '<pre>';print_r($student_ans);die;
            
            $json_extract = new stdClass;
            foreach ($quesInfo1->skp_quiz_box as $row) {
                $json_extract = json_decode($row);
                
                if ($json_extract->type == 'a') {
                    $col_extract = explode('_', $json_extract->cr);
                    $json_extract->val = $student_ans[$col_extract[0]][$col_extract[1]]['val'];
                }

                $json_insertion[] = json_encode($json_extract);
            }
            
            // echo '<pre>';print_r($json_insertion);die;
            
            $items = $this->indexQuesAns($json_insertion);
            $data['numOfRows'] = $quesInfo1->numOfRows;
            $data['numOfCols'] = $quesInfo1->numOfCols;
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols'], $showAns = 1, 'edit');

            $data['questionBody'] = $quesInfo1->question_body;
            $question_box .= '/skip_quiz';
        }
        
        if ($data['question_info_s'][0]['question_type'] == 8) {
            $this->check_assignment_copy($data);
        }
        
        if ($data['question_info_s'][0]['questionType'] == 9) {
            $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
            $data['question_answer'] = json_decode($data['question_info_s'][0]['answer'], true);
            $data['question_description'] = json_decode($data['question_info_s'][0]['questionDescription'], true);
//            echo '<pre>';
//            print_r($data['question_info_s']);die;
            $question_box .= '/creative_quiz';
        }
        
        if ($data['question_info_s'][0]['question_type'] == 10) {
            $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
            $question_box .= '/times_table';
        }

        if ($data['question_info_s'][0]['question_type'] == 11) {
            $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
//            echo '<pre>';print_r($data['question_info']);die;
            $question_box .= '/algorithm';
        }

        if ($data['question_info_s'][0]['question_type'] == 12) {
            $data['question_info'] = json_decode($data['question_info_s'][0]['questionName'], true);
            $question_box .= '/workout_quiz';
        }
        
        if ($data['question_info_s'][0]['question_type'] != 8) {
            $data['question_box'] = $this->load->view($question_box, $data, true);

            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('tutors/check_student_copy/student_copy', $data, true);
            $this->load->view('master_dashboard', $data);
        }
    }
    
    public function check_assignment_copy($data)
    {
        $quesInfo     = json_decode($data['question_info_s'][0]['questionName']);
        $questionBody            = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
        $data['questionBody']    = $questionBody;
        $items                   = $quesInfo->assignment_tasks;
        $data['totalItems']      = count($items);
        $data['assignment_list'] = $this->renderAssignmentTasks($items);
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/check_student_copy/student_assignment_copy', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     *
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        //print_r($items);die;
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            //print_r($cr);die;
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
                );
        }
        return $arr;
    }
    
    /**
     * Render the indexed item to table data for preview
     *
     * @param array   $items   ques ans as indexed item
     * @param int     $rows    num of row in table
     * @param int     $cols    num of cols in table
     * @param integer $showAns optional, set 1 will show the answers too
     *
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {
        //print_r($items);die;
        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .='<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="' . $items[$i][$j]['val'] . '" name="skip_counting[]" class="form-control input-box  rsskpinpt' . $i . '_' . $j . '" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = array(
                        'cr' => $i . '_' . $j,
                        'val' => $items[$i][$j]['val'],
                        'type' => 'a',
                        );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns == 1) ? ' value="' . $items[$i][$j]['val'] . '"' : '';

                    $row .= '<td><input autocomplete="off" type="text" ' . $val . ' data_q_type="0" data_num_colofrow="' . $i . '_' . $j . '" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt' . $i . '_' . $j . '"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }

        return $row;
    }
    
    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    public function renderAssignmentTasks(array $items)
    {
        $row = '';
        foreach ($items as $task) {
            $task = json_decode($task);
            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            $row .= '<td><a class="qDtlsOpenModIcon" data-toggle="modal" data-target="#quesDtlsModal"><img src="assets/images/icon_details.png"></a></td>';
            $row .= '<input type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;
    }//end renderAssignmentTasks()
}
