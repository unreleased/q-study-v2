<?php

class Cron_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();        
        $this->load->model('Student_model');
		
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('upload');
		
    }


	public function get_parent_those_child_not_attend_module() {
        $get_all_time_zone = $this->Student_model->get_all_time_zone();
        foreach ($get_all_time_zone as $zone){
            $new_get_all_time_zone[$zone['id']][] = $zone;
        }
		// echo '<pre>';print_r($new_get_all_time_zone);die;
        $get_parent = array();
        foreach ($new_get_all_time_zone as $row) {
            if($row[0]['countryCode'] == 'any') {
				$row[0]['zone_name'] = 'Australia/Lord_Howe';
			}
//            foreach ($val as $row){
            if($row[0]['zone_name'] != NULL) {
                $date = new DateTime("today 9pm", new DateTimeZone($row[0]['zone_name']));
                $date1 = new DateTime("now", new DateTimeZone($row[0]['zone_name']));

                $today_9pm_time = strtotime($date->format('Y-m-d H:i:s'));
                $now_time = strtotime($date1->format('Y-m-d H:i:s'));
				echo $row[0]['id'].'<pre>';
				echo 'Today 9PM: ';echo $date->format('Y-m-d H:i:s').'<pre>';
				echo 'Now: ';echo $date1->format('Y-m-d H:i:s').'<pre>';
                if($now_time >= $today_9pm_time) {
//                    echo $row[0]['id'].'<pre>';
                    $get_parent[] = $this->Student_model->get_parent_with_children($row[0]['id'],$date->format('Y-m-d H:i:s'));
                }
            }
//            }
        }
        
        $new = array();
        foreach ($get_parent as $parent => $parent_val) {
//            echo '<pre>';
//            print_r($parent_val);
            foreach ($parent_val as $p_val){
                $new[$p_val['parentid']][] = $p_val;
            }
            
        }
		
		// echo '<pre>';
        // print_r($new);die;
        
        foreach ($new as $key => $val) {
            $country_code = $this->Student_model->country_code(strtoupper($val[0]['countryCode']));
            $date1 = new DateTime("now", new DateTimeZone($country_code[0]['zone_name']));
            
			$is_student_answered_module = $this->Student_model->get_student_today_ans($val[0]['childid'],$date1->format('Y-m-d'));
            $is_send_msg = $this->Student_model->send_msg($key,$date1->format('Y-m-d'));
			
//            print_r($is_send_msg);
            if(is_array($is_send_msg) && count($is_send_msg) == 0 && !$is_student_answered_module) {
			
				$settins_Api_key = $this->Student_model->getSmsApiKeySettings();
				$settins_sms_messsage = $this->Student_model->get_sms_response_after_9pm();
				
				$api_key = $settins_Api_key[0]['setting_value'];
				
				$register_code_string = $settins_sms_messsage[0]['setting_value'];
				$find = array("{{child_name}}");
				$replace = array($val[0]['child_name']);
				
				$message = str_replace($find, $replace, $register_code_string);
				
				$content = urlencode($message);
				
                //$content = urlencode("Today ".$val[0]['child_name']." didn't attend Everyday Study until now");
                $url = "https://platform.clickatell.com/messages/http/send?apiKey=$api_key&to=".$val[0]['parent_mobile']."&content=$content";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                curl_setopt($ch, CURLOPT_VERBOSE, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                //execute post
                $result = curl_exec($ch);
                curl_close($ch);
                
                $send_msg_status = json_decode($result);
                
                if(count($send_msg_status->messages) > 0 && $send_msg_status->messages[0]->accepted == 1) {
                    $send_msg['parent_id'] = $key;
                    $send_msg['created_at'] = $date1->format('Y-m-d H:i:s');
                    $this->Student_model->insertInfo('user_send_msg', $send_msg);
                }
				
            }
        }
        
    }
}