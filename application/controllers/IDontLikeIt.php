<?php

class IDontLikeIt extends CI_Controller
{

    public $loggedUserId;

    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        if ($user_type != 3 && $user_type != 7) {
            redirect('welcome');
        }
        $this->load->model('Preview_model');
        $this->load->model('tutor_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }

    public function question_preview($question_item, $question_id)
    {
        // print_r($question_item);die;
        if ($question_item == 1) {
            $this->general($question_item, $question_id);
        } elseif ($question_item == 2) {
            $this->true_false($question_item, $question_id);
        } elseif ($question_item == 3) {
            $this->preview_vocubulary($question_item, $question_id);
        } elseif ($question_item==4) {
            $this->preview_multiple_choice($question_item, $question_id);
        } elseif ($question_item==5) {
            $this->preview_multiple_response($question_item, $question_id);
        } elseif ($question_item == 6) {
            $this->preview_skip($question_item, $question_id);
        } elseif ($question_item == 7) {
            $this->preview_matching($question_item, $question_id);
        } elseif ($question_item == 8) {
            $this->preview_skip($question_item, $question_id);
        }
    }

    private function general($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        //echo '<pre>';print_r($data['question_info']);die;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/question_image', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function true_false($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('preview/true_false', $data, true);
        
        $this->load->view('master_dashboard', $data);
    }

    private function preview_vocubulary($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        //echo '<pre>';print_r($question_id);die;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_vocubulary', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_multiple_choice($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_multiple_choice', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_multiple_response($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info'] = json_decode($data['question_info_s'][0]['questionName']);
        
        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('preview/preview_multiple_response', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function preview_matching($question_item, $question_id)
    {
        $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        $data['question_info_total'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_s'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_left_right'] = json_decode($data['question_info_total'][0]['questionName']);

        $data['question_id'] = $question_id;
        $data['question_item'] = $question_item;
        
        //        echo '<pre>';print_r($data['question_info_left_right']);die;
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('preview/preview_matching', $data, true);
        
        $this->load->view('master_dashboard', $data);
    }

    /**
     * preview question type skip quiz
     *
     * @param  int $questionId questionId
     * @return void
     */
    public function preview_skip($question_item, $questionId)
    {

        $quesInfo     = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $data['question_info_s']   = $quesInfo;
        $questionType = $quesInfo[0]['questionType'];
        $quesInfo     = json_decode($quesInfo[0]['questionName']);
        $data['userType']  = $this->Preview_model->getInfo('tbl_usertype', 'id', $this->session->userdata('userType'));
        $data['userType'] = $data['userType'][0]['user_slug'];
        // common view file
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        // $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['header'] = '';
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['question_id'] = $questionId;
        $data['question_item'] = $question_item;

        if ($questionType == 8) {
            // Assignment
            $questionBody            = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionBody']    = $questionBody;
            $items                   = $quesInfo->assignment_tasks;
            $data['totalItems']      = count($items);
            $data['assignment_list'] = $this->renderAssignmentTasks($items);
            $data['maincontent']     = $this->load->view('preview/assignment', $data, true);
        } elseif ($questionType == 6) {
            // skip quiz
            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionId']   = $questionId;
            
            //            echo '<pre>';print_r($data['question_info_s']);die;
            $quesAnsItem          = $quesInfo->skp_quiz_box;

            $items = $this->indexQuesAns($quesAnsItem);

            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);

            $user_id             = $this->session->userdata('user_id');
            $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
            $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
            $data['maincontent'] = $this->load->view('preview/skip_quiz', $data, true);
        }//end if
        
        //        echo '<pre>';print_r($data['skp_box']);die;
        $this->load->view('master_dashboard', $data);
    }
    
    public function answer_matching()
    {
        $question_id = $_POST['id'];

        $text = $_POST['user_answer'];
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text = strip_tags($text);
        $text = str_replace($find, $repleace, $text);
        $text = trim($text);

        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);

        $text_1 = $answer_info[0]['answer'];
        $find = array('&nbsp;', '\n', '\t', '\r');
        $repleace = array('', '', '', '');
        $text_1 = strip_tags($text_1);
        $text_1 = str_replace($find, $repleace, $text_1);
        $text_1 = trim($text_1);
        if ($text == $text_1) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function answer_matching_true_false()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $answer = $this->input->post('answer');
            $question_id = $this->input->post('question_id');
            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
            $text_1 = $answer_info[0]['answer'];
            if ($answer == $text_1) {
                echo 2;
            } else {
                echo 3;
            }
        }
    }

    public function answer_matching_vocabolary()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if ($this->form_validation->run() == false) {
            echo 1;
        } else {
            $answer = strtolower($this->input->post('answer'));
            $question_id = $this->input->post('question_id');
            $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
            $text_1 = strtolower($answer_info[0]['answer']);
            if ($answer == $text_1) {
                echo 2;
            } else {
                echo 3;
            }
        }
    }
    
    public function answer_matching_multiple_choice()
    {
        $question_id = $_POST['id'];
        $answer_reply = $_POST['answer_reply'];
        
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_store = $answer_info[0]['answer'];
        
        if ($answer_store == $answer_reply) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function answer_matching_multiple_response()
    {
        $question_id = $_POST['id'];
        $answer_reply = $_POST['answer_reply'];
        
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_store = json_decode($answer_info[0]['answer']);
        
        $result_count = count(array_intersect($answer_reply, $answer_store));
        
        if ($result_count == count($answer_store) && count($answer_reply) == count($answer_store)) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function answer_creative_quiz()
    {
        $question_id = $this->input->post('question_id');
        $student_ans = $this->input->post('student_ans');
        
        $answer_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_store = json_decode($answer_info[0]['answer']);
        
        $result_count = count(array_intersect($student_ans, $answer_store));
        $flag = 1;
        //        echo $result_count;die;
        for ($k = 0; $k < sizeof($student_ans); $k++) {
            if ($student_ans[$k] != $answer_store[$k]) {
                $flag = 0;
            }
        }
        if ($flag == 1) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function answer_times_table()
    {
        $question_id = $this->input->post('question_id');
        $result = $this->input->post('result');
        
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        
        $answer_info['student_ans'] = $result;
        
        $result_count = count(array_intersect($answer_info['student_ans'], $answer_info['tutor_ans']));
        
        if ($result_count == count($answer_info['tutor_ans']) && count($answer_info['student_ans']) == count($answer_info['tutor_ans'])) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function answer_algorithm()
    {
        
        $question_id = $this->input->post('question_id');
        $result = $this->input->post('answer');

        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $question_info = json_decode($answer[0]['questionName'], true);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);


        $answer_info['student_ans'] = $result;

        if ($question_info['operator'] != '/' && $answer_info['student_ans'] == $answer_info['tutor_ans']) {
            echo 1;
        } elseif ($question_info['operator'] == '/') {// && $result[1] == $answer_info['tutor_ans']
            $stGivenQuotient = $result[0];
            $stGivenRemainder = $result[1];

            $recordedQuotient = $question_info['quotient'];
            $recordedRemainder = $answer_info['tutor_ans'];

            echo ($stGivenQuotient==$recordedQuotient) && ($stGivenRemainder==$recordedRemainder) ? 1 : 0;
        } else {
            echo 0;
        }
    }

    //    Question Edit Portion

    public function question_edit($question_item, $question_id)
    {
        if ($question_item == 1) {
            $this->edit_general($question_item, $question_id);
        } elseif ($question_item == 2) {
            $this->edit_true_false($question_item, $question_id);
        } elseif ($question_item == 3) {
            $this->edit_preview_vocubulary($question_item, $question_id);
        }
    }

    private function edit_general($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/general', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function edit_true_false($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/true-false', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    private function edit_preview_vocubulary($item, $question_id)
    {
        $data['question_info'] = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $data['question_item'] = $item;
        $data['question_id'] = $question_id;
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->Preview_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->Preview_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        $data['subject_base_chapter'] = $this->Preview_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $data['question_box'] = $this->load->view('question_edit/question-box/edit_vocubulary', $data, true);
        $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     *
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
            );
        }
        return $arr;
    }
    
    /**
     * render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item
     * @param  int     $rows    num of row in table
     * @param  int     $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0)
    {
        //print_r($items);die;
        $row = '';
        for ($i=1; $i<=$rows; $i++) {
            $row .='<tr>';
            for ($j=1; $j<=$cols; $j++) {
                if ($items[$i][$j]['type']=='q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = array(
                        'cr'=>$i.'_'.$j,
                        'val'=> $items[$i][$j]['val'],
                        'type'=> 'a',
                    );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns==1)?' value="'.$items[$i][$j]['val'].'"' : '';
                    
                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt'.$i.'_'.$j.'"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }
        
        return $row;
    }
    
    public function answer_multiple_matching()
    {
        $total = $_POST['total_ans'];
        $question_id = $_POST['id'];
        $st_ans = array();
        for ($i = 1; $i <= $total; $i++) {
            $ans_id = 'answer_' . $i;
            $st_ans[] = $_POST[$ans_id];
        }

        $flag = 1;
        $answer = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        
        for ($i = 0; $i < count($answer_info['tutor_ans']); $i++) {
            if ($answer_info['tutor_ans'][$i] != $st_ans[$i]) {
                $flag = 0;
            }
        }
        
        $answer_info['student_ans'] = $st_ans;
        $answer_info['flag'] = $flag;
        echo (json_encode($answer_info));
    }
    
    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    public function renderAssignmentTasks(array $items)
    {
        $row = '';
        foreach ($items as $task) {
            $task = json_decode($task);
            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$task->qMark.'</td>';
            //$row .= '<td>'.$task->obtnMark.'</td>';
            $row .= '<td><i class="fa fa-eye qDtlsOpenModIcon" data-toggle="modal" data-target="#quesDtlsModal"></i></td>';
            $row .= '<input type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;
    }//end renderAssignmentTasks()
}
