<?php

class Tutor extends CI_Controller
{

    public $loggedUserId, $loggedUserType;

    public function test()
    {
        $this->load->view('test');
    }
    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;
        $this->loggedUserType = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }
        
        if ($user_type != 3 && $user_type != 4 && $user_type != 5 && $user_type != 7) {
            redirect('welcome');
        }
        
        $this->load->model('Parent_model');
        $this->load->model('Admin_model');
        $this->load->model('tutor_model');
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->model('QuestionModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }

    public function index()
    {
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/tutors_dashboard', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function tutor_setting()
    {
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('tutors/tutor_setting', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function tutor_details()
    {
        $data['user_info'] = $this->tutor_model->userInfo($this->session->userdata('user_id'));
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('tutors/tutor_details', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function update_tutor_details()
    {
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('passconf', 'passconf', 'trim|required|matches[password]');
        if ($this->form_validation->run() == false) {
            echo 0;
        } else {
            $password = md5($this->input->post('password'));
            $data = array(
                'user_pawd' => $password
            );
            $this->tutor_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    }

    public function tutor_upload_photo()
    {
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/upload', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Tutor can set paypal,credit card info
     *
     * @return void
     */
    public function accountSettings()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if (!$post) {
            $data['user_info'] = $this->tutor_model->userInfo($this->session->userdata('user_id'));
            $data['payment_accounts'] = isset($data['user_info'][0]['payment_accounts'])?$data['user_info'][0]['payment_accounts'] : null;
            if (strlen($data['payment_accounts'])) {
                $data['accounts'] = json_decode($data['payment_accounts']);
            }
            $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
            $data['maincontent'] = $this->load->view('tutors/account_settings', $data, true);
            $this->load->view('master_dashboard', $data);
        } else {
            $dataToUpdate = ['payment_accounts' => json_encode($clean)];
            
            //if additional user info exist then update else insert
            $additionalInfo = $this->tutor_model->getRow('additional_tutor_info', 'tutor_id', $this->loggedUserId);
            if (count($additionalInfo)) {
                $this->tutor_model->updateInfo('additional_tutor_info', 'tutor_id', $this->loggedUserId, $dataToUpdate);
            } else {
                $dataToUpdate['tutor_id'] = $this->loggedUserId;
                $this->tutor_model->insertInfo('additional_tutor_info', $dataToUpdate);
            }
            $this->session->set_flashdata('success_msg', 'Account settings updated');
            redirect('tutor/account/settings');
        }
    }

    private function upload_user_photo_options()
    {
        $config = array();
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docs';
        // $config['max_width'] = 1080;
        // $config['max_height'] = 640;
        // $config['min_width']  = 150;
        // $config['min_height'] = 150;
        $config['overwrite'] = false;
        return $config;
    }

    public function tutor_file_upload()
    {
        $this->upload->initialize($this->upload_user_photo_options());
        if (!$this->upload->do_upload('file')) {
            echo 0;
        } else {
            $imageName = $this->upload->data();
            $user_profile_picture = $imageName['file_name'];
            $data = array(
                'image' => $user_profile_picture
            );
            $rs['res'] = $this->tutor_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    }

    public function view_course()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['maincontent'] = $this->load->view('tutors/view_course', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function all_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course'] = $this->tutor_model->getAllInfo('tbl_course');


        $data['maincontent'] = $this->load->view('tutors/module/all_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function add_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course'] = $this->tutor_model->getAllInfo('tbl_course');

        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        foreach ($data['all_question_type'] as $row) {
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question', $row['id'], $user_id);
        }
        
        $data['all_question'] = $question_list;

        $data['maincontent'] = $this->load->view('tutors/module/add_module', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function question_list()
    {
        $post = $this->input->post();
        $post = array_filter($post);
        $get = $this->input->get();
        $countrySelected = 0;
        $fromQuestionEditPage = 0;
        //module info in flash data for all question area search param
        //if come from module edit page
        if (isset($get['type']) && ($get['type']=='edit')) {
            $mId = $get['mId'];
            $module = $this->Admin_model->search('tbl_module', ['id'=>$mId]);
            if (count($module)) {
                $this->session->set_flashdata('modInfo', $module[0]);
            }
            $countrySelected = 1;
        } elseif (isset($get['country']) || isset($_SESSION['modInfo']['country']) || isset($_SESSION['selCountry'])) { //q-study will select country before going question-list/module, in that case need to filter by country
            $country = isset($get['country']) ? $get['country'] : (isset($_SESSION['modInfo']['country']) ? $_SESSION['modInfo']['country'] : (isset($_SESSION['selCountry']))?$_SESSION['selCountry']:'');
            
            $countrySelected = 1;
        }

        if (isset($_SESSION['refPage']) && $_SESSION['refPage']=='questionEdit') {
            $fromQuestionEditPage = 1;
        }

        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        
        foreach ($data['all_question_type'] as $questionType) {
            $question_list[$questionType['id']] = [];
        }

        if (count($post) || ($countrySelected) || $fromQuestionEditPage) {
            //if get query string fetch question from query scoped module
            $mId = isset($_GET['mId']) ? $_GET['mId'] : null;
            $module = $this->Admin_model->search('tbl_module', ['id'=>$mId]);

            $moduleName = count($module) ? $module[0]['moduleName'] : (isset($post['moduleName']) ? $post['moduleName'] : '');
            //$country = count($module) ? $module[0]['country'] : (isset($post['country']) ? $post['country'] : (isset($get['country']) ? $get['country'] : ''));
            $country = count($module) ? $module[0]['country'] : (isset($country)?$country:'');
            $grade = isset($post['grade']) ? $post['grade'] : (isset($module[0]['studentGrade'])?$module[0]['studentGrade'] : '');

            $moduleType =  count($module) ? $module[0]['moduleType'] : (isset($post['moduleType']) ? $post['moduleType'] : '');
            $subject = isset($post['subject']) ? $post['subject'] :  (isset($module[0]['subject'])?$module[0]['subject'] : '');
            $chapter = isset($post['chapter']) ? $post['chapter'] :  (isset($module[0]['chapter'])?$module[0]['chapter'] : '');
            $course  = isset($post['course']) ? $post['course'] :  (isset($module[0]['course_id'])?$module[0]['course_id'] : '');
            $user_id = $this->loggedUserId;
            if ($post) {
                //save on session for filtering(ques search button click)
                $_SESSION['modInfo'] =  [
                'moduleName' => $moduleName,
                'country' =>    $country,
                'studentGrade' =>    $grade,
                'moduleType' => $moduleType,
                'subject'    => $subject,
                'chapter'    => $this->get_chapter_name($subject, $chapter),
                'course'    => $course,
                ];
            }
            //if request param for module/country/module_type then fetch module question
            //else fetch question from question table
            if (isset($post['moduleName'])  || isset($post['moduleType']) || isset($_GET['mId'])) {
                $conditions = [
                    'moduleName' => $moduleName,
                    'country' =>    $country,
                    'studentGrade' =>    $grade,
                    'moduleType' => $moduleType,
                    'subject'    => $subject,
                    'chapter'    => $chapter,
                    'course_id'    => $course,
                    'user_id' =>    $user_id,
                ];
                $conditions = array_filter($conditions);
                
                $modules = $this->Admin_model->search('tbl_module', $conditions);
                $moduleIds = count($modules) ? array_column($modules, 'id') : -1;
                $moduleQuestions = $this->Admin_model->whereIn('tbl_modulequestion', 'module_id', $moduleIds);
                $questionIds = count($moduleQuestions) ? array_column($moduleQuestions, 'question_id') : -1;
                $questions = $this->Admin_model->whereIn('tbl_question', 'id', $questionIds);
                
                foreach ($questions as $question) {
                    $question_list[$question['questionType']][] = $question;
                }
                foreach ($data['all_question_type'] as $questionType) {
                    if (!($question_list[$questionType['id']])) {
                        $question_list[$questionType['id']] = [];
                    }
                }
            } else {
                //if params come from question edit page
                if (isset($_SESSION['modInfo'])) {
                    $sSub = isset($_SESSION['modInfo']['subject'])?$_SESSION['modInfo']['subject']:'';
                    $pSub = isset($post['subject'])?$post['subject']:'';
                    $sChap = isset($_SESSION['modInfo']['selChapter'])?$_SESSION['modInfo']['selChapter']:'';
                    $pChap = isset($post['chapter'])?$post['chapter']:'';
                    $sGrade = isset($_SESSION['modInfo']['studentGrade'])?$_SESSION['modInfo']['studentGrade']:'';
                    $pGrade = isset($post['grade'])?$post['grade']:'';
                    $sCountry = isset($_SESSION['modInfo']['country'])?$_SESSION['modInfo']['country']:'';
                    $pCountry = isset($post['country'])?$post['country']:'';

                    $subject      = isset($sSub) ? $sSub : (isset($pSub) ? $pSub : '');
                    $chapter      = isset($sChap) ? $sChap : (isset($pChap) ? $pChap : '');
                    $studentgrade = isset($sGrade) ? $sGrade : (isset($pGrade) ? $pGrade : '');
                    $country      = isset($sCountry) ? $sCountry : (isset($pCountry) ? $pCountry : '');
                } else {
                    $subject      = isset($post['subject']) ? $post['subject'] : '';
                    $chapter      = isset($post['chapter']) ? $post['chapter'] : '';
                    $studentgrade = isset($post['grade']) ? $post['grade'] : '';
                    $country = isset($post['country']) ? $post['country'] : '';
                }

                $conditions = [
                    'subject'      => $subject,
                    'chapter'      => $chapter,
                    'studentgrade' => $studentgrade,
                    'country'      => $country,
                ];

                $conditions = array_filter($conditions);
                $conditions['user_id'] = $user_id;
                foreach ($data['all_question_type'] as $questionType) {
                    $conditions['questionType'] = $questionType['id'];
                    $question_list[$questionType['id']] = $this->tutor_model->getUserQuestion('tbl_question', $conditions);
                }
            }
        } else {
            foreach ($data['all_question_type'] as $questionType) {
                $conditions = [
                    'user_id' => $user_id,
                    'questionType' => $questionType['id'],
                ];
                $question_list[$questionType['id']] = $this->tutor_model->getUserQuestion('tbl_question', $conditions);
            }
        }


        $data['all_question'] = $question_list;

        $data['allCountry']        = $this->Admin_model->search('tbl_country', [1=>1]);
        $data['all_subject']        = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
        $data['all_course']        = $this->Admin_model->search('tbl_course', [1=>1]);
        ;

        $data['maincontent'] = $this->load->view('tutors/question/question_list', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function create_question($item)
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
        $data['allCountry']        = $this->Admin_model->search('tbl_country', [1=>1]);

        $data['question_item'] = $item;
        $question_box = 'tutors/question/question-box';

        if ($item==1) {
            $question_box .='/general';
        } elseif ($item==2) {
            $question_box .='/true-false';
        } elseif ($item==3) {
            $question_box .= '/vocabulary';
        } elseif ($item==4) {
            $question_box .= '/multiple-choice';
        } elseif ($item == 5) {
            $question_box .= '/multiple-response';
        } elseif ($item==6) {
            $question_box .= '/skip_quiz';
        } elseif ($item==7) {
            $question_box .= '/matching';
        } elseif ($item == 8) {
            $this->add_assignment_question();
            //$question_box .= '/assignment';
        } elseif ($item == 9) {
            $question_box .= '/creative_quiz';
        } elseif ($item == 10) {
            $question_box .= '/times_table';
        } elseif ($item == 11) {
            $question_box .= '/algorithm';
        } elseif ($item == 12) {
            $question_box .= '/workout_quiz';
        } elseif ($item == 13) {
            $question_box .= '/matching_workout';
        }

        if ($item != 8) {
            $data['question_box']=$this->load->view($question_box, '', true);
            $data['maincontent'] = $this->load->view('tutors/question/create_question', $data, true);
            $this->load->view('master_dashboard', $data);
        }
    }

    public function add_assignment_question()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);

        $data['maincontent'] = $this->load->view('tutors/question/create_assignment_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    public function add_subject_name()
    {
        $data['created_by'] = $this->session->userdata('user_id');
        $data['subject_name'] = $this->input->post('subject_name');

        $this->tutor_model->insertInfo('tbl_subject', $data);

        $all_tutor_subject = $this->tutor_model->getInfo('tbl_subject', 'created_by', $data['created_by']);
        echo '<option value="">Select ...</option>';
        foreach ($all_tutor_subject as $row) {
            echo '<option value="' . $row['subject_id'] . '" onchange="getChapter(' . $row['subject_id'] . ')">' . $row['subject_name'] . '</option>';
        }
    }

    public function get_chapter_name($subject = 0, $selected = 0)
    {
        $subject_id = $subject ? $subject : $this->input->post('subject_id');

        $all_subject_chapter = $this->tutor_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        $html = '<option value="">Select Chapter</option>';
        foreach ($all_subject_chapter as $chapter) {
            $sel = $chapter['id'] == $selected ? 'selected' : '';
            $html .= '<option value="' . $chapter['id'] . '" '.$sel.'>' . $chapter['chapterName'] . '</option>';
        }
        if ($subject) {
            return $html; //within controller
        } else {
            echo $html; // ajax/form submit
        }
    }

    public function save_question_data()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $clean['media'] = isset($_FILES)?$_FILES:[];

        $data['questionType'] = $this->input->post('questionType');
        $questionName = $this->input->post('questionName');
        $answer = $this->input->post('answer');
        $questionMarks = $this->input->post('questionMarks');
        $description = $this->input->post('questionDescription');
        $solution = $this->input->post('question_solution');
        if ($data['questionType'] == 3) {
            $questionName =  $this->processVocabulary($post);
        } if ($_POST['questionType']==4) {
            $questionName = $this->save_multiple_choice($_POST);
            if (isset($_POST['response_answer'])) {
                $answer = $_POST['response_answer'];
            }
        } if ($_POST['questionType']==5) {
            //Same as Multiple Choice
            $questionName = $this->save_multiple_response($_POST);
            if (isset($_POST['response_answer'])) {
                $answer = json_encode($_POST['response_answer']);
            }
        } if ($data['questionType']==6) { //skip quiz
            $temp['question_body'] = isset($clean['question_body'])?$clean['question_body']:'';
            $temp['skp_quiz_box'] = $clean['ques_ans'];
            $temp['numOfRows']     = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $temp['numOfCols']     = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;
            $questionName =  json_encode($temp);
            $answer = json_encode(array_values(array_filter($clean['ans'])));
        } if ($_POST['questionType']==7) {
            $questionName = $this->ques_matching_data($_POST);
            $answer = $this->ans_matching_data($_POST);
        } if ($data['questionType'] == 8) {
            // assignment
            $temp          = $this->processAssignmentTasks($clean);
            $questionName  = json_encode($temp);
            $questionMarks = isset($temp['totMarks'])?$temp['totMarks']:0;
        } if ($_POST['questionType']==9) { //creative quiz
            $question_data['sentence'] = $post['sentence'];
            $question_data['questionName'] = $post['questionName'];
            $questionName = json_encode($question_data);
            $answer = $post['ansSequence'];
            $description = json_encode($post['description']);
        } if ($data['questionType'] == 10) {
            $question_data['questionName'] = $post['questionName'];
            $question_data['factor1'] = $post['factor1'];
            $question_data['factor2'] = $post['factor2'];
            $questionName = json_encode($question_data);

            $answer = json_encode($post['result']);
        } if ($data['questionType'] == 11) {
            $question_data['questionName'] = $post['question_body'];
            $question_data['operator'] = $post['operator'];
            
            if ($post['operator'] == '/') {
                $question_data['divisor'] = $post['divisor'];
                $question_data['dividend'] = $post['dividend'];
                $question_data['remainder'] = $post['remainder'];
                $question_data['quotient'] = $post['quotient'];
                
                $answer = json_encode($post['remainder']);
            } if ($post['operator'] != '/') {
                $question_data['item'] = $post['item'];
                $answer = json_encode($post['result']);
            }
            
            $question_data['numOfRows']     = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $question_data['numOfCols']     = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;
            
            $questionName = json_encode($question_data);
        } if ($_POST['questionType']==13) {
            $questionName = $this->save_multiple_choice($_POST);
            if (isset($_POST['response_answer'])) {
                $answer = $_POST['response_answer'];
            }
        }

        //print_r($questionName);die;
        $data['studentgrade'] = $this->input->post('studentgrade');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['subject'] = $this->input->post('subject');
        $data['chapter'] = $this->input->post('chapter');
        $data['country'] = $this->input->post('country');
        $data['questionName'] = $questionName;//$this->input->post('questionName');
        $data['answer'] = $answer;
        $data['questionMarks'] = $questionMarks;
        $data['questionDescription'] =  $description;
        $data['isCalculator'] = $this->input->post('isCalculator');
        $data['question_solution'] = strlen($solution)<1 ? 'NO solution given' : $solution;

        $hour = $this->input->post('hour');
        $minute = $this->input->post('minute');
        $second = $this->input->post('second');

        $data['questionTime'] = $hour.":".$minute.":".$second;

        $chkValidation['flag'] = 1;
        if ($data['questionType'] != 8) {
            $chkValidation = $this->checkValidation($data);
        }
        if ($chkValidation['flag'] == 0) {
            echo json_encode($chkValidation);
        } else {
            $questionId = $this->tutor_model->insertId('tbl_question', $data);
            //        $this->questionMediaUpload($questionId);
            $chkValidation['question_id'] = $questionId;
            echo json_encode($chkValidation);
        }
    }

    public function checkValidation($data)
    {
        $return_data['flag'] = 1;
        if ($data['studentgrade'] == '') {
            $return_data['msg'] = 'Student Grade Need To Be Selected';
            $return_data['flag'] = 0;
        } elseif ($data['subject'] == '') {
            $return_data['msg'] = 'Subject Need To Be Selected';
            $return_data['flag'] = 0;
        } elseif ($data['chapter'] == '') {
            $return_data['msg'] = 'Chapter Need To Be Selected';
            $return_data['flag'] = 0;
        } elseif ($data['questionName'] == '') {
            $return_data['msg'] = 'Question Can Not Be empty';
            $return_data['flag'] = 0;
        } elseif ($data['answer'] == '') {
            $return_data['msg'] = 'Answer Can Not Be empty';
            $return_data['flag'] = 0;
        } elseif ($data['question_solution'] == '') {
            $return_data['msg'] = 'Solution Can Not Be empty';
            $return_data['flag'] = 0;
        }
        // else if($data['questionTime'] == 'HH:MM:SS'){
            // $return_data['msg'] = 'Time Can Not Be empty';
            // $return_data['flag'] = 0;
        // }
        elseif ($data['questionMarks'] == '') {
            $return_data['msg'] = 'Marks Can Not Be empty';
            $return_data['flag'] = 0;
        }
        //else if($data['questionDescription'] == ''){
            // $return_data['msg'] = 'Description Can Not Be empty';
            // $return_data['flag'] = 0;
        //}
        return $return_data;
    }

    public function imageUpload()
    {
        $files = $_FILES;

        $_FILES['file']['name'] = $files['file']['name'];
        $_FILES['file']['type'] = $files['file']['type'];
        $_FILES['file']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['file']['error'] = $files['file']['error'];
        $_FILES['file']['size'] = $files['file']['size'];
        
        $config['upload_path'] = 'assets/uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|webm|doc|docx|mp4|webm|ogg|avi';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        //        $this->upload->do_upload();

        $error = array();
        if (!$this->upload->do_upload('file')) {
            $error = $this->upload->display_errors();
            //echo $error;
        } else {
            $imageName = $this->upload->data();
            $base = base_url() . 'assets/uploads/' . $imageName['file_name'];
            echo '{"fileName":"' . $imageName['file_name'] . '","uploaded":1,"url":"' . $base . '"}';
        }
    }
    
    /**
     * Grab item from post method for question field
     *
     * @param array $items post array
     *
     * @return string json encoded data to record as question
     */
    public function processVocabulary($items)
    {
        $arr['definition'] = $items['definition'];
        $arr['parts_of_speech'] = $items['parts_of_speech'];
        $arr['synonym'] = $items['synonym'];
        $arr['antonym'] = $items['antonym'];
        $arr['sentence'] = $items['sentence'];
        $arr['near_antonym'] = $items['near_antonym'];
        $arr['speech_to_text'] = $items['speech_to_text'];
        $arr['ytLinkInput'] = $items['ytLinkInput'];
        $arr['ytLinkTitle'] = $items['ytLinkTitle'];
        
        $uType = $this->session->userdata('userType');

        //$arr['vocubulary_image'] = $items['vocubulary_image'];
        for ($i = 1; $i <= $items['image_quantity']; $i++) {
            //$image = 'vocubulary_image_' . $i . '[]';
            $desired_image[] = $items['vocubulary_image_'.$i];
        }
        if ($desired_image) {
            $arr['vocubulary_image'] = $desired_image;
        }
        
        if (isset($items['existed_audio_File']) && $items['existed_audio_File'] != '') {
            $arr['audioFile'] = $items['existed_audio_File'];
        }
        
        $files = $_FILES;
        //only q-study user can upload video
        if (isset($_FILES['videoFile']) && $_FILES['videoFile']['error'][0] != 4 && $uType==7) {
            $_FILES['videoFile']['name'] = $files['videoFile']['name'];
            $_FILES['videoFile']['type'] = $files['videoFile']['type'];
            $_FILES['videoFile']['tmp_name'] = $files['videoFile']['tmp_name'];
            $_FILES['videoFile']['error'] = $files['videoFile']['error'];
            $_FILES['videoFile']['size'] = $files['videoFile']['size'];

            $config['upload_path'] = 'assets/uploads/question_media/';
            $config['allowed_types'] = 'mp3|mp4|3gp|ogg|wmv';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload();

            $error = array();
            if (!$this->upload->do_upload('videoFile')) {
                $error = $this->upload->display_errors();
            } else {
                $fdata = $this->upload->data();
                $arr['videoFile'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
        
        if (isset($_FILES['audioFile']) && $_FILES['audioFile']['error'][0] != 4) {
            $_FILES['audioFile']['name'] = $files['audioFile']['name'];
            $_FILES['audioFile']['type'] = $files['audioFile']['type'];
            $_FILES['audioFile']['tmp_name'] = $files['audioFile']['tmp_name'];
            $_FILES['audioFile']['error'] = $files['audioFile']['error'];
            $_FILES['audioFile']['size'] = $files['audioFile']['size'];

            $config['upload_path'] = 'assets/uploads/question_media/';
            $config['allowed_types'] = 'mp3|mp4|3gp|ogg|wmv';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload();
            
            if (!$this->upload->do_upload('audioFile')) {
                $error = $this->upload->display_errors();
            } else {
                $fdata1 = $this->upload->data();
                $arr['audioFile'] = $config['upload_path'] . $fdata1['file_name'];
            }
        }
        
        return json_encode($arr);
    }
    
    /**
     * question media file upload and record
     *
     * @param  integer $questionId questionId for files to link with
     * @return void
     */
    public function questionMediaUpload($questionId = 0)
    {
        $files = $_FILES;
        $dataToInsert = [];
        $config['upload_path'] = "assets/uploads/question_media";
        
        $config['allowed_types']        = 'mp3|mp4|3gp|ogg|wmv';
        $config['max_size']             = 18403791;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        
        foreach ($files as $index => $item) {
            $config['file_name'] = uniqid();
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload($index);
            
            $fileName = $this->upload->data('file_name');
            $filePath = $this->upload->data('file_path');
            $fileType = $this->upload->data('file_type');
            
            $dataToInsert['media_url'] = $filePath.$fileName;
            $dataToInsert['media_type'] = $fileType;
            $dataToInsert['upload_date'] = date("Y-m-d H:i:s");
            $dataToInsert['question_id'] = $questionId;
            $this->tutor_model->insertInfo('tbl_question_media', $dataToInsert);
        }
    }
    
    //    Question Edit Option
    public function question_edit($type, $question_id)
    {
        $data['question_info'] = $this->tutor_model->getQuestionInfo($type, $question_id);
        
        $data['question_item'] = $type;
        $data['question_id'] = $question_id;
        
        $user_id = $this->session->userdata('user_id');
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
        $subject_id = $data['question_info'][0]['subject'];
        
        $data['subject_base_chapter'] = $this->tutor_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        if (count($data['question_info'])) {
            $data['allCountry'] = $this->Admin_model->search('tbl_country', [1=>1]);
            $data['selCountry'] = $data['question_info'][0]['country'];
            $quesSub = $data['question_info'][0]['subject'];
            $quesChap = $data['question_info'][0]['chapter'];
            $chaps = $this->get_chapter_name($quesSub, $quesChap); //selected $quesChap
            $temp = [
                'subject' =>$data['question_info'][0]['subject'],
                'chapter' =>$chaps,
                'selChapter' =>$quesChap,
                'studentGrade' =>$data['question_info'][0]['studentgrade'],
            ];
            $this->session->set_flashdata('refPage', 'questionEdit');
            $this->session->set_flashdata('modInfo', $temp);
        }
        $qSearchParams = [
            'questionType' =>$type,
            'user_id' =>$this->loggedUserId,
        ];
        $allQuestionIds = $this->QuestionModel->search('tbl_question', $qSearchParams);
        $allQuestionIds = array_column($allQuestionIds, 'id');
        $data['qIndex'] = array_search($question_id, $allQuestionIds);
        
        //if ques not found by loggedUserId and ques type then redirect to list
        if (!is_int($data['qIndex'])) {
            redirect('question-list');
        } else {
            $data['qIndex'] += 1;
        }

        
        $question_box = 'question_edit/question-box';
        if ($type == 1) {
            $question_box .= '/general';
        }
        if ($type == 2) {
            $question_box .= '/true-false';
        }
        if ($type == 3) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
            $question_box .= '/edit_vocubulary';
        }
        if ($type == 4) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
            $question_box .= '/edit_multiple_choice';
        }
        if ($type == 5) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
            $question_box .= '/edit_multiple_response';
        }
        if ($type == 7) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
            $data['question_answer'] = json_decode($data['question_info'][0]['answer']);
            $question_box .= '/edit_matching';
        }
        if ($type == 6) {
            $quesInfo1 = json_decode($data['question_info'][0]['questionName']);
            $items = $this->indexQuesAns($quesInfo1->skp_quiz_box);
            $data['numOfRows'] = $quesInfo1->numOfRows;
            $data['numOfCols'] = $quesInfo1->numOfCols;
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols'], $showAns = 1, 'edit');

            $data['questionBody'] = $quesInfo1->question_body;
            $question_box .= '/skip_quiz';
        } if ($type == 8) {
            $this->edit_assignment_question($data);
        } if ($type == 9) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName'], true);
            $data['question_answer'] = json_decode($data['question_info'][0]['answer'], true);
            $data['question_description'] = json_decode($data['question_info'][0]['questionDescription'], true);
            //            print_r($data['question_description']);die;
            $question_box .= '/edit_creative_quiz';
        }
        if ($type == 10) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName'], true);
            $data['question_answer'] = json_decode($data['question_info'][0]['answer'], true);
            
            $question_box .= '/edit_times_table';
        } if ($type == 11) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName'], true);
            $data['question_answer'] = json_decode($data['question_info'][0]['answer'], true);
            
            $question_box .= '/edit_algorithm';
        } if ($type == 12) {
            $question_box .= '/workout_quiz';
        } if ($type == 13) {
            $data['question_info_ind'] = json_decode($data['question_info'][0]['questionName']);
            $question_box .= '/edit_matching_workout';
        }
        if ($type != 8) {
            $data['question_box'] = $this->load->view($question_box, $data, true);

            $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
            $data['header'] = $this->load->view('dashboard_template/header', $data, true);
            $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

            $data['maincontent'] = $this->load->view('question_edit/edit_question', $data, true);
            $this->load->view('master_dashboard', $data);
        }
    }
    
    
    public function edit_assignment_question($data)
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('question_edit/edit_assignment_question', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    
    /**
     * Before passing items to renderSkpQuizPrevTable() index it first with this func
     * basically for preview skip quiz table
     *
     * @param array $items Json object array.
     *
     * @return array        Array with proper indexing
     */
    public function indexQuesAns($items)
    {
        $arr = [];
        foreach ($items as $item) {
            $temp            = json_decode($item);
            if ($temp) {
                $cr              = explode('_', $temp->cr);
                $col             = $cr[0];
                $row             = $cr[1];
                $arr[$col][$row] = [
                    'type' => $temp->type,
                    'val'  => $temp->val,
                ];
            }
        }

        return $arr;
    }//end indexQuesAns()
    

    /**
     * Render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item(get processed items from indexQuesAns())
     * @param  integer $rows    num of row in table
     * @param  integer $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 0, $pageType = '')
    {
        // print_r($items);die;
        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .= '<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td>'
                    . '<input type="text" data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control rsskpin input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px; background-color:#ffb7c5;">';
                    if ($pageType = 'edit') {
                        $quesObj = [
                            'cr'   => $i.'_'.$j,
                            'val'  => $items[$i][$j]['val'],
                            'type' => 'q',
                        ];
                        $quesObj = json_encode($quesObj);
                        //                        echo $quesObj.'<pre>';
                        $row    .= '<input type="hidden" value=\''.$quesObj.'\' name="ques_ans[]" id="obj">';
                        $row .= '<input type="hidden" value="" name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                } else {
                    $ansObj = [
                        'cr'   => $i.'_'.$j,
                        'val'  => $items[$i][$j]['val'],
                        'type' => 'a',
                    ];
                    $ansObj = json_encode($ansObj);
                    $val    = ($showAns == 1) ? ' value="'.$items[$i][$j]['val'].'"' : '';

                    $row .= '<td>'
                    . '<input type="text" data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control rsskpin input-box rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px; background-color:#baffba;">';
                    //                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    if ($pageType = 'edit') {
                        $row .= '<input type="hidden" value=\''.$ansObj.'\' name="ques_ans[]" id="obj">';
                        $row .= '<input type="hidden" value=\''.$ansObj.'\' name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                }//end if
            }//end for

            $row .= '</tr>';
        }//end for

        return $row;
    }//end renderSkpQuizPrevTable()
    

    /**
     * view student progress by his/her tutor
     * based on search params
     *
     * @return void
     */
    public function viewStudentProgress()
    {
        $data = $this->commonPart();

        $post = $this->input->post();
        $data['st_progress'] = '';

        $this->form_validation->set_rules('studentId', 'Student Id', 'required');
        $this->form_validation->set_rules('moduleTypeId', 'Module Type', 'required');
        if ($this->form_validation->run() == true) {
            if (isset($post['studentId'])) {
                $conditions['student_id'] = $post['studentId'];
            }if (isset($post['moduleTypeId'])) {
                $conditions['moduletype'] = $post['moduleTypeId'];
            }

            $allProgress = $this->Student_model->studentProgress($conditions);
            $data['st_progress'] = $this->renderStProgress($allProgress);
        }

        $conditions = array(
            'sct_id' => $this->loggedUserId,
        );

        $studentIds = $this->tutor_model->allStudents($conditions);
        $data['students'] = $this->renderStudents($studentIds);
        $data['moduleTypes'] = $this->renderModuletypes($this->tutor_model->allModuleType());


        $data['maincontent'] = $this->load->view('students/student_progress', $data, true);
        $this->load->view('master_dashboard', $data);
    }

    /**
     * wrap students info with option
     *
     * @param  array $studentIds student ids of tutor
     * @return string             wrapped string
     */
    public function renderStudents($studentIds)
    {

        $options = '';
        foreach ($studentIds as $studentId) {
            $student = $this->Student_model->userInfo($studentId);
            $student = $student[0];
            $options .= '<option value="' . $studentId . '">' . $student['name'] . '</option>';
        }
        return $options;
    }

    /**
     * wrap module types with option tag
     *
     * @param  array $moduleTypes all module type of a tutor attached with
     * @return string              wrapped string
     */
    public function renderModuletypes($moduleTypes)
    {
        $options = '';
        foreach ($moduleTypes as $moduleType) {
            $options .= '<option value="' . $moduleType['id'] . '">' . $moduleType['module_type'] . '</option>';
        }
        return $options;
    }

    public function renderStProgress($items)
    {
        $row = '';
        foreach ($items as $item) {
            $row .= '<tr>';
            $row .= '<td>' . $this->ModuleModel->moduleName($item['module']) . '</td>';
            $row .= '<td>' . $this->ModuleModel->moduleTypeName($item['moduletype']) . '</td>';
            $row .= '<td>' . date('Y-m-d', $item['answerDate']) . '</td>';
            $row .= '<td>' . $item['answerTime'] . '</td>';
            $row .= '<td>' . $item['timeTaken'] . '</td>';
            $row .= '<td>' . $item['originalMark'] . '</td>';
            $row .= '<td>' . $item['studentMark'] . '</td>';
            $row .= '<td>' . $item['percentage'] . '</td>';
            $row .= '</tr>';
        }
        return $row;
    }

    /**
     * till now seems common on all functions
     *
     * @return array essential data/view
     */
    public function commonPart()
    {
        $user_id = $this->loggedUserId;

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header'] = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course'] = $this->tutor_model->getAllInfo('tbl_course');

        return $data;
    }

    public function get_subject()
    {
        $subject = $this->tutor_model->getInfo('tbl_subject', 'created_by', $this->session->userdata('user_id'));
        echo json_encode($subject);
    }

    public function add_chapter()
    {
        $this->form_validation->set_rules('attached_subject', 'attached_subject', 'required');
        $this->form_validation->set_rules('chapter', 'chapter', 'required');
        if ($this->form_validation->run() == false) {
            echo validation_errors();
        } else {
            //            print_r($_POST);
            $data['subjectId'] = $_POST['attached_subject'];
            $data['chapterName'] = $_POST['chapter'];
            $data['created_by'] = $this->session->userdata('user_id');
            $chapter = $this->tutor_model->insertInfo('tbl_chapter', $data);
            $all_subject_chapter = $this->tutor_model->getInfo('tbl_chapter', 'subjectId', $data['subjectId']);
            
            echo '<option value="">Select Chapter</option>';
            foreach ($all_subject_chapter as $chapter) {
                echo '<option value="' . $chapter['id'] . '">' . $chapter['chapterName'] . '</option>';
            }
        }
    }

    /**
     * check given skip box answers with the stored answers
     * can give wrong value given indices
     *
     * @return void
     */
    public function checkSkpboxAnswer()
    {
        $post = $this->input->post();
        $questionId = $this->input->post('questionId');
        $givenAns = $this->indexQuesAns($post['given_ans']);

        $temp = $this->tutor_model->getInfo('tbl_question', 'id', $questionId);
        $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

        $temp2 = json_decode($temp[0]['questionName']);
        $numOfRows = $temp2->numOfRows;
        $numOfCols = $temp2->numOfCols;
        //echo $numOfRows .' ' . $numOfCols;
        $wrongAnsIndices = [];
        
        for ($row=1; $row<=$numOfRows; $row++) {
            for ($col=1; $col<=$numOfCols; $col++) {
                if (isset($savedAns[$row][$col]) && isset($givenAns[$row][$col])) {
                    $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row.'_'.$col:null;
                }
            }
        }
        
        $wrongAnsIndices = array_filter($wrongAnsIndices);
        //echo count($savedAns);
        if (count($wrongAnsIndices) || count($givenAns) != count($savedAns)) {
            echo 3;
        } else {
            echo 2;
        }
    }

    /**
     * get right answers for a question, get hit from ajax call
     *
     * @return string table item
     */
    public function getRightAns()
    {
        $post = $this->input->post();
        $questionId = $post['qId'];
        $temp = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $quesAns = json_decode($temp[0]['questionName']);
        $items = $this->indexQuesAns($quesAns->skp_quiz_box);
        // print_r($items);
        $rows = $quesAns->numOfRows;
        $cols = $quesAns->numOfCols;
        $tblData = $this->renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 1);
        echo $tblData;
    }
    
    public function update_question_data()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $clean['media'] = isset($_FILES)?$_FILES:[];
        
        $data['questionType'] = $this->input->post('questionType');
        $question_id = $this->input->post('question_id');
        
        $questionName = $this->input->post('questionName');
        $answer = $this->input->post('answer');
        $description = $this->input->post('questionDescription');
        
        if ($data['questionType'] == 3) {
            $questionName =  $this->processVocabulary($post);
        } if ($_POST['questionType']==4) {
            $questionName = $this->save_multiple_choice($_POST);
            $answer = $_POST['response_answer'];
        } if ($_POST['questionType']==5) {
            //Same as Multiple Choice
            $questionName = $this->save_multiple_response($_POST);
            $answer = json_encode($_POST['response_answer']);
        } if ($data['questionType']==6) {
            //skip quiz
            $temp['question_body'] = isset($clean['question_body'])?$clean['question_body']:'';
            $temp['skp_quiz_box'] = $clean['ques_ans'];
            $temp['numOfRows']     = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $temp['numOfCols']     = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;
            $questionName =  json_encode($temp);
            $answer = json_encode(array_values(array_filter($clean['ans'])));
        } if ($_POST['questionType']==7) {
            $questionName = $this->ques_matching_data($_POST);
            $answer = $this->ans_matching_data($_POST);
        } if ($data['questionType'] == 8) {
            // assignment
            $temp         = $this->processAssignmentTasks($clean);
            $questionName = json_encode($temp);
        } if ($_POST['questionType']==9) { //creative quiz
            $question_data['sentence'] = $post['sentence'];
            $question_data['questionName'] = $post['questionName'];
            $questionName = json_encode($question_data);
            asort($post['answer_value'], SORT_NUMERIC);
            $question_answer = array_keys(array_filter($post['answer_value']));
            foreach ($question_answer as $ans) {
                $ans1[] = $ans+1;
            }
            $answer = json_encode($ans1);
            
            $description = json_encode($post['description']);
        } if ($data['questionType'] == 10) {
            $question_data['questionName'] = $post['questionName'];
            $question_data['factor1'] = $post['factor1'];
            $question_data['factor2'] = $post['factor2'];
            $questionName = json_encode($question_data);
            
            $answer = json_encode($post['result']);
        } if ($data['questionType'] == 11) {
            $question_data['questionName'] = $post['question_body'];
            $question_data['operator'] = $post['operator'];
            
            if ($post['operator'] == '/') {
                $question_data['divisor'] = $post['divisor'];
                $question_data['dividend'] = $post['dividend'];
                $question_data['remainder'] = $post['remainder'];
                
                $answer = json_encode($post['remainder']);
            } if ($post['operator'] != '/') {
                $question_data['item'] = $post['item'];
                $answer = json_encode($post['result']);
            }
            
            $question_data['numOfRows']     = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $question_data['numOfCols']     = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;
            
            $questionName = json_encode($question_data);
        }
        
        
        $data['studentgrade'] = $this->input->post('studentgrade');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['subject'] = $this->input->post('subject');
        $data['chapter'] = $this->input->post('chapter');
        $data['country'] = $this->input->post('country');
        $data['questionName'] = $questionName;
        $data['answer'] = $answer;
        $data['questionMarks'] = $this->input->post('questionMarks');
        $data['questionDescription'] = $description;
        $data['isCalculator'] = $this->input->post('isCalculator');
        $data['question_solution'] = $this->input->post('question_solution');

        $hour = $this->input->post('hour');
        $minute = $this->input->post('minute');
        $second = $this->input->post('second');

        $data['questionTime'] = $hour.":".$minute.":".$second;
        $this->tutor_model->updateInfo('tbl_question', 'id', $question_id, $data);
        echo $question_id;
    }

    private function update_vocabulary()
    {
        $question_id = $this->input->post('question_id');
        $data_image_quantity = $this->input->post('image_quantity');
        $array = array();
        for ($i = 0; $i <= $data_image_quantity; $i++) {
            $image = 'vocubulary_image_' . $i . '[]';
            $desired_image = $this->input->post($image);
            if ($desired_image[0]) {
                $array[] = $desired_image;
            }
        }
        $arr['definition'] = $_POST['definition'];
        $arr['parts_of_speech'] = $_POST['parts_of_speech'];
        $arr['synonym'] = $_POST['synonym'];
        $arr['antonym'] = $_POST['antonym'];
        $arr['sentence'] = $_POST['sentence'];
        $arr['near_antonym'] = $_POST['near_antonym'];
        $arr['vocubulary_image'] = $array;
        $combined_data = json_encode($arr);
        $data['answer'] = $this->input->post['answer'];
        $data['questionName'] = $combined_data;
        $data['subject'] = $this->input->post['subject'];
        $data['chapter'] = $this->input->post['chapter'];
        $data['questionDescription'] = $this->input->post['questionDescription'];
        $this->tutor_model->updateInfo('tbl_question', 'id', $question_id, $data);
        echo $question_id;
    }
    
    
    public function ques_matching_data($post_data)
    {
        $array_1 = array();
        for ($i = 1; $i <= $post_data['image_quantity']; $i++) {
            $array_1[] = $post_data['match_image_1_'.$i];
        }
        $arr['left_side'] = $array_1;

        $array_2 = array();
        for ($i = 1; $i <= $post_data['image_quantity']; $i++) {
            $array_2[] = $post_data['match_image_2_'.$i];
        }

        $arr['right_side'] = $array_2;

        $arr['questionName'] = $post_data['questionName'];
        $combined_data = json_encode($arr);
        return $combined_data;
    }
    
    public function ans_matching_data($post_data)
    {
        $data_answer = array();
        for ($i = 1; $i <= $post_data['image_quantity']; $i++) {
            //            $answer = 'answer_' . $i;
            $data_answer[] = $post_data['answer_'.$i];
        }
        return json_encode($data_answer);
    }
    
    /**
     * process assignment type question form input
     *
     * @param  array $items form input
     * @return string        json object string
     */
    public function processAssignmentTasks(array $items)
    {
        $itemNum = count($items['qMark']);
        $arr     = [];
        $temp    = [];
        $temp['totMarks'] = 0;
        
        for ($a = 0; $a < $itemNum; $a++) {
            $arr[] = json_encode(
                [
                    'serial'      => $a,
                    'qMark'       => $items['qMark'][$a],
                //'obtnMark'    => $items['obtnMark'][$a],
                    'description' => $items['descriptions'][$a],
                ]
            );
            $temp['totMarks'] += $items['qMark'][$a];
        }

        $temp['question_body']    = $items['question_body'];
        $temp['assignment_tasks'] = $arr;
        return $temp;
    }//end processAssignmentTasks()
    
    
    private function save_multiple_choice($post_data)
    {
        for ($i = 1; $i <= $post_data['image_quantity']; $i++) {
            //            $image = 'vocubulary_image_' . $i . '[]';
            $desired_image[] = $post_data['vocubulary_image_'.$i];
        }
        $arr['questionName'] = $post_data['questionName'];
        if ($desired_image) {
            $arr['vocubulary_image'] = $desired_image;
        }

        $combined_data = json_encode($arr);
        return $combined_data;
    }
    
    private function save_multiple_response($post_data)
    {
        for ($i = 1; $i <= $post_data['image_quantity']; $i++) {
            //            $image = 'vocubulary_image_' . $i . '[]';
            $desired_image[] = $post_data['vocubulary_image_'.$i];
        }
        $arr['questionName'] = $post_data['questionName'];
        if ($desired_image) {
            $arr['vocubulary_image'] = $desired_image;
        }

        $combined_data = json_encode($arr);
        return $combined_data;
    }
    // added by sobuj
    public function module_preview($modle_id, $question_order_id)
    {

        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);
        $data['total_question'] = $this->tutor_model->getModuleQuestion($modle_id, null, 1);
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        if ($data['question_info_s'][0]['question_type']==1) {
            $data['maincontent'] = $this->load->view('module_preview/preview_general', $data, true);
        } elseif ($data['question_info_s'][0]['question_type']==2) {
            $data['maincontent'] = $this->load->view('module_preview/preview_true_false', $data, true);
        } elseif ($data['question_info_s'][0]['question_type']==3) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('module_preview/preview_vocabulary', $data, true);
        } elseif ($data['question_info_s'][0]['question_type']==4) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('module_preview/preview_multiple_choice', $data, true);
        } elseif ($data['question_info_s'][0]['question_type']==5) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('module_preview/preview_multiple_response', $data, true);
        } elseif ($data['question_info_s'][0]['question_type']==7) {
            $data['question_info_left_right'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent'] = $this->load->view('module_preview/preview_matching', $data, true);
        }
        
        $this->load->view('master_dashboard', $data);
    }

    /**
     * Update tutor account and additional info table
     *
     * @return void
     */
    public function updateProfile()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $this->form_validation->set_rules('name', 'Name', 'Required');
        if ($this->form_validation->run()==true) {
            $additionalTableData = [
                'address'        =>isset($clean['address'])?$clean['address']:'',
                'city'           =>isset($clean['city'])?$clean['city']:'',
                'state'          =>isset($clean['state'])?$clean['state']:'',
                'post_code'      =>isset($clean['post_code'])?$clean['post_code']:'',
                'phone_num'      =>isset($clean['phone_num'])?$clean['phone_num']:'',
                'website'        =>isset($clean['website'])?$clean['website']:'',
                'short_bio'      =>isset($clean['short_bio'])?$clean['short_bio']:'',
                'tutoring_rates' =>isset($clean['tutoring_rates'])?$clean['tutoring_rates']:'',
                'qualification'  =>isset($clean['qualification'])?$clean['qualification']:'',
                'availability'   =>isset($clean['availability'])?$clean['availability']:'',
                'language'       =>isset($clean['language'])?$clean['language']:'',
                'updated_at'     =>date('Y-m-d H:i:s'),
            ];

            $userAccountTableData = [
                'name'          =>$post['name'],
                'country_id'    =>$clean['country_id'],
                'user_email'    =>isset($clean['user_email'])?$clean['user_email']:'',
                'user_mobile'   =>isset($clean['user_mobile'])?$clean['user_mobile']:'',
            ];

            $this->tutor_model->updateInfo('additional_tutor_info', 'tutor_id', $this->loggedUserId, $additionalTableData);
            $this->tutor_model->updateInfo('tbl_useraccount', 'id', $this->loggedUserId, $userAccountTableData);
            
            $this->session->set_flashdata('success_msg', 'Account Updated Successfully');
        } // update tutor account if post has data


        $conditions = [
            'tbl_useraccount.id'=>$this->loggedUserId,
            'tbl_useraccount.user_type'=>3,
        ];
        $tutor = $this->tutor_model->tutorInfo($conditions);
        $data['tutor_info'] = $tutor[0];
        $country = $this->tutor_model->getRow('tbl_country', 'id', $data['tutor_info']['country_id']);
        $data['tutor_info']['country'] = $country['countryName'];
        $data['tutor_info']['country_id'] = $country['id'];

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('tutors/update_profile', $data, true);
        $this->load->view('master_dashboard', $data);
    }
    
    public function check_student_copy($module_id, $student_id, $question_order_id)
    {
        $data['module_info'] = $this->Student_model->getInfo('tbl_module', 'id', $module_id);

        if (!$data['module_info']) {
            redirect('error');
        }

        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $student_id);

        //****** Get Temp table data for Tutorial Module Type ******
        if ($data['module_info'][0]['moduleType'] != 1) {
            $table = 'tbl_student_answer';
        } else {
            $table = 'tbl_temp_tutorial_mod_ques';
        }

        $data['tutorial_ans_info'] = $this->Student_model->getTutorialAnsInfo($table, $module_id, $student_id);

        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($module_id, $question_order_id, null);

        if (!$data['question_info_s']) {
            $question_order_id = $question_order_id + 1;
            redirect('check_student_copy/' . $module_id . '/' . $student_id . '/' . $question_order_id);
        }

        $data['total_question'] = $this->tutor_model->getModuleQuestion($module_id, null, 1);
        $question_box = 'tutors/check_student_copy/question_box';
        if ($data['question_info_s'][0]['question_type'] == 1) {
            $question_box .= '/general';
        }
        if ($data['question_info_s'][0]['question_type'] == 2) {
            $question_box .= '/true-false';
        }
        if ($data['question_info_s'][0]['question_type'] == 3) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/vocabulary';
        }
        if ($data['question_info_s'][0]['question_type'] == 4) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/multiple-choice';
        }
        if ($data['question_info_s'][0]['question_type'] == 5) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $question_box .= '/multiple-response';
        }
        if ($data['question_info_s'][0]['question_type'] == 7) {
            $data['question_info_ind'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['question_answer'] = json_decode($data['question_info_s'][0]['answer']);
            $question_box .= '/matching';
        }
        if ($data['question_info_s'][0]['question_type'] == 6) {
            $quesInfo1 = json_decode($data['question_info_s'][0]['questionName']);
            $student_all_ans = json_decode($data['tutorial_ans_info'][0]['st_ans'], true);
            $student_ans = json_decode($student_all_ans[$question_order_id]['student_ans']);
            $student_ans = json_decode($student_ans, true);


            $json_extract = new stdClass;
            foreach ($quesInfo1->skp_quiz_box as $row) {
                $json_extract = json_decode($row);

                if ($json_extract->type == 'a') {
                    $col_extract = explode('_', $json_extract->cr);
                    $json_extract->val = $student_ans[$col_extract[0]][$col_extract[1]]['val'];
                }
                //
                $json_insertion[] = json_encode($json_extract);
            }


            $items = $this->indexQuesAns($json_insertion);
            $data['numOfRows'] = $quesInfo1->numOfRows;
            $data['numOfCols'] = $quesInfo1->numOfCols;
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols'], $showAns = 1, 'edit');

            $data['questionBody'] = $quesInfo1->question_body;
            $question_box .= '/skip_quiz';
        }
        //        if ($data['module_info'][0]['moduleType'] == 8) {
        //            $this->edit_assignment_question($data);
        //        }

        $data['question_box'] = $this->load->view($question_box, $data, true);

        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/check_student_copy/student_copy', $data, true);
        $this->load->view('master_dashboard', $data);
    }
}
